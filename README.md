# Bread++
(c) 2016 Petr Havlíček (<petr.havlicek@petrh.cz>)

Licensed under CC-BY-SA More info at: <https://creativecommons.org/licenses/by-sa/3.0/>

Extension board for breadboard with 3220 holes like Elenco 9440. The main design goal is design simple piece of hardware which everyone can make by their own. I used mainly THT and largest SMD parts for easy soldering by beginners (sorry for FTDI chip). Component selection is based on my shelf status. So I suppose that major part of components will have everyone at home already.

![Bread++ on breadboard](photos/4_breadpp_mounted.jpg)

## Instalation
 
This board is designed to installation directly on bredboard. It is mounted at 8 standoffs with M3 thread, 12 mm height and two M3x6 screws from both ends.

You need to drill holes to metal base plate of breadboard. I currently don't have any drill plan so the only way is make board and solder power buse connectors. After you insert board to breadboard mark precise drill location and drill it.

If you prefer drill plan please contact me by e-mail.

## Modification

If You think that some essential module is missing or You can design this better You can write me or fork this. Sorry for using Eagle for this project. You will need Eagle Proffesional to edit PCB which is greater than 160x100 mm. I plan for next OSHW projects using KiCAD.

I will be also happy for any feedback about this project.

## Modules

This board  contains serval modules for make prototyping easier. Almost all modules has jumper marked as EN. Which is connecting module to central Vcc power rail which can be +5 V or + 3.3 V.

Selection of modules is based just on my personal preference.

#### PSU

PSU is main part which use 3 pcs of 4 mm bannana plugs to get power from lab power supply. Expected inputs are -12 V, GND and +12 V. PSU use basic circuit with 7805, 7905 and LD1117AV33 (like 7805 but for 3.3 V). Output voltages are -12 V, -5V , GND, +3.3 V, +5 V and +12 V. These voltages are used by Voltage selector module.

PSU has jumper for selecting source of +5 V power rail. You can choose between 7805 or USB connector. So for basic construction You don't need big external power supply because You can use just USB port of your PC.

#### Voltage selector

Voltage selector is colection of jumpers for selecting power rail for each power bus in bredboard. Power bus I mean column of conected pin marked blue or red line. Each column has own selector. For negative (blue) one are availible -12 V, -5 V and GND. For positive (red) You can choose from +12 V, +5 V, +3.3 V and GND.

#### LEDs

Basic module with LEDs which can be used as logic probe. There are two LEDs for each input, one is signalisation logic level 1 and the other signalisation logic level 0.

#### Buttons

This module has 4 buttons with standard connection with 10k pull-up and capacitor for minimize bouncing.

#### RGB LEDs

RGB LEDs are now very popular. I'm using they for diagnostic signalization. I used WS2812B RGB LED with serial in pin. You just need send 24 bits representing colour and right colour came up. The LED also have serial out pin to daisy chain more LEDs. This module contains 2 separate chains of 2 LED each

#### USB to Serial

Use classic USB B connector to connect USB to FT232XS chip. This is hardest part to solder at board so I recommend Solder this chip first. This module just convert USB to 3.3 V UART. Can be use for wiring up ESP-01 for programming.

#### Screw Terminals

Just 6 screw terminals to connecting external circuits to current project.

#### Trimer

Some times You need add trimer to your design. Trimer can be easily connected to Vcc and GND so it is voltage divider across Vcc.

#### Encoder

Few times I use these "endless trimers" to make some UI elements. It is good choice for menu browsing and adjust scalar values. I use basic circuit with just 2 pull-ups.

#### ESP

This module was create due some free space at board near screw. It is just breakout for ESP-01 ESP8266 module. Which is really cheap Wi-fi module with 32-bit MCU and 4 M of memory.