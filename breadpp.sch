<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="1" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Hidden" color="15" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Changes" color="12" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="yes" active="yes"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="14" fill="1" visible="yes" active="yes"/>
<layer number="232" name="Eagle3D_PG2" color="14" fill="2" visible="yes" active="yes"/>
<layer number="233" name="Eagle3D_PG3" color="14" fill="4" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="cooling" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ph-oth">
<description>&lt;h2&gt;PetrH's Other Library&lt;/h2&gt;
&lt;h4&gt;© 2012 Petr Havlíček&lt;/h4&gt;
&lt;p&gt;
High quality hand made libraries by Petr Havlíček. Land patterns and silkscreens are based on IPC-7351 through Mentor Graphics IPC-7351 LP Viewer.&lt;/p&gt;</description>
<packages>
<package name="SW-TACT">
<circle x="0" y="0" radius="1.75" width="0.2032" layer="21"/>
<pad name="1" x="-3.25" y="2.25" drill="1" diameter="1.778" shape="octagon"/>
<pad name="3" x="-3.25" y="-2.25" drill="1" diameter="1.778" shape="octagon"/>
<pad name="2" x="3.25" y="2.25" drill="1" diameter="1.778" shape="octagon"/>
<pad name="4" x="3.25" y="-2.25" drill="1" diameter="1.778" shape="octagon"/>
<wire x1="-3" y1="1" x2="-3" y2="-1" width="0.2032" layer="21"/>
<wire x1="-2.2" y1="3" x2="2.2" y2="3" width="0.2032" layer="21"/>
<wire x1="3" y1="1" x2="3" y2="-1" width="0.2032" layer="21"/>
<wire x1="2.2" y1="-3" x2="-2.2" y2="-3" width="0.2032" layer="21"/>
<text x="-3.81" y="3.937" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-5.207" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="SW-KS01-B">
<description>http://www.gme.cz/tlacitkovy-spinac-p-dt6sw-p630-048</description>
<pad name="1" x="-2.5" y="2.5" drill="1" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="-2.5" y="-2.5" drill="1" diameter="1.778" shape="octagon"/>
<pad name="2" x="2.5" y="2.5" drill="1" diameter="1.778" shape="octagon"/>
<pad name="4" x="2.5" y="-2.5" drill="1" diameter="1.778" shape="octagon"/>
<text x="-6.35" y="7.747" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.35" y="6.223" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="4.5" width="0.254" layer="51"/>
<wire x1="5.4" y1="2.69" x2="5.4" y2="-2.69" width="0.254" layer="21"/>
<wire x1="5.4" y1="2.69" x2="5.4" y2="-2.69" width="0.254" layer="21" curve="306.869898"/>
</package>
<package name="ENCODER">
<pad name="A" x="-2.5" y="-7.5" drill="1" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="C" x="0" y="-7.5" drill="1" diameter="1.6764" shape="octagon" rot="R90"/>
<pad name="B" x="2.5" y="-7.5" drill="1" diameter="1.6764" shape="octagon" rot="R90"/>
<text x="9.271" y="-8.382" size="1.27" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<text x="-5.842" y="6.731" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<pad name="P$1" x="-5.6" y="0" drill="2" shape="octagon"/>
<pad name="P$2" x="5.6" y="0" drill="2" shape="octagon"/>
<wire x1="-6.2" y1="6.6" x2="6.2" y2="6.6" width="0.254" layer="21"/>
<wire x1="6.2" y1="-6.35" x2="-6.2" y2="-6.35" width="0.254" layer="21"/>
<wire x1="-6.2" y1="-6.35" x2="-6.2" y2="-1.905" width="0.254" layer="21"/>
<wire x1="-6.2" y1="1.905" x2="-6.2" y2="6.6" width="0.254" layer="21"/>
<wire x1="6.2" y1="1.905" x2="6.2" y2="6.6" width="0.254" layer="21"/>
<wire x1="6.2" y1="-6.35" x2="6.2" y2="-1.905" width="0.254" layer="21"/>
</package>
<package name="MOD-ESP8266-01">
<wire x1="-5.08" y1="0" x2="-5.08" y2="5.08" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="5.08" x2="5.08" y2="5.08" width="0.2032" layer="21"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="0" x2="5.08" y2="0" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="3.81" drill="1.016" diameter="1.778" shape="square"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="3" x="-1.27" y="3.81" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="5" x="1.27" y="3.81" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="7" x="3.81" y="3.81" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<text x="-5.08" y="5.715" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-1.905" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="52"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="52"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="52"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="52"/>
<rectangle x1="3.556" y1="3.556" x2="4.064" y2="4.064" layer="52"/>
<rectangle x1="1.016" y1="3.556" x2="1.524" y2="4.064" layer="52"/>
<rectangle x1="-1.524" y1="3.556" x2="-1.016" y2="4.064" layer="52"/>
<rectangle x1="-4.064" y1="3.556" x2="-3.556" y2="4.064" layer="52"/>
<rectangle x1="-5.08" y1="2.38125" x2="-2.38125" y2="2.69875" layer="21"/>
<rectangle x1="-2.69875" y1="2.38125" x2="-2.38125" y2="5.08" layer="21"/>
<rectangle x1="-5.08" y1="4.92125" x2="-2.38125" y2="5.08" layer="21"/>
<rectangle x1="-5.08" y1="2.38125" x2="-4.92125" y2="5.08" layer="21"/>
<wire x1="7.15" y1="-0.3" x2="7.15" y2="17.5" width="0.2032" layer="51"/>
<wire x1="7.15" y1="17.5" x2="7.15" y2="24.5" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="-0.3" x2="-7.15" y2="17.5" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="17.5" x2="-7.15" y2="24.5" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="24.5" x2="7.15" y2="24.5" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="-0.3" x2="7.15" y2="-0.3" width="0.2032" layer="51"/>
<wire x1="-7.15" y1="17.5" x2="7.15" y2="17.5" width="0.2032" layer="51"/>
<wire x1="5.715" y1="23.495" x2="1.905" y2="23.495" width="0.6096" layer="51"/>
<wire x1="1.905" y1="23.495" x2="1.905" y2="19.685" width="0.6096" layer="51"/>
<wire x1="1.905" y1="19.685" x2="0.635" y2="19.685" width="0.6096" layer="51"/>
<wire x1="0.635" y1="19.685" x2="0.635" y2="23.495" width="0.6096" layer="51"/>
<wire x1="0.635" y1="23.495" x2="-0.635" y2="23.495" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="23.495" x2="-0.635" y2="19.685" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="19.685" x2="-1.905" y2="19.685" width="0.6096" layer="51"/>
<wire x1="-1.905" y1="19.685" x2="-1.905" y2="23.495" width="0.6096" layer="51"/>
<wire x1="-1.905" y1="23.495" x2="-3.175" y2="23.495" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="23.495" x2="-3.175" y2="19.685" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="19.685" x2="-4.445" y2="19.685" width="0.6096" layer="51"/>
<wire x1="-4.445" y1="19.685" x2="-4.445" y2="23.495" width="0.6096" layer="51"/>
<wire x1="-4.445" y1="23.495" x2="-5.715" y2="23.495" width="0.6096" layer="51"/>
<wire x1="-5.715" y1="23.495" x2="-5.715" y2="18.415" width="0.6096" layer="51"/>
<wire x1="-5.715" y1="18.415" x2="1.905" y2="18.415" width="0.6096" layer="51"/>
<wire x1="-5.715" y1="18.415" x2="-5.715" y2="17.145" width="0.6096" layer="51"/>
</package>
<package name="HS-V68-50SA">
<description>http://www.gme.cz/v68-50sa-p620-059</description>
<wire x1="-23.25" y1="25.25" x2="-23.25" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="-23.25" y1="-25.25" x2="-20.06" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="-20.06" y1="-25.25" x2="-15.24" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="-25.25" x2="-7.62" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="-7.62" y1="-25.25" x2="0" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="0" y1="-25.25" x2="7.62" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="7.62" y1="-25.25" x2="15.24" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="15.24" y1="-25.25" x2="20.06" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="20.06" y1="-25.25" x2="23.25" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="23.25" y1="-25.25" x2="23.25" y2="25.25" width="0.3048" layer="21"/>
<hole x="-17.65" y="19.65" drill="3.2"/>
<hole x="-17.65" y="-19.65" drill="3.2"/>
<hole x="17.65" y="19.65" drill="3.2"/>
<hole x="17.65" y="-19.65" drill="3.2"/>
<wire x1="23.25" y1="25.25" x2="20.06" y2="25.25" width="0.3048" layer="21"/>
<wire x1="20.06" y1="25.25" x2="15.24" y2="25.25" width="0.3048" layer="21"/>
<wire x1="15.24" y1="25.25" x2="7.62" y2="25.25" width="0.3048" layer="21"/>
<wire x1="7.62" y1="25.25" x2="0" y2="25.25" width="0.3048" layer="21"/>
<wire x1="0" y1="25.25" x2="-7.62" y2="25.25" width="0.3048" layer="21"/>
<wire x1="-7.62" y1="25.25" x2="-15.24" y2="25.25" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="25.25" x2="-20.06" y2="25.25" width="0.3048" layer="21"/>
<wire x1="-20.06" y1="25.25" x2="-23.25" y2="25.25" width="0.3048" layer="21"/>
<wire x1="0" y1="25.25" x2="0" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="-7.62" y1="25.25" x2="-7.62" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="7.62" y1="25.25" x2="7.62" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="-15.24" y1="25.25" x2="-15.24" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="15.24" y1="25.25" x2="15.24" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="-20.06" y1="25.25" x2="-20.06" y2="-25.25" width="0.3048" layer="21"/>
<wire x1="20.06" y1="25.25" x2="20.06" y2="-25.25" width="0.3048" layer="21"/>
</package>
<package name="HS-SK09">
<wire x1="-14.5" y1="-2.5" x2="14.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-6" y1="0" x2="6" y2="0" width="0.127" layer="21"/>
<wire x1="-14.35" y1="8.5" x2="-13.35" y2="8.5" width="0.127" layer="21" curve="-180"/>
<wire x1="-10.825" y1="8.5" x2="-9.825" y2="8.5" width="0.127" layer="21" curve="-180"/>
<wire x1="-7.3" y1="8.5" x2="-6.3" y2="8.5" width="0.127" layer="21" curve="-180"/>
<wire x1="7.3" y1="8.5" x2="6.3" y2="8.5" width="0.127" layer="21" curve="180"/>
<wire x1="10.825" y1="8.5" x2="9.825" y2="8.5" width="0.127" layer="21" curve="180"/>
<wire x1="14.35" y1="8.5" x2="13.35" y2="8.5" width="0.127" layer="21" curve="180"/>
<wire x1="-13.0875" y1="1" x2="-11.0875" y2="1" width="0.127" layer="21" curve="180"/>
<wire x1="-9.5625" y1="1" x2="-7.5625" y2="1" width="0.127" layer="21" curve="180"/>
<wire x1="9.5625" y1="1" x2="7.5625" y2="1" width="0.127" layer="21" curve="-180"/>
<wire x1="13.0875" y1="1" x2="11.0875" y2="1" width="0.127" layer="21" curve="-180"/>
<wire x1="13.0875" y1="1" x2="13.35" y2="8.5" width="0.127" layer="21"/>
<wire x1="10.825" y1="8.5" x2="11.0875" y2="1" width="0.127" layer="21"/>
<wire x1="9.825" y1="8.5" x2="9.5625" y2="1" width="0.127" layer="21"/>
<wire x1="7.5625" y1="1" x2="7.3" y2="8.5" width="0.127" layer="21"/>
<wire x1="6.3" y1="8.5" x2="6" y2="0" width="0.127" layer="21"/>
<wire x1="14.5" y1="-2.5" x2="14.35" y2="8.5" width="0.127" layer="21"/>
<wire x1="-14.35" y1="8.5" x2="-14.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-13.35" y1="8.5" x2="-13.0875" y2="1" width="0.127" layer="21"/>
<wire x1="-10.825" y1="8.5" x2="-11.0875" y2="1" width="0.127" layer="21"/>
<wire x1="-9.825" y1="8.5" x2="-9.5625" y2="1" width="0.127" layer="21"/>
<wire x1="-7.5625" y1="1" x2="-7.3" y2="8.5" width="0.127" layer="21"/>
<wire x1="-6.3" y1="8.5" x2="-6" y2="0" width="0.127" layer="21"/>
</package>
<package name="HS-ICK35SA">
<wire x1="-8.05" y1="0" x2="8.05" y2="0" width="0.127" layer="21"/>
<wire x1="-8.05" y1="0" x2="-8.05" y2="15" width="0.127" layer="21"/>
<wire x1="8.05" y1="0" x2="8.05" y2="15" width="0.127" layer="21"/>
<wire x1="9.25" y1="-1.2" x2="-9.25" y2="-1.2" width="0.127" layer="21"/>
<wire x1="-9.25" y1="-1.2" x2="-9.25" y2="15" width="0.127" layer="21"/>
<wire x1="9.25" y1="-1.2" x2="9.25" y2="15" width="0.127" layer="21"/>
<wire x1="-9.25" y1="15" x2="-8.05" y2="15" width="0.127" layer="21"/>
<wire x1="8.05" y1="15" x2="9.25" y2="15" width="0.127" layer="21"/>
</package>
<package name="LOGO-PH-TOP">
<description>&lt;h1&gt;PetrH - Logo&lt;/h1&gt;</description>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-0.635" width="0.3048" layer="29"/>
<wire x1="-4.445" y1="-0.635" x2="-3.175" y2="-0.635" width="0.3048" layer="29"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.27" width="0.3048" layer="29"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.3048" layer="29"/>
<wire x1="4.445" y1="-0.635" x2="-1.27" y2="-0.635" width="0.3048" layer="29"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.3048" layer="29"/>
<wire x1="-1.27" y1="-1.27" x2="4.445" y2="-1.27" width="0.3048" layer="29"/>
<wire x1="4.445" y1="-1.27" x2="4.445" y2="-0.635" width="0.3048" layer="29"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-2.54" width="0.3048" layer="29"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.3048" layer="29"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.3048" layer="29"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.3048" layer="29"/>
<wire x1="0" y1="5.08" x2="0.635" y2="5.08" width="0.3048" layer="29"/>
<wire x1="0.635" y1="5.08" x2="0.635" y2="2.54" width="0.3048" layer="29"/>
<wire x1="0.635" y1="2.54" x2="2.54" y2="2.54" width="0.3048" layer="29"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.3048" layer="29"/>
<wire x1="2.54" y1="0" x2="1.905" y2="0" width="0.3048" layer="29"/>
<wire x1="1.905" y1="0" x2="1.905" y2="1.905" width="0.3048" layer="29"/>
<wire x1="1.905" y1="1.905" x2="0.635" y2="1.905" width="0.3048" layer="29"/>
<wire x1="0.635" y1="1.905" x2="0.635" y2="0" width="0.3048" layer="29"/>
<wire x1="0.635" y1="0" x2="0" y2="0" width="0.3048" layer="29"/>
<wire x1="0" y1="0" x2="-1.905" y2="0" width="0.3048" layer="29"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-2.54" width="0.3048" layer="29"/>
<wire x1="-1.905" y1="1.905" x2="-0.635" y2="1.905" width="0.3048" layer="29"/>
<wire x1="-0.635" y1="1.905" x2="-0.635" y2="0.635" width="0.3048" layer="29"/>
<wire x1="-0.635" y1="0.635" x2="-1.905" y2="0.635" width="0.3048" layer="29"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="1.905" width="0.3048" layer="29"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.3048" layer="29"/>
<rectangle x1="0" y1="0" x2="0.635" y2="5.08" layer="29"/>
<rectangle x1="1.905" y1="0" x2="2.54" y2="2.54" layer="29"/>
<rectangle x1="0.635" y1="1.905" x2="1.905" y2="2.54" layer="29"/>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-0.635" width="0.3048" layer="1"/>
<wire x1="-4.445" y1="-0.635" x2="-3.175" y2="-0.635" width="0.3048" layer="1"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.27" width="0.3048" layer="1"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.3048" layer="1"/>
<wire x1="4.445" y1="-0.635" x2="-1.27" y2="-0.635" width="0.3048" layer="1"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.3048" layer="1"/>
<wire x1="-1.27" y1="-1.27" x2="4.445" y2="-1.27" width="0.3048" layer="1"/>
<wire x1="4.445" y1="-1.27" x2="4.445" y2="-0.635" width="0.3048" layer="1"/>
<wire x1="-1.905" y1="-2.54" x2="-2.54" y2="-2.54" width="0.3048" layer="1"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="2.54" width="0.3048" layer="1"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.3048" layer="1"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.3048" layer="1"/>
<wire x1="0" y1="5.08" x2="0.635" y2="5.08" width="0.3048" layer="1"/>
<wire x1="0.635" y1="5.08" x2="0.635" y2="2.54" width="0.3048" layer="1"/>
<wire x1="0.635" y1="2.54" x2="2.54" y2="2.54" width="0.3048" layer="1"/>
<wire x1="2.54" y1="2.54" x2="2.54" y2="0" width="0.3048" layer="1"/>
<wire x1="2.54" y1="0" x2="1.905" y2="0" width="0.3048" layer="1"/>
<wire x1="1.905" y1="0" x2="1.905" y2="1.905" width="0.3048" layer="1"/>
<wire x1="1.905" y1="1.905" x2="0.635" y2="1.905" width="0.3048" layer="1"/>
<wire x1="0.635" y1="1.905" x2="0.635" y2="0" width="0.3048" layer="1"/>
<wire x1="0.635" y1="0" x2="0" y2="0" width="0.3048" layer="1"/>
<wire x1="0" y1="0" x2="-1.905" y2="0" width="0.3048" layer="1"/>
<wire x1="-1.905" y1="0" x2="-1.905" y2="-2.54" width="0.3048" layer="1"/>
<wire x1="-1.905" y1="1.905" x2="-0.635" y2="1.905" width="0.3048" layer="1"/>
<wire x1="-0.635" y1="1.905" x2="-0.635" y2="0.635" width="0.3048" layer="1"/>
<wire x1="-0.635" y1="0.635" x2="-1.905" y2="0.635" width="0.3048" layer="1"/>
<wire x1="-1.905" y1="0.635" x2="-1.905" y2="1.905" width="0.3048" layer="1"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.3048" layer="1"/>
<rectangle x1="0" y1="0" x2="0.635" y2="5.08" layer="1"/>
<rectangle x1="1.905" y1="0" x2="2.54" y2="2.54" layer="1"/>
<rectangle x1="0.635" y1="1.905" x2="1.905" y2="2.54" layer="1"/>
</package>
<package name="LOGO-PH-SILK">
<description>&lt;h1&gt;PetrH - Logo&lt;/h1&gt;</description>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0" y1="2.54" x2="-2.54" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.3048" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="-1.905" y2="-2.54" width="0.3048" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-1.905" y2="0" width="0.3048" layer="21"/>
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.3048" layer="21"/>
<wire x1="-1.905" y1="0.635" x2="-0.635" y2="0.635" width="0.3048" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="1.905" width="0.3048" layer="21"/>
<wire x1="-0.635" y1="1.905" x2="-1.905" y2="1.905" width="0.3048" layer="21"/>
<wire x1="-1.905" y1="1.905" x2="-1.905" y2="0.635" width="0.3048" layer="21"/>
<wire x1="0" y1="0" x2="0.635" y2="0" width="0.3048" layer="21"/>
<wire x1="0.635" y1="0" x2="0.635" y2="1.905" width="0.3048" layer="21"/>
<wire x1="0.635" y1="1.905" x2="1.905" y2="1.905" width="0.3048" layer="21"/>
<wire x1="1.905" y1="1.905" x2="1.905" y2="0" width="0.3048" layer="21"/>
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.3048" layer="21"/>
<wire x1="2.54" y1="0" x2="2.54" y2="2.54" width="0.3048" layer="21"/>
<wire x1="2.54" y1="2.54" x2="0.635" y2="2.54" width="0.3048" layer="21"/>
<wire x1="0.635" y1="2.54" x2="0.635" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0.635" y1="5.08" x2="0" y2="5.08" width="0.3048" layer="21"/>
<wire x1="0" y1="5.08" x2="0" y2="2.54" width="0.3048" layer="21"/>
<wire x1="-4.445" y1="-0.635" x2="-3.175" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-3.175" y1="-0.635" x2="-3.175" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-4.445" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="4.445" y2="-1.27" width="0.3048" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="4.445" y2="-0.635" width="0.3048" layer="21"/>
<wire x1="4.445" y1="-0.635" x2="-1.27" y2="-0.635" width="0.3048" layer="21"/>
<rectangle x1="0" y1="0" x2="0.635" y2="5.08" layer="21"/>
<rectangle x1="1.905" y1="0" x2="2.54" y2="2.54" layer="21"/>
<rectangle x1="0.635" y1="1.905" x2="1.905" y2="2.54" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="A4P-LOC">
<wire x1="139.065" y1="24.13" x2="180.34" y2="24.13" width="0.1016" layer="94"/>
<wire x1="165.1" y1="3.81" x2="165.1" y2="8.89" width="0.1016" layer="94"/>
<wire x1="165.1" y1="8.89" x2="180.34" y2="8.89" width="0.1016" layer="94"/>
<wire x1="165.1" y1="8.89" x2="139.065" y2="8.89" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="3.81" width="0.1016" layer="94"/>
<wire x1="139.065" y1="8.89" x2="139.065" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="180.34" y2="13.97" width="0.1016" layer="94"/>
<wire x1="139.065" y1="13.97" x2="139.065" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="180.34" y2="19.05" width="0.1016" layer="94"/>
<wire x1="139.065" y1="19.05" x2="139.065" y2="24.13" width="0.1016" layer="94"/>
<text x="140.97" y="20.32" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<text x="140.97" y="10.16" size="2.286" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="154.305" y="5.08" size="2.54" layer="94" font="vector">&gt;SHEET</text>
<text x="140.716" y="4.953" size="2.54" layer="94" font="vector">Sheet:</text>
<text x="140.97" y="15.24" size="2.54" layer="94" font="vector">Petr Havlicek</text>
<frame x1="0" y1="0" x2="184.15" y2="265.43" columns="4" rows="6" layer="94"/>
</symbol>
<symbol name="+3V3">
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96" font="vector" rot="R180" align="center">&gt;VALUE</text>
<pin name="+3V3" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="+5V">
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96" font="vector" rot="R180" align="center">&gt;VALUE</text>
<pin name="+5V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
</symbol>
<symbol name="+12V">
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="+12V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="0" y="-1.27" size="1.778" layer="96" align="center">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="-12V">
<wire x1="1.27" y1="1.905" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.778" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="-12V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="3.81" x2="0" y2="2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="-5V">
<wire x1="1.27" y1="1.905" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.778" layer="96" font="vector" align="center">&gt;VALUE</text>
<pin name="-5V" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="3.81" x2="0" y2="2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="SW-TACT">
<wire x1="1.905" y1="0" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="-1.905" y1="4.445" x2="-1.905" y2="3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="4.445" x2="0" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="4.445" x2="-1.905" y2="4.445" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="4.445" x2="0" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="1.905" y2="1.27" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<circle x="2.54" y="0" radius="0.127" width="0.4064" layer="94"/>
<text x="-10.16" y="-6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="-5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="2"/>
<pin name="S" x="5.08" y="0" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="S1" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="P1" x="-5.08" y="-2.54" visible="pad" length="short" direction="pas" swaplevel="2"/>
</symbol>
<symbol name="ENCODER">
<pin name="C" x="0" y="-7.62" visible="pin" length="short" rot="R90"/>
<pin name="A" x="-2.54" y="-7.62" visible="pin" length="short" rot="R90"/>
<pin name="B" x="2.54" y="-7.62" visible="pin" length="short" rot="R90"/>
<text x="-6.985" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="5.715" y="-6.985" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="-5.08" y1="7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<circle x="0" y="2.54" radius="2.54" width="0.254" layer="94"/>
<polygon width="0.254" layer="94">
<vertex x="-2.54" y="2.54"/>
<vertex x="2.54" y="2.54" curve="-90"/>
<vertex x="0" y="0" curve="-90"/>
</polygon>
</symbol>
<symbol name="VCC">
<wire x1="1.27" y1="1.905" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<text x="0" y="5.08" size="1.778" layer="96" font="vector" rot="R180" align="center">&gt;VALUE</text>
<pin name="VCC" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="2.54" x2="0" y2="3.81" width="0.1524" layer="94"/>
</symbol>
<symbol name="+5V_USB">
<wire x1="1.27" y1="-0.635" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="-0.635" width="0.254" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="96" font="vector" rot="R180" align="center">&gt;VALUE</text>
<pin name="+5V_USB" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
<wire x1="0" y1="0" x2="0" y2="1.27" width="0.1524" layer="94"/>
</symbol>
<symbol name="MOD-ESP8266-01">
<text x="-9.525" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="9.525" y="-8.255" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.254" layer="94"/>
<pin name="VCC" x="-15.24" y="5.08" length="middle"/>
<pin name="CH_PD" x="-15.24" y="0" length="middle"/>
<pin name="RST" x="-15.24" y="2.54" length="middle"/>
<pin name="GND" x="-15.24" y="-5.08" length="middle"/>
<pin name="TX" x="15.24" y="5.08" length="middle" rot="R180"/>
<pin name="RX" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="GPIO0" x="15.24" y="-2.54" length="middle" rot="R180"/>
<pin name="GPIO2" x="15.24" y="-5.08" length="middle" rot="R180"/>
</symbol>
<symbol name="HEATSINK">
<wire x1="-2.54" y1="7.62" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="7.62" y2="-7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="7.62" y2="5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="5.08" x2="7.62" y2="7.62" width="0.254" layer="94"/>
<wire x1="7.62" y1="7.62" x2="-2.54" y2="7.62" width="0.254" layer="94"/>
<text x="-1.905" y="8.255" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.175" y="-6.985" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
</symbol>
<symbol name="LOGO">
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.3048" layer="94"/>
<wire x1="0" y1="5.08" x2="-5.08" y2="5.08" width="0.3048" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="-5.08" width="0.3048" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-3.81" y2="-5.08" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-5.08" x2="-3.81" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="1.27" x2="-1.27" y2="1.27" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="3.81" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="3.81" x2="-3.81" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="3.81" width="0.3048" layer="94"/>
<wire x1="1.27" y1="3.81" x2="3.81" y2="3.81" width="0.3048" layer="94"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="0" width="0.3048" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.3048" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="5.08" width="0.3048" layer="94"/>
<wire x1="5.08" y1="5.08" x2="1.27" y2="5.08" width="0.3048" layer="94"/>
<wire x1="1.27" y1="5.08" x2="1.27" y2="10.16" width="0.3048" layer="94"/>
<wire x1="1.27" y1="10.16" x2="0" y2="10.16" width="0.3048" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="5.08" width="0.3048" layer="94"/>
<wire x1="-6.35" y1="-1.27" x2="-8.89" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="-8.89" y1="-1.27" x2="-8.89" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-8.89" y1="-2.54" x2="-6.35" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-6.35" y1="-2.54" x2="-6.35" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="8.89" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="8.89" y1="-2.54" x2="8.89" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="8.89" y1="-1.27" x2="-2.54" y2="-1.27" width="0.3048" layer="94"/>
<rectangle x1="0" y1="0" x2="1.27" y2="10.16" layer="94"/>
<rectangle x1="3.81" y1="0" x2="5.08" y2="5.08" layer="94"/>
<rectangle x1="1.27" y1="3.81" x2="3.81" y2="5.08" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FRAME-A4-PORTRAIT" prefix="FRAME" uservalue="yes">
<description>&lt;h3&gt;Frame A4&lt;/h3&gt;
&lt;pre&gt;
&lt;b&gt;Paper size:&lt;/b&gt;	A4
&lt;b&gt;Orientation:&lt;/b&gt;	Portrait
&lt;b&gt;Horizontal sec.:&lt;/b&gt;	4
&lt;b&gt;Vertical sec.:&lt;/b&gt;	6
&lt;/pre&gt;</description>
<gates>
<gate name="G$1" symbol="A4P-LOC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+3V3" prefix="+3V3">
<description>Supply symbol for generic 3.3 Volts rail.</description>
<gates>
<gate name="G$1" symbol="+3V3" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V" prefix="P+">
<description>Supply symbol for general +5 Volts rail.</description>
<gates>
<gate name="1" symbol="+5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+12V" prefix="P+">
<description>Supply sumbol for generic +12 Volts rail.</description>
<gates>
<gate name="1" symbol="+12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;Ground Supply symbol&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="-12V" prefix="SUP">
<gates>
<gate name="G$1" symbol="-12V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="-5V" prefix="SUP">
<gates>
<gate name="G$1" symbol="-5V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SW-TACT" prefix="SW" uservalue="yes">
<description>&lt;b&gt;OMRON SWITCH&lt;/b&gt;</description>
<gates>
<gate name="&gt;NAME" symbol="SW-TACT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SW-TACT">
<connects>
<connect gate="&gt;NAME" pin="P" pad="3"/>
<connect gate="&gt;NAME" pin="P1" pad="4"/>
<connect gate="&gt;NAME" pin="S" pad="1"/>
<connect gate="&gt;NAME" pin="S1" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="KS01-B" package="SW-KS01-B">
<connects>
<connect gate="&gt;NAME" pin="P" pad="3"/>
<connect gate="&gt;NAME" pin="P1" pad="4"/>
<connect gate="&gt;NAME" pin="S" pad="1"/>
<connect gate="&gt;NAME" pin="S1" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ENCODER" prefix="ENC">
<gates>
<gate name="G$1" symbol="ENCODER" x="0" y="0"/>
</gates>
<devices>
<device name="" package="ENCODER">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="B" pad="B"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="VCC" prefix="SUP">
<gates>
<gate name="G$1" symbol="VCC" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+5V_USB" prefix="P+">
<description>Supply symbol for 5 Volts delivered from USB.</description>
<gates>
<gate name="G$1" symbol="+5V_USB" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOD-ESP8266-01" prefix="J">
<gates>
<gate name="G$1" symbol="MOD-ESP8266-01" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MOD-ESP8266-01">
<connects>
<connect gate="G$1" pin="CH_PD" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="GPIO0" pad="5"/>
<connect gate="G$1" pin="GPIO2" pad="3"/>
<connect gate="G$1" pin="RST" pad="6"/>
<connect gate="G$1" pin="RX" pad="7"/>
<connect gate="G$1" pin="TX" pad="2"/>
<connect gate="G$1" pin="VCC" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HEATSINK" prefix="HS">
<gates>
<gate name="G$1" symbol="HEATSINK" x="0" y="0"/>
</gates>
<devices>
<device name="V68-50SA" package="HS-V68-50SA">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="SK09" package="HS-SK09">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="ICK35SA" package="HS-ICK35SA">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LOGO" prefix="LOGO">
<description>&lt;h1&gt;PetrH - Logo&lt;/h1&gt;</description>
<gates>
<gate name="G$1" symbol="LOGO" x="0" y="0"/>
</gates>
<devices>
<device name="-TOP" package="LOGO-PH-TOP">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-SILK" package="LOGO-PH-SILK">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ph-con">
<description>&lt;h2&gt;PetrH's Connectors Library&lt;/h2&gt;
&lt;h4&gt;© 2012 Petr Havlíček&lt;/h4&gt;
&lt;p&gt;
High quality hand made libraries by Petr Havlíček. Land patterns and silkscreens are based on IPC-7351 through Mentor Graphics IPC-7351 LP Viewer.&lt;/p&gt;</description>
<packages>
<package name="J2X02">
<wire x1="-2.54" y1="2.54" x2="-2.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="2.54" x2="2.54" y2="2.54" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="2" x="-1.27" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="3" x="1.27" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="4" x="1.27" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<text x="-2.54" y="3.175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-4.445" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-2.54" y1="-0.15875" x2="0.15875" y2="0.15875" layer="21"/>
<rectangle x1="-0.15875" y1="-2.54" x2="0.15875" y2="0.15875" layer="21"/>
<rectangle x1="-2.54" y1="-2.54" x2="0.15875" y2="-2.38125" layer="21"/>
<rectangle x1="-2.54" y1="-2.54" x2="-2.38125" y2="0.15875" layer="21"/>
</package>
<package name="J2X04">
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-2.54" x2="5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="-1.27" drill="1.016" diameter="1.778" shape="square"/>
<pad name="2" x="-3.81" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="3" x="-1.27" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="4" x="-1.27" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="5" x="1.27" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="6" x="1.27" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="7" x="3.81" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="8" x="3.81" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<text x="-5.08" y="3.175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.445" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-5.08" y1="-0.15875" x2="-2.38125" y2="0.15875" layer="21"/>
<rectangle x1="-2.69875" y1="-2.54" x2="-2.38125" y2="0.15875" layer="21"/>
<rectangle x1="-5.08" y1="-2.54" x2="-2.38125" y2="-2.38125" layer="21"/>
<rectangle x1="-5.08" y1="-2.54" x2="-4.92125" y2="0.15875" layer="21"/>
</package>
<package name="USB-B-F-TH">
<text x="1.27" y="-9.525" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="1.27" y="-10.795" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="1" x="-1.25" y="-4.35" drill="0.9" shape="octagon"/>
<pad name="4" x="-1.25" y="-6.35" drill="0.9" shape="octagon"/>
<pad name="2" x="1.25" y="-4.35" drill="0.9" shape="octagon"/>
<pad name="3" x="1.25" y="-6.35" drill="0.9" shape="octagon"/>
<wire x1="-6.02" y1="8.64" x2="6.02" y2="8.64" width="0.2032" layer="51"/>
<wire x1="-6.02" y1="-7.67" x2="6.02" y2="-7.67" width="0.2032" layer="21"/>
<wire x1="6.02" y1="6.64" x2="6.02" y2="0.35" width="0.2032" layer="21"/>
<wire x1="6.02" y1="-3.65" x2="6.02" y2="-7.67" width="0.2032" layer="21"/>
<wire x1="-6.02" y1="6.64" x2="-6.02" y2="0.35" width="0.2032" layer="21"/>
<wire x1="-6.02" y1="-3.65" x2="-6.02" y2="-7.67" width="0.2032" layer="21"/>
<wire x1="-6.02" y1="6.64" x2="6.02" y2="6.64" width="0.2032" layer="21"/>
<wire x1="-6.02" y1="8.64" x2="-6.02" y2="6.64" width="0.2032" layer="51"/>
<wire x1="6.02" y1="8.64" x2="6.02" y2="6.64" width="0.2032" layer="51"/>
<pad name="SH1" x="-6.02" y="-1.64" drill="2.3" shape="octagon"/>
<pad name="SH2" x="6.02" y="-1.64" drill="2.3" shape="octagon"/>
<text x="0" y="3.81" size="1.27" layer="51" font="vector" ratio="10" align="center">USB</text>
</package>
<package name="J1X04">
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="1.016" diameter="1.778" shape="square" rot="R90"/>
<pad name="2" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-5.1562" y="1.8288" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-5.08" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="0.635" width="0.2032" layer="21"/>
</package>
<package name="J1X08">
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="0.635" x2="-6.985" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="1.27" x2="-5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="1.27" x2="-5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-0.635" x2="-5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-5.715" y1="-1.27" x2="-6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-6.985" y1="-1.27" x2="-7.62" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<pad name="1" x="-8.89" y="0" drill="1.016" diameter="1.778" shape="square" rot="R90"/>
<pad name="2" x="-6.35" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="-3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="5" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="6" x="3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="7" x="6.35" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-10.2362" y="1.8288" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-10.16" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<rectangle x1="-9.144" y1="-0.254" x2="-8.636" y2="0.254" layer="51"/>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<wire x1="8.255" y1="1.27" x2="9.525" y2="1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="1.27" x2="10.16" y2="0.635" width="0.2032" layer="21"/>
<wire x1="10.16" y1="0.635" x2="10.16" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="10.16" y1="-0.635" x2="9.525" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="8.255" y1="1.27" x2="7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="9.525" y1="-1.27" x2="8.255" y2="-1.27" width="0.2032" layer="21"/>
<pad name="8" x="8.89" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<rectangle x1="8.636" y1="-0.254" x2="9.144" y2="0.254" layer="51"/>
<wire x1="-7.62" y1="0.635" x2="-7.62" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="1.27" x2="-10.16" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="1.27" x2="-10.16" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-10.16" y1="-1.27" x2="-7.62" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="-0.635" width="0.2032" layer="21"/>
</package>
<package name="J1X02">
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.9" diameter="1.778" shape="square" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="0.9" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<wire x1="0" y1="-0.635" x2="0" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0" y1="-1.27" x2="-2.54" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="-2.54" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="1.27" x2="0" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
</package>
<package name="SMT-PAD">
<smd name="P$1" x="0" y="0" dx="3.81" dy="1.27" layer="1" rot="R90"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="BAN-PLUG-4MM">
<pad name="1" x="0" y="0" drill="6" diameter="10"/>
<text x="-5.08" y="5.715" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-6.985" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="5.23634375" width="0.2032" layer="21"/>
</package>
<package name="AK550-2">
<pad name="1" x="-1.75" y="0" drill="1.1" diameter="1.9304" shape="octagon"/>
<pad name="2" x="1.75" y="0" drill="1.1" diameter="1.9304" shape="octagon"/>
<wire x1="-3.55" y1="3.8" x2="-3.55" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-3.55" y1="2.54" x2="-3.55" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="-3.55" y1="-3.75" x2="3.55" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="3.55" y1="-3.75" x2="3.55" y2="2.54" width="0.2032" layer="21"/>
<wire x1="3.55" y1="2.54" x2="3.55" y2="3.8" width="0.2032" layer="21"/>
<wire x1="3.55" y1="3.8" x2="0" y2="3.8" width="0.2032" layer="21"/>
<wire x1="0" y1="3.8" x2="-3.55" y2="3.8" width="0.2032" layer="21"/>
<wire x1="0" y1="3.8" x2="0" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0" y1="2.54" x2="-3.55" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0" y1="2.54" x2="3.55" y2="2.54" width="0.2032" layer="21"/>
<text x="-3.2512" y="-5.1562" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="5.08" y="-3.81" size="1.27" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
</package>
<package name="AKZ120-2">
<pad name="1" x="-2.54" y="0" drill="1.3" diameter="2.54" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.3" diameter="2.54" shape="octagon"/>
<wire x1="-5.08" y1="4" x2="-5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="2.54" x2="-5.08" y2="-5" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-5" x2="5.08" y2="-5" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-5" x2="5.08" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="4" width="0.2032" layer="21"/>
<wire x1="5.08" y1="4" x2="0" y2="4" width="0.2032" layer="21"/>
<wire x1="0" y1="4" x2="-5.08" y2="4" width="0.2032" layer="21"/>
<wire x1="0" y1="4" x2="0" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0" y1="2.54" x2="-5.08" y2="2.54" width="0.2032" layer="21"/>
<wire x1="0" y1="2.54" x2="5.08" y2="2.54" width="0.2032" layer="21"/>
<text x="-6.2738" y="-3.2512" size="1.27" layer="25" font="vector" ratio="10" rot="R90">&gt;NAME</text>
<text x="7.62" y="-3.81" size="1.27" layer="27" font="vector" ratio="10" rot="R90">&gt;VALUE</text>
<wire x1="5.08" y1="-1.27" x2="5.78" y2="-0.77" width="0.127" layer="21"/>
<wire x1="5.78" y1="-0.77" x2="5.78" y2="-3.04" width="0.127" layer="21"/>
<wire x1="5.78" y1="-3.04" x2="5.08" y2="-2.54" width="0.127" layer="21"/>
<circle x="-2.54" y="-3.175" radius="1" width="0.3048" layer="21"/>
<circle x="2.54" y="-3.175" radius="1" width="0.3048" layer="21"/>
</package>
<package name="J1X06">
<wire x1="5.08" y1="0.635" x2="5.715" y2="1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="1.27" x2="6.985" y2="1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="1.27" x2="7.62" y2="0.635" width="0.2032" layer="21"/>
<wire x1="7.62" y1="0.635" x2="7.62" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="7.62" y1="-0.635" x2="6.985" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="6.985" y1="-1.27" x2="5.715" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="5.715" y1="-1.27" x2="5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="2.54" y1="0.635" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="1.27" x2="5.08" y2="0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="0.635" x2="5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="5.08" y1="-0.635" x2="4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="4.445" y1="-1.27" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.905" y1="-1.27" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="0" y2="0.635" width="0.2032" layer="21"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-5.08" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="0.635" x2="-4.445" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="1.27" x2="-3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="1.27" x2="-2.54" y2="0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-3.175" y1="-1.27" x2="-4.445" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-4.445" y1="-1.27" x2="-5.08" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="1.016" diameter="1.778" shape="square" rot="R90"/>
<pad name="2" x="-3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="-1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="4" x="1.27" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="5" x="3.81" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="6" x="6.35" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-7.6962" y="1.8288" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-7.62" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.254" x2="6.604" y2="0.254" layer="51"/>
<rectangle x1="3.556" y1="-0.254" x2="4.064" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="-4.064" y1="-0.254" x2="-3.556" y2="0.254" layer="51"/>
<rectangle x1="-6.604" y1="-0.254" x2="-6.096" y2="0.254" layer="51"/>
<wire x1="-5.08" y1="-0.635" x2="-5.08" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="-1.27" x2="-7.62" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="-1.27" x2="-7.62" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-7.62" y1="1.27" x2="-5.08" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-5.08" y1="1.27" x2="-5.08" y2="0.635" width="0.2032" layer="21"/>
</package>
<package name="J1X03">
<wire x1="1.27" y1="0.635" x2="1.905" y2="1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="1.27" x2="3.175" y2="1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="1.27" x2="3.81" y2="0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="0.635" x2="3.81" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="3.81" y1="-0.635" x2="3.175" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="3.175" y1="-1.27" x2="1.905" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.2032" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.778" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.778" shape="octagon" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="1.27" x2="-3.81" y2="1.27" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.2032" layer="21"/>
<wire x1="-1.27" y1="-1.27" x2="-1.27" y2="-0.635" width="0.2032" layer="21"/>
</package>
<package name="BAN-PLUG-2MM">
<pad name="1" x="0" y="0" drill="4" diameter="8"/>
<text x="-5.08" y="4.445" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.715" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<circle x="0" y="0" radius="4.2" width="0.2032" layer="21"/>
</package>
<package name="J2X03">
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-2.54" x2="3.81" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="3.81" y1="2.54" x2="3.81" y2="-2.54" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="2.54" x2="3.81" y2="2.54" width="0.2032" layer="21"/>
<pad name="1" x="-2.54" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="2" x="-2.54" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="3" x="0" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="4" x="0" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="5" x="2.54" y="-1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<pad name="6" x="2.54" y="1.27" drill="1.016" diameter="1.778" shape="octagon"/>
<text x="-3.81" y="3.175" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-4.445" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.794" y1="1.016" x2="-2.286" y2="1.524" layer="51"/>
<rectangle x1="-0.254" y1="1.016" x2="0.254" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="1.016" x2="2.794" y2="1.524" layer="51"/>
<rectangle x1="2.286" y1="-1.524" x2="2.794" y2="-1.016" layer="51"/>
<rectangle x1="-0.254" y1="-1.524" x2="0.254" y2="-1.016" layer="51"/>
<rectangle x1="-2.794" y1="-1.524" x2="-2.286" y2="-1.016" layer="51"/>
<rectangle x1="-3.81" y1="-0.15875" x2="-1.11125" y2="0.15875" layer="21"/>
<rectangle x1="-1.42875" y1="-2.54" x2="-1.11125" y2="0.15875" layer="21"/>
<rectangle x1="-3.81" y1="-2.54" x2="-1.11125" y2="-2.38125" layer="21"/>
<rectangle x1="-3.81" y1="-2.54" x2="-3.65125" y2="0.15875" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="PINHD-2X02">
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-4.445" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-4.445" x2="5.08" y2="-4.445" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-4.445" x2="5.08" y2="4.445" width="0.4064" layer="94"/>
<wire x1="5.08" y1="4.445" x2="-5.08" y2="4.445" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<text x="-5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-6.985" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="4" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="PINHD-2X04">
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.4064" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-5.08" x2="-5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="-6.985" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-6.985" x2="5.08" y2="-6.985" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-6.985" x2="5.08" y2="4.445" width="0.4064" layer="94"/>
<wire x1="5.08" y1="4.445" x2="-5.08" y2="4.445" width="0.4064" layer="94"/>
<text x="-5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-9.525" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="4" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="6" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="7" x="-7.62" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="8" x="7.62" y="-5.08" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="USB-B-F">
<wire x1="-5.08" y1="10.16" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-8.89" x2="7.62" y2="-8.89" width="0.254" layer="94"/>
<wire x1="7.62" y1="-8.89" x2="7.62" y2="0" width="0.254" layer="94"/>
<wire x1="7.62" y1="0" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-5.08" y2="10.16" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-1.27" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-2.54" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-3.175" x2="-0.635" y2="-5.715" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-5.715" x2="3.175" y2="-5.715" width="0.254" layer="94"/>
<wire x1="3.175" y1="-5.715" x2="3.175" y2="-3.175" width="0.254" layer="94"/>
<wire x1="3.175" y1="-3.175" x2="-0.635" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="7.62" y2="0" width="0.254" layer="94"/>
<pin name="VBUS" x="-2.54" y="15.24" length="middle" direction="sup" rot="R270"/>
<pin name="D+" x="2.54" y="15.24" length="middle" rot="R270"/>
<pin name="D-" x="0" y="15.24" length="middle" rot="R270"/>
<pin name="GND" x="5.08" y="15.24" length="middle" direction="sup" rot="R270"/>
<text x="-5.715" y="-8.89" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="10.16" y="-8.89" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<wire x1="0" y1="-2.8575" x2="0.635" y2="-2.8575" width="0.254" layer="94"/>
<wire x1="0.635" y1="-2.8575" x2="0.635" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0.635" y1="-3.175" x2="0" y2="-3.175" width="0.254" layer="94"/>
<wire x1="0" y1="-3.175" x2="0" y2="-2.8575" width="0.254" layer="94"/>
<wire x1="1.905" y1="-2.8575" x2="2.54" y2="-2.8575" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.8575" x2="2.54" y2="-3.175" width="0.254" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="1.905" y2="-3.175" width="0.254" layer="94"/>
<wire x1="1.905" y1="-3.175" x2="1.905" y2="-2.8575" width="0.254" layer="94"/>
<wire x1="0" y1="-5.715" x2="0.635" y2="-5.715" width="0.254" layer="94"/>
<wire x1="0.635" y1="-5.715" x2="0.635" y2="-6.0325" width="0.254" layer="94"/>
<wire x1="0.635" y1="-6.0325" x2="0" y2="-6.0325" width="0.254" layer="94"/>
<wire x1="0" y1="-6.0325" x2="0" y2="-5.715" width="0.254" layer="94"/>
<wire x1="1.905" y1="-5.715" x2="2.54" y2="-5.715" width="0.254" layer="94"/>
<wire x1="2.54" y1="-5.715" x2="2.54" y2="-6.0325" width="0.254" layer="94"/>
<wire x1="2.54" y1="-6.0325" x2="1.905" y2="-6.0325" width="0.254" layer="94"/>
<wire x1="1.905" y1="-6.0325" x2="1.905" y2="-5.715" width="0.254" layer="94"/>
</symbol>
<symbol name="PINHD-1X04">
<wire x1="0" y1="0" x2="5.08" y2="0" width="0.4064" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="12.7" width="0.4064" layer="94"/>
<wire x1="5.08" y1="12.7" x2="0" y2="12.7" width="0.4064" layer="94"/>
<wire x1="0" y1="12.7" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="2.54" y1="10.16" x2="1.27" y2="10.16" width="0.4064" layer="94"/>
<wire x1="1.27" y1="10.16" x2="0" y2="10.16" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<text x="-2.54" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-2.54" y="7.62" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-2.54" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-2.54" y="2.54" visible="pad" length="short" direction="pas"/>
<wire x1="2.54" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="PINHD-1X08">
<wire x1="2.54" y1="-10.16" x2="7.62" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="7.62" y2="12.7" width="0.4064" layer="94"/>
<wire x1="7.62" y1="12.7" x2="2.54" y2="12.7" width="0.4064" layer="94"/>
<wire x1="2.54" y1="12.7" x2="2.54" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="5.08" y1="10.16" x2="3.81" y2="10.16" width="0.4064" layer="94"/>
<wire x1="3.81" y1="10.16" x2="2.54" y2="10.16" width="0.1524" layer="94"/>
<wire x1="5.08" y1="7.62" x2="3.81" y2="7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="7.62" x2="2.54" y2="7.62" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="3.81" y2="5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="5.08" x2="2.54" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.4064" layer="94"/>
<wire x1="3.81" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="3.81" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="2.54" y2="-5.08" width="0.1524" layer="94"/>
<text x="0" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-12.7" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="10.16" visible="pad" length="short" direction="pas"/>
<pin name="2" x="0" y="7.62" visible="pad" length="short" direction="pas"/>
<pin name="3" x="0" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="4" x="0" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="5" x="0" y="0" visible="pad" length="short" direction="pas"/>
<pin name="6" x="0" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="7" x="0" y="-5.08" visible="pad" length="short" direction="pas"/>
<pin name="8" x="0" y="-7.62" visible="pad" length="short" direction="pas"/>
<wire x1="5.08" y1="-7.62" x2="3.81" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-7.62" x2="2.54" y2="-7.62" width="0.1524" layer="94"/>
</symbol>
<symbol name="PINHD-1X02">
<wire x1="2.54" y1="-5.08" x2="7.62" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="5.08" width="0.4064" layer="94"/>
<wire x1="7.62" y1="5.08" x2="2.54" y2="5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="5.08" x2="2.54" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<text x="0" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="0" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="2" x="0" y="-2.54" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="PAD">
<wire x1="-3.81" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="-3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<text x="-3.81" y="3.175" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="0" visible="off" length="short" direction="pas" function="dot"/>
</symbol>
<symbol name="PINHD-1X06">
<wire x1="0" y1="-7.62" x2="5.08" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="5.08" y2="10.16" width="0.4064" layer="94"/>
<wire x1="5.08" y1="10.16" x2="0" y2="10.16" width="0.4064" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="2.54" y1="7.62" x2="1.27" y2="7.62" width="0.4064" layer="94"/>
<wire x1="1.27" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="5.08" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-5.08" x2="1.27" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="0" y2="-5.08" width="0.1524" layer="94"/>
<text x="-2.54" y="10.795" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="7.62" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-2.54" y="5.08" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-2.54" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="4" x="-2.54" y="0" visible="pad" length="short" direction="pas"/>
<pin name="5" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="6" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="PINHD-1X03">
<wire x1="0" y1="-5.08" x2="5.08" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="5.08" width="0.4064" layer="94"/>
<wire x1="5.08" y1="5.08" x2="0" y2="5.08" width="0.4064" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="-2.54" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="-7.62" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="2" x="-2.54" y="0" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.4064" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="PINHD-2X03">
<wire x1="2.54" y1="2.54" x2="3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-3.81" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="2.54" y1="0" x2="3.81" y2="0" width="0.4064" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-3.81" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="-3.81" y1="-2.54" x2="-5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="-4.445" width="0.4064" layer="94"/>
<wire x1="-5.08" y1="-4.445" x2="5.08" y2="-4.445" width="0.4064" layer="94"/>
<wire x1="5.08" y1="-4.445" x2="5.08" y2="4.445" width="0.4064" layer="94"/>
<wire x1="5.08" y1="4.445" x2="-5.08" y2="4.445" width="0.4064" layer="94"/>
<text x="-5.08" y="5.08" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-6.985" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-7.62" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="2" x="7.62" y="2.54" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="3" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
<pin name="4" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
<pin name="5" x="-7.62" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="6" x="7.62" y="-2.54" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PINHD-2X02" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-2X02" x="0" y="0"/>
</gates>
<devices>
<device name="" package="J2X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X04" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-2X04" x="0" y="0"/>
</gates>
<devices>
<device name="" package="J2X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="USB-B-F" prefix="CON">
<gates>
<gate name="G$1" symbol="USB-B-F" x="2.54" y="-2.54"/>
</gates>
<devices>
<device name="-TH" package="USB-B-F-TH">
<connects>
<connect gate="G$1" pin="D+" pad="2"/>
<connect gate="G$1" pin="D-" pad="3"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VBUS" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X04" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-1X04" x="0" y="0"/>
</gates>
<devices>
<device name="" package="J1X04">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X08" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-1X08" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="J1X08">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X02" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-1X02" x="-5.08" y="0"/>
</gates>
<devices>
<device name="" package="J1X02">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PAD" prefix="P" uservalue="yes">
<gates>
<gate name="G$1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="-SMT" package="SMT-PAD">
<connects>
<connect gate="G$1" pin="1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-BAN4MM" package="BAN-PLUG-4MM">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-BAN2MM" package="BAN-PLUG-2MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SVOR-2" prefix="CON">
<gates>
<gate name="G$1" symbol="PINHD-1X02" x="-5.08" y="0"/>
</gates>
<devices>
<device name="-AK550-2" package="AK550-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-AKZ120-2" package="AKZ120-2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X06" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-1X06" x="0" y="0"/>
</gates>
<devices>
<device name="" package="J1X06">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X03" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-1X03" x="0" y="0"/>
</gates>
<devices>
<device name="" package="J1X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-2X03" prefix="J">
<gates>
<gate name="G$1" symbol="PINHD-2X03" x="0" y="0"/>
</gates>
<devices>
<device name="" package="J2X03">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ph-pas">
<description>&lt;h2&gt;PetrH's Passive Components Library&lt;/h2&gt;
&lt;h4&gt;© 2012 Petr Havlíček&lt;/h4&gt;
&lt;p&gt;
High quality hand made libraries by Petr Havlíček. Land patterns and silkscreens are based on IPC-7351 through Mentor Graphics IPC-7351 LP Viewer.&lt;/p&gt;</description>
<packages>
<package name="CAPC0805">
<description>&lt;h3&gt;Capacitor SMD 0805&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Ceramic Chip Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 0805
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPC2013X95N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-1.75" y1="1" x2="1.75" y2="1" width="0.2032" layer="21"/>
<wire x1="1.75" y1="1" x2="1.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="1.75" y1="-1" x2="-1.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1" x2="-1.75" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="20"/>
<smd name="2" x="0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="20"/>
<rectangle x1="-0.1016" y1="-0.4572" x2="0.1016" y2="0.4572" layer="35"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.75" y1="1" x2="-1.75" y2="-1" width="0.127" layer="39"/>
<wire x1="-1.75" y1="-1" x2="1.75" y2="-1" width="0.127" layer="39"/>
<wire x1="1.75" y1="-1" x2="1.75" y2="1" width="0.127" layer="39"/>
<wire x1="1.75" y1="1" x2="-1.75" y2="1" width="0.127" layer="39"/>
</package>
<package name="CAPC1206">
<description>&lt;h3&gt;Capacitor SMD 1206&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Ceramic Chip Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 1206
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPC3216X95N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-2.35" y1="1.15" x2="2.35" y2="1.15" width="0.2032" layer="21"/>
<wire x1="2.35" y1="1.15" x2="2.35" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="2.35" y1="-1.15" x2="-2.35" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="-2.35" y1="-1.15" x2="-2.35" y2="1.15" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<smd name="2" x="1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<rectangle x1="-0.2032" y1="-0.635" x2="0.2032" y2="0.635" layer="35"/>
<text x="-2.54" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.35" y1="1.15" x2="-2.35" y2="-1.15" width="0.127" layer="39"/>
<wire x1="-2.35" y1="-1.15" x2="2.35" y2="-1.15" width="0.127" layer="39"/>
<wire x1="2.35" y1="-1.15" x2="2.35" y2="1.15" width="0.127" layer="39"/>
<wire x1="2.35" y1="1.15" x2="-2.35" y2="1.15" width="0.127" layer="39"/>
</package>
<package name="CAPC0603">
<description>&lt;h3&gt;Capacitor SMD 0603&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Ceramic Chip Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 0603
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPC1608X95N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="1.55" y1="0.75" x2="1.55" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="1.55" y1="-0.75" x2="-1.55" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="-0.75" x2="-1.55" y2="0.75" width="0.2032" layer="21"/>
<smd name="1" x="-0.8" y="0" dx="0.95" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0.8" y="0" dx="0.95" dy="1" layer="1" roundness="20"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.55" y1="0.75" x2="1.55" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-1.55" y1="0.75" x2="-1.55" y2="-0.75" width="0.127" layer="39"/>
<wire x1="-1.55" y1="-0.75" x2="1.55" y2="-0.75" width="0.127" layer="39"/>
<wire x1="1.55" y1="-0.75" x2="1.55" y2="0.75" width="0.127" layer="39"/>
<wire x1="1.55" y1="0.75" x2="-1.55" y2="0.75" width="0.127" layer="39"/>
<rectangle x1="-0.1016" y1="-0.2032" x2="0.1016" y2="0.2032" layer="35"/>
</package>
<package name="RESC0805">
<description>&lt;h3&gt;Resistor SMD 0805&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Thin Film Chip Resistor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 0805
&lt;b&gt;IPC Name:&lt;/b&gt;	RESC2013X65N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="1.7" y1="1" x2="1.7" y2="-1" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-1" x2="-1.7" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-1" x2="-1.7" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1" dy="1.45" layer="1" roundness="20"/>
<smd name="2" x="0.95" y="0" dx="1" dy="1.45" layer="1" roundness="20"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.7" y1="1" x2="1.7" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="1" x2="-1.7" y2="-1" width="0.127" layer="39"/>
<wire x1="-1.7" y1="-1" x2="1.7" y2="-1" width="0.127" layer="39"/>
<wire x1="1.7" y1="-1" x2="1.7" y2="1" width="0.127" layer="39"/>
<wire x1="1.7" y1="1" x2="-1.7" y2="1" width="0.127" layer="39"/>
<rectangle x1="-0.1016" y1="-0.4064" x2="0.1016" y2="0.4064" layer="35"/>
</package>
<package name="RESC1206">
<description>&lt;h3&gt;Resistor SMD 1206&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Thin Film Chip Resistor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 1206
&lt;b&gt;IPC Name:&lt;/b&gt;	RESC3216X84N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-2.3" y1="1.15" x2="2.3" y2="1.15" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.15" x2="2.3" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.15" x2="-2.3" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.15" x2="-2.3" y2="1.15" width="0.2032" layer="21"/>
<smd name="2" x="1.5" y="0" dx="1.05" dy="1.75" layer="1" roundness="20"/>
<smd name="1" x="-1.5" y="0" dx="1.05" dy="1.75" layer="1" roundness="20"/>
<text x="-2.54" y="1.3335" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.6035" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2032" y1="-0.635" x2="0.2032" y2="0.635" layer="35"/>
<wire x1="-2.3" y1="1.15" x2="-2.3" y2="-1.15" width="0.127" layer="39"/>
<wire x1="-2.3" y1="-1.15" x2="2.3" y2="-1.15" width="0.127" layer="39"/>
<wire x1="2.3" y1="-1.15" x2="2.3" y2="1.15" width="0.127" layer="39"/>
<wire x1="2.3" y1="1.15" x2="-2.3" y2="1.15" width="0.127" layer="39"/>
</package>
<package name="RESC0603">
<description>&lt;h3&gt;Resistor SMD 0603&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Thin Film Chip Resistor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 0603
&lt;b&gt;IPC Name:&lt;/b&gt;	RESC1608X84N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="0.8" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0.85" y="0" dx="0.8" dy="1" layer="1" roundness="20"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="-0.75" width="0.127" layer="39"/>
<wire x1="-1.5" y1="-0.75" x2="1.5" y2="-0.75" width="0.127" layer="39"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.127" layer="39"/>
<wire x1="1.5" y1="0.75" x2="-1.5" y2="0.75" width="0.127" layer="39"/>
<rectangle x1="-0.1016" y1="-0.2032" x2="0.1016" y2="0.2032" layer="35"/>
</package>
<package name="R-TO-221">
<text x="-5.08" y="2.855" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.125" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="1" x="-2.54" y="0" drill="1.4" shape="long" rot="R90"/>
<pad name="2" x="2.54" y="0" drill="1.4" shape="long" rot="R90"/>
<wire x1="-5.25" y1="2.35" x2="-5.25" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.35" x2="5.25" y2="-2.35" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.35" x2="5.25" y2="2.35" width="0.127" layer="21"/>
<wire x1="5.25" y1="2.35" x2="-5.25" y2="2.35" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-5.25" y="2.34"/>
<vertex x="-5.25" y="1.27"/>
<vertex x="-4.1" y="1.27"/>
<vertex x="-3.1" y="2.34"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="5.25" y="2.34"/>
<vertex x="5.25" y="1.27"/>
<vertex x="4.1" y="1.27"/>
<vertex x="3.1" y="2.34"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-1.98" y="2.34"/>
<vertex x="-0.98" y="1.27"/>
<vertex x="0.98" y="1.27"/>
<vertex x="1.98" y="2.34"/>
</polygon>
</package>
<package name="L-1A">
<smd name="P$1" x="-3.81" y="0" dx="7" dy="4" layer="1" rot="R90"/>
<smd name="P$2" x="3.81" y="0" dx="7" dy="4" layer="1" rot="R90"/>
</package>
<package name="INDC1210">
<description>&lt;h3&gt;Inductor SMD 1210&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Inductor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 1210 
&lt;b&gt;IPC Name:&lt;/b&gt;	INDC3225
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<smd name="1" x="-1.5" y="0" dx="1.15" dy="2.7" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.15" dy="2.7" layer="1"/>
<text x="-2.2225" y="1.905" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.2225" y="-3.175" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.35" y1="1.6" x2="-2.35" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="-2.35" y1="-1.6" x2="2.35" y2="-1.6" width="0.2032" layer="21"/>
<wire x1="2.35" y1="-1.6" x2="2.35" y2="1.6" width="0.2032" layer="21"/>
<wire x1="2.35" y1="1.6" x2="-2.35" y2="1.6" width="0.2032" layer="21"/>
<wire x1="-2.35" y1="1.6" x2="-2.35" y2="-1.6" width="0.2032" layer="39"/>
<wire x1="-2.35" y1="-1.6" x2="2.35" y2="-1.6" width="0.2032" layer="39"/>
<wire x1="2.35" y1="-1.6" x2="2.35" y2="1.6" width="0.2032" layer="39"/>
<wire x1="2.35" y1="1.6" x2="-2.35" y2="1.6" width="0.2032" layer="39"/>
<rectangle x1="-0.3175" y1="-0.635" x2="0.3175" y2="0.635" layer="35"/>
</package>
<package name="INDCDE0704">
<smd name="1" x="-3.2" y="0" dx="1.6" dy="2.2" layer="1"/>
<smd name="2" x="3.2" y="0" dx="1.6" dy="2.2" layer="1"/>
<text x="-3.4925" y="4.445" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.4925" y="-5.715" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-4.4" y1="3.8" x2="-4.4" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="-3.8" x2="4.4" y2="-3.8" width="0.2032" layer="21"/>
<wire x1="4.4" y1="-3.8" x2="4.4" y2="3.8" width="0.2032" layer="21"/>
<wire x1="4.4" y1="3.8" x2="-4.4" y2="3.8" width="0.2032" layer="21"/>
<wire x1="-4.4" y1="3.8" x2="-4.4" y2="-3.8" width="0.2032" layer="39"/>
<wire x1="-4.4" y1="-3.8" x2="4.4" y2="-3.8" width="0.2032" layer="39"/>
<wire x1="4.4" y1="-3.8" x2="4.4" y2="3.8" width="0.2032" layer="39"/>
<wire x1="4.4" y1="3.8" x2="-4.4" y2="3.8" width="0.2032" layer="39"/>
<rectangle x1="-0.635" y1="-1.905" x2="0.635" y2="1.905" layer="35"/>
</package>
<package name="INDC1206">
<wire x1="-2.35" y1="1.15" x2="2.35" y2="1.15" width="0.2032" layer="21"/>
<wire x1="2.35" y1="1.15" x2="2.35" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="2.35" y1="-1.15" x2="-2.35" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="-2.35" y1="-1.15" x2="-2.35" y2="1.15" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<smd name="2" x="1.5" y="0" dx="1.15" dy="1.8" layer="1" roundness="20"/>
<rectangle x1="-0.2032" y1="-0.635" x2="0.2032" y2="0.635" layer="35"/>
<text x="-2.54" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.35" y1="1.15" x2="-2.35" y2="-1.15" width="0.127" layer="39"/>
<wire x1="-2.35" y1="-1.15" x2="2.35" y2="-1.15" width="0.127" layer="39"/>
<wire x1="2.35" y1="-1.15" x2="2.35" y2="1.15" width="0.127" layer="39"/>
<wire x1="2.35" y1="1.15" x2="-2.35" y2="1.15" width="0.127" layer="39"/>
</package>
<package name="INDC0805">
<wire x1="-1.75" y1="1" x2="1.75" y2="1" width="0.2032" layer="21"/>
<wire x1="1.75" y1="1" x2="1.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="1.75" y1="-1" x2="-1.75" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.75" y1="-1" x2="-1.75" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="20"/>
<smd name="2" x="0.9" y="0" dx="1.15" dy="1.45" layer="1" roundness="20"/>
<rectangle x1="-0.1016" y1="-0.4572" x2="0.1016" y2="0.4572" layer="35"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.75" y1="1" x2="-1.75" y2="-1" width="0.127" layer="39"/>
<wire x1="-1.75" y1="-1" x2="1.75" y2="-1" width="0.127" layer="39"/>
<wire x1="1.75" y1="-1" x2="1.75" y2="1" width="0.127" layer="39"/>
<wire x1="1.75" y1="1" x2="-1.75" y2="1" width="0.127" layer="39"/>
</package>
<package name="INDCDC0604">
<smd name="1" x="-2.15" y="0" dx="2.7" dy="5.8" layer="1"/>
<smd name="2" x="2.15" y="0" dx="2.7" dy="5.8" layer="1"/>
<text x="-3.4925" y="4.445" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.4925" y="-5.715" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-3.7" y1="3.1" x2="-3.7" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="-3.1" x2="3.7" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="3.7" y1="-3.1" x2="3.7" y2="3.1" width="0.2032" layer="21"/>
<wire x1="3.7" y1="3.1" x2="-3.7" y2="3.1" width="0.2032" layer="21"/>
<wire x1="-3.7" y1="3.1" x2="-3.7" y2="-3.1" width="0.2032" layer="39"/>
<wire x1="-3.7" y1="-3.1" x2="3.7" y2="-3.1" width="0.2032" layer="39"/>
<wire x1="3.7" y1="-3.1" x2="3.7" y2="3.1" width="0.2032" layer="39"/>
<wire x1="3.7" y1="3.1" x2="-3.7" y2="3.1" width="0.2032" layer="39"/>
<rectangle x1="-0.2" y1="-1.905" x2="0.2" y2="1.905" layer="35"/>
</package>
<package name="INDC-WE-PD-SMD">
<smd name="1" x="-4.95" y="0" dx="2.9" dy="5.4" layer="1"/>
<smd name="2" x="4.95" y="0" dx="2.9" dy="5.4" layer="1"/>
<text x="-6.0325" y="6.985" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-6.0325" y="-8.255" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-6" y1="-6" x2="6" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="6" x2="-6" y2="6" width="0.2032" layer="21"/>
<wire x1="-6" y1="6" x2="-6" y2="-6" width="0.2032" layer="39"/>
<wire x1="-6" y1="-6" x2="6" y2="-6" width="0.2032" layer="39"/>
<wire x1="6" y1="-6" x2="6" y2="6" width="0.2032" layer="39"/>
<wire x1="6" y1="6" x2="-6" y2="6" width="0.2032" layer="39"/>
<rectangle x1="-0.635" y1="-1.905" x2="0.635" y2="1.905" layer="35"/>
<wire x1="-6" y1="6" x2="-6" y2="3" width="0.2032" layer="21"/>
<wire x1="-6" y1="-3" x2="-6" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="-3" x2="6" y2="-6" width="0.2032" layer="21"/>
<wire x1="6" y1="6" x2="6" y2="3" width="0.2032" layer="21"/>
</package>
<package name="INDCDE0703">
<smd name="1" x="-3.15" y="0" dx="1.5" dy="2.2" layer="1"/>
<smd name="2" x="3.15" y="0" dx="1.5" dy="2.2" layer="1"/>
<text x="-3.4925" y="4.445" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.4925" y="-5.715" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-3.75" y1="-3.75" x2="3.75" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="3.75" y1="3.75" x2="-3.75" y2="3.75" width="0.2032" layer="21"/>
<wire x1="-3.75" y1="3.75" x2="-3.75" y2="-3.75" width="0.2032" layer="39"/>
<wire x1="-3.75" y1="-3.75" x2="3.75" y2="-3.75" width="0.2032" layer="39"/>
<wire x1="3.75" y1="-3.75" x2="3.75" y2="3.75" width="0.2032" layer="39"/>
<wire x1="3.75" y1="3.75" x2="-3.75" y2="3.75" width="0.2032" layer="39"/>
<rectangle x1="-0.635" y1="-1.905" x2="0.635" y2="1.905" layer="35"/>
<wire x1="-3.75" y1="-1.33" x2="-3.75" y2="-3.75" width="0.2032" layer="21"/>
<wire x1="-3.75" y1="3.75" x2="-3.75" y2="1.33" width="0.2032" layer="21"/>
<wire x1="3.75" y1="1.33" x2="3.75" y2="3.75" width="0.2032" layer="21"/>
<wire x1="3.75" y1="-3.75" x2="3.75" y2="-1.33" width="0.2032" layer="21"/>
</package>
<package name="R-VAR-TC33X">
<wire x1="1.8" y1="1.9" x2="1.8" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="-1.9" x2="-1.8" y2="1.9" width="0.2032" layer="21"/>
<text x="-3.81" y="2.6035" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.8735" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2032" y1="-0.635" x2="0.2032" y2="0.635" layer="35" rot="R90"/>
<wire x1="-1.8" y1="1.9" x2="-1.8" y2="-1.9" width="0.127" layer="39"/>
<wire x1="-1.8" y1="-1.9" x2="1.8" y2="-1.9" width="0.127" layer="39"/>
<wire x1="1.8" y1="-1.9" x2="1.8" y2="1.9" width="0.127" layer="39"/>
<wire x1="1.8" y1="1.9" x2="-1.8" y2="1.9" width="0.127" layer="39"/>
<smd name="1" x="-1" y="-1.8" dx="1.2" dy="1.2" layer="1" rot="R90"/>
<smd name="2" x="1" y="-1.8" dx="1.2" dy="1.2" layer="1" rot="R90"/>
<smd name="3" x="0" y="1.45" dx="1.5" dy="1.6" layer="1" rot="R90"/>
<wire x1="0.2" y1="-1.9" x2="-0.2" y2="-1.9" width="0.2032" layer="21"/>
<wire x1="-1.8" y1="1.9" x2="-1" y2="1.9" width="0.2032" layer="21"/>
<wire x1="1" y1="1.9" x2="1.8" y2="1.9" width="0.2032" layer="21"/>
</package>
<package name="R-VAR-CA14V">
<pad name="1" x="-5" y="-7.5" drill="1.3" shape="octagon"/>
<pad name="3" x="0" y="5" drill="1.3" shape="octagon"/>
<pad name="2" x="5" y="-7.5" drill="1.3" shape="octagon"/>
<wire x1="-7" y1="7" x2="-7" y2="-7" width="0.2032" layer="21"/>
<wire x1="7" y1="-7" x2="7" y2="7" width="0.2032" layer="21"/>
<wire x1="7" y1="7" x2="-7" y2="7" width="0.2032" layer="21"/>
<text x="-7.62" y="7.6835" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="1.27" y="7.5565" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-7" y1="-7" x2="-6.3" y2="-7" width="0.2032" layer="21"/>
<wire x1="-3.81" y1="-7" x2="3.81" y2="-7" width="0.2032" layer="21"/>
<wire x1="6.35" y1="-7" x2="7" y2="-7" width="0.2032" layer="21"/>
<circle x="0" y="0" radius="3.5" width="0.2032" layer="51"/>
<wire x1="-0.508" y1="-3.429" x2="-0.508" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-0.508" y1="1.27" x2="-2.159" y2="1.27" width="0.2032" layer="51"/>
<wire x1="-2.159" y1="1.27" x2="-0.635" y2="2.794" width="0.2032" layer="51"/>
<wire x1="-0.635" y1="2.794" x2="-0.635" y2="3.429" width="0.2032" layer="51"/>
<wire x1="0.508" y1="-3.429" x2="0.508" y2="1.27" width="0.2032" layer="51"/>
<wire x1="0.508" y1="1.27" x2="2.159" y2="1.27" width="0.2032" layer="51"/>
<wire x1="2.159" y1="1.27" x2="0.635" y2="2.794" width="0.2032" layer="51"/>
<wire x1="0.635" y1="2.794" x2="0.635" y2="3.302" width="0.2032" layer="51"/>
<wire x1="-7.112" y1="6.985" x2="-7.112" y2="-6.985" width="0.2032" layer="39"/>
<wire x1="-7.112" y1="-6.985" x2="7.112" y2="-6.985" width="0.2032" layer="39"/>
<wire x1="7.112" y1="-6.985" x2="7.112" y2="6.985" width="0.2032" layer="39"/>
<wire x1="7.112" y1="6.985" x2="-7.112" y2="6.985" width="0.2032" layer="39"/>
</package>
<package name="LED-0805">
<description>&lt;h3&gt;SMD 0805 LED&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	LED
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Size:&lt;/b&gt;	0805
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<wire x1="-1.9304" y1="-0.8636" x2="-1.9304" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.9304" y1="0.8636" x2="1.9304" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="0.8636" x2="1.9304" y2="-0.8636" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="-0.8636" x2="-1.9304" y2="-0.8636" width="0.2032" layer="21"/>
<wire x1="-0.925" y1="-0.35" x2="-0.925" y2="0.35" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="0.925" y1="-0.35" x2="0.925" y2="0.35" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="-0.525" y1="0.575" x2="0.525" y2="0.575" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-0.575" x2="-0.925" y2="-0.575" width="0.1016" layer="51"/>
<circle x="-0.85" y="-0.45" radius="0.0889" width="0.1016" layer="51"/>
<smd name="C" x="-1.0668" y="0" dx="1.2192" dy="1.2192" layer="1" rot="R90"/>
<smd name="A" x="1.0668" y="0" dx="1.2192" dy="1.2192" layer="1" roundness="20" rot="R90"/>
<text x="-2.54" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.9125" y1="0.2125" x2="-0.5875" y2="0.7125" layer="51" rot="R90"/>
<rectangle x1="-0.6855" y1="-0.376" x2="-0.5511" y2="-0.1394" layer="51" rot="R90"/>
<rectangle x1="-0.6855" y1="0.1394" x2="-0.5511" y2="0.376" layer="51" rot="R90"/>
<rectangle x1="-0.7875" y1="-0.0875" x2="-0.3875" y2="0.0875" layer="51" rot="R90"/>
<rectangle x1="0.5875" y1="0.2125" x2="0.9125" y2="0.7125" layer="51" rot="R90"/>
<rectangle x1="0.5875" y1="-0.7125" x2="0.9125" y2="-0.2125" layer="51" rot="R90"/>
<rectangle x1="0.5511" y1="0.1394" x2="0.6855" y2="0.376" layer="51" rot="R90"/>
<rectangle x1="0.5511" y1="-0.376" x2="0.6855" y2="-0.1394" layer="51" rot="R90"/>
<rectangle x1="0.3875" y1="-0.0875" x2="0.7875" y2="0.0875" layer="51" rot="R90"/>
<rectangle x1="-0.2" y1="-0.1" x2="0" y2="0.1" layer="21" rot="R90"/>
<rectangle x1="-0.8" y1="-0.6" x2="-0.5" y2="-0.3" layer="51" rot="R90"/>
<rectangle x1="-1.125" y1="-0.5" x2="-0.8" y2="-0.425" layer="51" rot="R90"/>
<polygon width="0.2032" layer="21">
<vertex x="-1.9304" y="0.8636"/>
<vertex x="-2.4384" y="0.8636"/>
<vertex x="-2.4384" y="-0.8636"/>
<vertex x="-1.9304" y="-0.8636"/>
</polygon>
</package>
<package name="LED-1206">
<description>&lt;h3&gt;SMD 1206 LED&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	LED
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Size:&lt;/b&gt;	1206
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<wire x1="2.794" y1="-1.0414" x2="2.794" y2="1.0414" width="0.2032" layer="21"/>
<wire x1="2.794" y1="1.0414" x2="-2.794" y2="1.0414" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="1.0414" x2="-2.794" y2="-1.0414" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="-1.0414" x2="2.794" y2="-1.0414" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-0.4" x2="-1.6" y2="0.4" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="0.95" y1="-0.8" x2="-0.95" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.8" x2="0.95" y2="0.8" width="0.1016" layer="51"/>
<circle x="-1.425" y="-0.55" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="-1.7526" y="0" dx="1.524" dy="1.524" layer="1" rot="R90"/>
<smd name="A" x="1.7526" y="0" dx="1.524" dy="1.524" layer="1" roundness="20" rot="R90"/>
<text x="-2.54" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.8375" y1="-0.6625" x2="-1.3375" y2="-0.5375" layer="51" rot="R90"/>
<rectangle x1="-1.5" y1="-0.9" x2="-1.275" y2="-0.575" layer="51" rot="R90"/>
<rectangle x1="-1.4" y1="-0.5" x2="-1.275" y2="-0.275" layer="51" rot="R90"/>
<rectangle x1="-1.5" y1="-0.5" x2="-1.075" y2="-0.375" layer="51" rot="R90"/>
<rectangle x1="-1.725" y1="0.425" x2="-1.225" y2="0.775" layer="51" rot="R90"/>
<rectangle x1="-1.5875" y1="0.4875" x2="-0.9875" y2="0.6125" layer="51" rot="R90"/>
<rectangle x1="-1.95" y1="-0.15" x2="-0.25" y2="0.15" layer="51" rot="R90"/>
<rectangle x1="0.45" y1="-0.35" x2="2.15" y2="0.35" layer="51" rot="R90"/>
<rectangle x1="-0.725" y1="-0.9" x2="-0.4" y2="-0.475" layer="21" rot="R90"/>
<rectangle x1="-0.725" y1="0.475" x2="-0.4" y2="0.9" layer="21" rot="R90"/>
<rectangle x1="-0.35" y1="-0.175" x2="0" y2="0.175" layer="21" rot="R90"/>
<polygon width="0.2032" layer="21">
<vertex x="-3.302" y="-1.0414"/>
<vertex x="-3.302" y="1.0414"/>
<vertex x="-2.794" y="1.0414"/>
<vertex x="-2.794" y="-1.0414"/>
</polygon>
</package>
<package name="F-PTC-RX250">
<pad name="1" x="-5.25" y="0.45" drill="1" diameter="2.54" shape="octagon"/>
<pad name="2" x="5.25" y="-0.45" drill="1" diameter="2.54" shape="octagon"/>
<wire x1="-8.6" y1="2" x2="8.6" y2="2" width="0.254" layer="21"/>
<wire x1="-8.6" y1="-2" x2="8.6" y2="-2" width="0.254" layer="21"/>
<wire x1="-8.6" y1="-2" x2="-8.6" y2="2" width="0.254" layer="21" curve="-180"/>
<wire x1="8.6" y1="-2" x2="8.6" y2="2" width="0.254" layer="21" curve="180"/>
<text x="-8.89" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-8.89" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="F-PTC-1210">
<wire x1="-2.3" y1="1.625" x2="2.3" y2="1.625" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.625" x2="2.3" y2="-1.625" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.625" x2="-2.3" y2="-1.625" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.625" x2="-2.3" y2="1.625" width="0.2032" layer="21"/>
<smd name="2" x="1.5" y="0" dx="1.05" dy="2.8" layer="1" roundness="20"/>
<smd name="1" x="-1.5" y="0" dx="1.05" dy="2.8" layer="1" roundness="20"/>
<text x="-2.54" y="2.6035" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.8735" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2032" y1="-0.635" x2="0.2032" y2="0.635" layer="35"/>
<wire x1="-2.3" y1="1.6" x2="-2.3" y2="-1.6" width="0.127" layer="39"/>
<wire x1="-2.3" y1="-1.6" x2="2.3" y2="-1.6" width="0.127" layer="39"/>
<wire x1="2.3" y1="-1.6" x2="2.3" y2="1.6" width="0.127" layer="39"/>
<wire x1="2.3" y1="1.6" x2="-2.3" y2="1.6" width="0.127" layer="39"/>
</package>
<package name="F-PTC-RB250">
<pad name="1" x="-2.55" y="0.45" drill="0.8" diameter="1.9304" shape="octagon"/>
<pad name="2" x="2.55" y="-0.45" drill="0.8" diameter="1.9304" shape="octagon"/>
<wire x1="-4.2" y1="1.65" x2="4.2" y2="1.65" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-1.65" x2="4.2" y2="-1.65" width="0.254" layer="21"/>
<wire x1="-4.2" y1="-1.65" x2="-4.2" y2="1.65" width="0.254" layer="21" curve="-180"/>
<wire x1="4.2" y1="-1.65" x2="4.2" y2="1.65" width="0.254" layer="21" curve="180"/>
<text x="-5.08" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="CAPAECASEA">
<description>&lt;h3&gt;Capacitor Electrolytic Aluminum SMD CASE A&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Aluminum Electrolytic Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA CASE A
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPAE330X540N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="0.9" y1="-1.65" x2="-1.65" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1" y1="1" x2="1" y2="1" width="0.127" layer="21" curve="-104.993"/>
<wire x1="-1.65" y1="1" x2="-1.65" y2="1.65" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="1.65" x2="0.9" y2="1.65" width="0.2" layer="21"/>
<wire x1="0.9" y1="1.65" x2="1.65" y2="1" width="0.2032" layer="21"/>
<wire x1="1.65" y1="-1" x2="0.9" y2="-1.65" width="0.2032" layer="21"/>
<wire x1="-1.65" y1="-1.65" x2="-1.65" y2="-1" width="0.2032" layer="21"/>
<wire x1="1" y1="-1" x2="-1" y2="-1" width="0.127" layer="21" curve="-105"/>
<smd name="-" x="-1.4" y="0" dx="2.25" dy="1.5" layer="1"/>
<smd name="+" x="1.4" y="0" dx="2.25" dy="1.5" layer="1" roundness="20"/>
<text x="-2.54" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="-1" y="1"/>
<vertex x="-0.635" y="1"/>
<vertex x="-0.635" y="1.3"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-0.635" y="-1.3"/>
<vertex x="-0.635" y="-1"/>
<vertex x="-1" y="-1"/>
</polygon>
<rectangle x1="-0.1016" y1="-0.762" x2="0.1016" y2="0.762" layer="35"/>
<wire x1="-3.05" y1="2.25" x2="-3.05" y2="-2.25" width="0.127" layer="39"/>
<wire x1="-3.05" y1="-2.25" x2="3.05" y2="-2.25" width="0.127" layer="39"/>
<wire x1="3.05" y1="-2.25" x2="3.05" y2="2.25" width="0.127" layer="39"/>
<wire x1="3.05" y1="2.25" x2="-3.05" y2="2.25" width="0.127" layer="39"/>
</package>
<package name="CAPAECASEB">
<description>&lt;h3&gt;Capacitor Electrolytic Aluminum SMD CASE B&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Aluminum Electrolytic Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA CASE B
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPAE430X540N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-2.15" y1="2.15" x2="1.1" y2="2.15" width="0.2032" layer="21"/>
<wire x1="1.1" y1="2.15" x2="2.15" y2="1.1" width="0.2032" layer="21"/>
<wire x1="2.15" y1="-1.1" x2="1.1" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="1.1" y1="-2.15" x2="-2.15" y2="-2.15" width="0.2032" layer="21"/>
<wire x1="-1.5748" y1="1.1" x2="1.4986" y2="1.1" width="0.127" layer="21" curve="-128.042"/>
<wire x1="-1.5748" y1="-1.1" x2="1.4986" y2="-1.1" width="0.127" layer="21" curve="128.187"/>
<wire x1="-2.15" y1="1.1" x2="-2.15" y2="2.15" width="0.2032" layer="21"/>
<wire x1="-2.15" y1="-2.15" x2="-2.15" y2="-1.1" width="0.2032" layer="21"/>
<smd name="-" x="-1.75" y="0" dx="2.55" dy="1.6" layer="1"/>
<smd name="+" x="1.75" y="0" dx="2.55" dy="1.6" layer="1" roundness="20"/>
<polygon width="0.127" layer="21">
<vertex x="-1.5748" y="1.1"/>
<vertex x="-1" y="1.1"/>
<vertex x="-1" y="1.7"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-1.5748" y="-1.1"/>
<vertex x="-1" y="-1.1"/>
<vertex x="-1" y="-1.7"/>
</polygon>
<text x="-2.54" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.1016" y1="-0.762" x2="0.1016" y2="0.762" layer="35"/>
<wire x1="-3.55" y1="2.75" x2="-3.55" y2="-2.75" width="0.127" layer="39"/>
<wire x1="-3.55" y1="-2.75" x2="3.55" y2="-2.75" width="0.127" layer="39"/>
<wire x1="3.55" y1="-2.75" x2="3.55" y2="2.75" width="0.127" layer="39"/>
<wire x1="3.55" y1="2.75" x2="-3.55" y2="2.75" width="0.127" layer="39"/>
</package>
<package name="CAPAECASEC">
<description>&lt;h3&gt;Capacitor Electrolytic Aluminum SMD CASE C&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Aluminum Electrolytic Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA CASE C
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPAE530X540N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-2.65" y1="2.65" x2="1.4" y2="2.65" width="0.2032" layer="21"/>
<wire x1="1.4" y1="2.65" x2="2.65" y2="1.4" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.4" x2="1.4" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="1.4" y1="-2.65" x2="-2.65" y2="-2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="1.2" x2="-2.65" y2="2.65" width="0.2032" layer="21"/>
<wire x1="-2.65" y1="-2.65" x2="-2.65" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2" y1="1.2" x2="2" y2="1.2" width="0.127" layer="21" curve="-140"/>
<wire x1="-2" y1="-1.2" x2="2" y2="-1.2" width="0.127" layer="21" curve="140"/>
<smd name="-" x="-2.25" y="0" dx="3" dy="1.6" layer="1"/>
<smd name="+" x="2.25" y="0" dx="3" dy="1.6" layer="1" roundness="20"/>
<polygon width="0.127" layer="21">
<vertex x="-1.9812" y="1.2"/>
<vertex x="-1.3462" y="1.2"/>
<vertex x="-1.3462" y="2.1"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-1.3462" y="-2.1"/>
<vertex x="-1.9812" y="-1.2"/>
<vertex x="-1.3462" y="-1.2"/>
</polygon>
<wire x1="2.65" y1="1.4" x2="2.65" y2="1.2" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.2" x2="2.65" y2="-1.4" width="0.2032" layer="21"/>
<wire x1="-4.25" y1="3.25" x2="-4.25" y2="-3.25" width="0.127" layer="39"/>
<wire x1="-4.25" y1="-3.25" x2="4.25" y2="-3.25" width="0.127" layer="39"/>
<wire x1="4.25" y1="-3.25" x2="4.25" y2="3.25" width="0.127" layer="39"/>
<wire x1="4.25" y1="3.25" x2="-4.25" y2="3.25" width="0.127" layer="39"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-1.016" x2="0.254" y2="1.016" layer="35"/>
</package>
<package name="CAPAECASED">
<description>&lt;h3&gt;Capacitor Electrolytic Aluminum SMD CASE D&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Aluminum Electrolytic Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA CASE D
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPAE660X540N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-3.3" y1="3.3" x2="1.5494" y2="3.3" width="0.2032" layer="21"/>
<wire x1="1.5494" y1="3.3" x2="3.3" y2="1.5494" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.5494" x2="1.5494" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="1.5494" y1="-3.3" x2="-3.3" y2="-3.3" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="1.2" x2="-3.3" y2="3.3" width="0.2032" layer="21"/>
<wire x1="3.3" y1="1.5494" x2="3.3" y2="1.2" width="0.2032" layer="21"/>
<wire x1="3.3" y1="-1.2" x2="3.3" y2="-1.5494" width="0.2032" layer="21"/>
<wire x1="-3.3" y1="-3.3" x2="-3.3" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-2.667" y1="1.2" x2="2.667" y2="1.2" width="0.127" layer="21" curve="-140"/>
<wire x1="-2.667" y1="-1.2" x2="2.667" y2="-1.2" width="0.127" layer="21" curve="140"/>
<smd name="-" x="-2.45" y="0" dx="3.15" dy="1.6" layer="1"/>
<smd name="+" x="2.45" y="0" dx="3.15" dy="1.6" layer="1" roundness="20"/>
<polygon width="0.127" layer="21">
<vertex x="-2.667" y="1.2"/>
<vertex x="-2.032" y="1.2"/>
<vertex x="-2.032" y="2.2"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-2.667" y="-1.2"/>
<vertex x="-2.032" y="-1.2"/>
<vertex x="-2.032" y="-2.2"/>
</polygon>
<rectangle x1="-0.508" y1="-1.27" x2="0.508" y2="1.27" layer="35"/>
<wire x1="-4.55" y1="3.9" x2="-4.55" y2="-3.9" width="0.127" layer="39"/>
<wire x1="-4.55" y1="-3.9" x2="4.55" y2="-3.9" width="0.127" layer="39"/>
<wire x1="4.55" y1="-3.9" x2="4.55" y2="3.9" width="0.127" layer="39"/>
<wire x1="4.55" y1="3.9" x2="-4.55" y2="3.9" width="0.127" layer="39"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="CAPAECASEE">
<description>&lt;h3&gt;Capacitor Electrolytic Aluminum SMD CASE E&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Aluminum Electrolytic Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA CASE E
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPAE830X620N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-4.2" y1="4.2" x2="1.8034" y2="4.2" width="0.2032" layer="21"/>
<wire x1="1.8034" y1="4.2" x2="4.2" y2="1.8034" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-1.8034" x2="1.8034" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="1.8034" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.2" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.8034" x2="4.2" y2="1.2" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-1.2" x2="4.2" y2="-1.8034" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.2" width="0.2032" layer="21"/>
<wire x1="-3.556" y1="1.2" x2="3.556" y2="1.2" width="0.127" layer="21" curve="-140"/>
<wire x1="-3.556" y1="-1.2" x2="3.556" y2="-1.2" width="0.127" layer="21" curve="140"/>
<smd name="-" x="-3.15" y="0" dx="4.15" dy="1.6" layer="1"/>
<smd name="+" x="3.15" y="0" dx="4.15" dy="1.6" layer="1" roundness="20"/>
<polygon width="0.127" layer="21">
<vertex x="-3.556" y="1.2"/>
<vertex x="-2.667" y="1.2"/>
<vertex x="-2.667" y="2.55"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-2.667" y="-1.2"/>
<vertex x="-3.556" y="-1.2"/>
<vertex x="-2.667" y="-2.55"/>
</polygon>
<rectangle x1="-0.508" y1="-1.524" x2="0.508" y2="1.524" layer="35"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-6.35" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-5.75" y1="4.75" x2="-5.75" y2="-4.75" width="0.127" layer="39"/>
<wire x1="-5.75" y1="-4.75" x2="5.75" y2="-4.75" width="0.127" layer="39"/>
<wire x1="5.75" y1="-4.75" x2="5.75" y2="4.75" width="0.127" layer="39"/>
<wire x1="5.75" y1="4.75" x2="-5.75" y2="4.75" width="0.127" layer="39"/>
</package>
<package name="CAPAECASEF">
<description>&lt;h3&gt;Capacitor Electrolytic Aluminum SMD CASE F&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Aluminum Electrolytic Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA CASE F
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPAE830X1020N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="4.2" y1="-1.8034" x2="1.8034" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="1.3" x2="-4.2" y2="4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="4.2" x2="1.8034" y2="4.2" width="0.2032" layer="21"/>
<wire x1="1.8034" y1="4.2" x2="4.2" y2="1.8034" width="0.2032" layer="21"/>
<wire x1="4.2" y1="1.8034" x2="4.2" y2="1.3" width="0.2032" layer="21"/>
<wire x1="4.2" y1="-1.3" x2="4.2" y2="-1.8034" width="0.2032" layer="21"/>
<wire x1="1.8034" y1="-4.2" x2="-4.2" y2="-4.2" width="0.2032" layer="21"/>
<wire x1="-4.2" y1="-4.2" x2="-4.2" y2="-1.3" width="0.2032" layer="21"/>
<wire x1="-3.556" y1="1.3" x2="3.556" y2="1.3" width="0.127" layer="21" curve="-140"/>
<wire x1="-3.556" y1="-1.3" x2="3.556" y2="-1.3" width="0.127" layer="21" curve="140"/>
<smd name="-" x="-3.7" y="0" dx="4.4" dy="1.8" layer="1"/>
<smd name="+" x="3.7" y="0" dx="4.4" dy="1.8" layer="1" roundness="20"/>
<polygon width="0.127" layer="21">
<vertex x="-3.556" y="1.3"/>
<vertex x="-2.667" y="1.3"/>
<vertex x="-2.667" y="2.6416"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-2.667" y="-1.3"/>
<vertex x="-3.556" y="-1.3"/>
<vertex x="-2.667" y="-2.6416"/>
</polygon>
<text x="-3.81" y="5.08" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-6.35" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-6.4" y1="4.75" x2="-6.4" y2="-4.75" width="0.127" layer="39"/>
<wire x1="-6.4" y1="-4.75" x2="6.4" y2="-4.75" width="0.127" layer="39"/>
<wire x1="6.4" y1="-4.75" x2="6.4" y2="4.75" width="0.127" layer="39"/>
<wire x1="6.4" y1="4.75" x2="-6.4" y2="4.75" width="0.127" layer="39"/>
<rectangle x1="-0.762" y1="-2.032" x2="0.762" y2="2.032" layer="35"/>
</package>
<package name="CAPAECASEG">
<description>&lt;h3&gt;Capacitor Electrolytic Aluminum SMD CASE G&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Aluminum Electrolytic Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA CASE G
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPAE1030X1020N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-5.15" y1="5.15" x2="2.8194" y2="5.15" width="0.2032" layer="21"/>
<wire x1="2.8194" y1="5.15" x2="5.15" y2="2.8194" width="0.2032" layer="21"/>
<wire x1="5.15" y1="-2.8194" x2="2.8194" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="2.8194" y1="-5.15" x2="-5.15" y2="-5.15" width="0.2032" layer="21"/>
<wire x1="-5.15" y1="1.5" x2="-5.15" y2="5.15" width="0.2032" layer="21"/>
<wire x1="5.15" y1="2.8194" x2="5.15" y2="1.5" width="0.2032" layer="21"/>
<wire x1="5.15" y1="-1.5" x2="5.15" y2="-2.8194" width="0.2032" layer="21"/>
<wire x1="-5.15" y1="-5.15" x2="-5.15" y2="-1.5" width="0.2032" layer="21"/>
<wire x1="-4.572" y1="-1.5" x2="4.572" y2="-1.5" width="0.127" layer="21" curve="140"/>
<wire x1="-4.572" y1="1.5" x2="4.572" y2="1.5" width="0.127" layer="21" curve="-140"/>
<smd name="-" x="-4.5" y="0" dx="4.5" dy="2.1" layer="1"/>
<smd name="+" x="4.5" y="0" dx="4.5" dy="2.1" layer="1" roundness="20"/>
<polygon width="0.127" layer="21">
<vertex x="-4.572" y="1.5"/>
<vertex x="-3.683" y="1.5"/>
<vertex x="-3.683" y="3"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-3.683" y="-1.5"/>
<vertex x="-4.572" y="-1.5"/>
<vertex x="-3.683" y="-3"/>
</polygon>
<text x="-5.08" y="6.35" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="1.27" y="6.35" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-7.25" y1="5.75" x2="-7.25" y2="-5.75" width="0.127" layer="39"/>
<wire x1="-7.25" y1="-5.75" x2="7.25" y2="-5.75" width="0.127" layer="39"/>
<wire x1="7.25" y1="-5.75" x2="7.25" y2="5.75" width="0.127" layer="39"/>
<wire x1="7.25" y1="5.75" x2="-7.25" y2="5.75" width="0.127" layer="39"/>
<rectangle x1="-1.27" y1="-2.54" x2="1.27" y2="2.54" layer="35"/>
</package>
<package name="CAPMPCASEA">
<description>&lt;h3&gt;Capacitor Electrolytic Tantalum SMD CASE A&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Molded body Polarized Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 535BAAC CASE A
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPMP3216X180N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-2.45" y1="1.15" x2="2.45" y2="1.15" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.15" x2="2.45" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.15" x2="2.45" y2="-1.15" width="0.2032" layer="21"/>
<smd name="+" x="-1.3" y="0" dx="1.8" dy="1.25" layer="1"/>
<smd name="-" x="1.3" y="0" dx="1.8" dy="1.25" layer="1" roundness="20" rot="R180"/>
<rectangle x1="-0.127" y1="-0.625" x2="0.127" y2="0.625" layer="35"/>
<polygon width="0.2032" layer="21">
<vertex x="-2.95" y="-1.15"/>
<vertex x="-2.95" y="1.15"/>
<vertex x="-2.45" y="1.15"/>
<vertex x="-2.45" y="-1.15"/>
</polygon>
<text x="-2.54" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.45" y1="1.15" x2="-2.45" y2="-1.15" width="0.127" layer="39"/>
<wire x1="-2.45" y1="-1.15" x2="2.45" y2="-1.15" width="0.127" layer="39"/>
<wire x1="2.45" y1="-1.15" x2="2.45" y2="1.15" width="0.127" layer="39"/>
<wire x1="2.45" y1="1.15" x2="-2.45" y2="1.15" width="0.127" layer="39"/>
</package>
<package name="CAPMPCASEB">
<description>&lt;h3&gt;Capacitor Electrolytic Tantalum SMD CASE B&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Molded body Polarized Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 535BAAC CASE B
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPMP3528X210N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-2.6" y1="1.75" x2="2.6" y2="1.75" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.75" x2="-2.6" y2="-1.75" width="0.2032" layer="21"/>
<wire x1="2.6" y1="-1.75" x2="2.6" y2="1.75" width="0.2032" layer="21"/>
<smd name="+" x="-1.45" y="0" dx="1.8" dy="2.25" layer="1"/>
<smd name="-" x="1.45" y="0" dx="1.8" dy="2.25" layer="1" roundness="20" rot="R180"/>
<rectangle x1="-0.254" y1="-1.125" x2="0.254" y2="1.125" layer="35"/>
<polygon width="0.2032" layer="21">
<vertex x="-2.6" y="-1.75"/>
<vertex x="-2.6" y="1.75"/>
<vertex x="-3.1" y="1.75"/>
<vertex x="-3.1" y="-1.75"/>
</polygon>
<text x="-2.54" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-2.6" y1="1.75" x2="-2.6" y2="-1.75" width="0.127" layer="39"/>
<wire x1="-2.6" y1="-1.75" x2="2.6" y2="-1.75" width="0.127" layer="39"/>
<wire x1="2.6" y1="-1.75" x2="2.6" y2="1.75" width="0.127" layer="39"/>
<wire x1="2.6" y1="1.75" x2="-2.6" y2="1.75" width="0.127" layer="39"/>
</package>
<package name="CAPMPCASEC">
<description>&lt;h3&gt;Capacitor Electrolytic Tantalum SMD CASE C&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Molded body Polarized Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 535BAAC CASE C
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPMP6032X280N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-4.0132" y1="2" x2="3.9" y2="2" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2" x2="-3.9878" y2="-2" width="0.2032" layer="21"/>
<wire x1="3.9" y1="-2" x2="3.9" y2="2" width="0.2032" layer="21"/>
<smd name="+" x="-2.45" y="0" dx="2.35" dy="2.25" layer="1"/>
<smd name="-" x="2.45" y="0" dx="2.35" dy="2.25" layer="1" roundness="20" rot="R180"/>
<rectangle x1="-0.508" y1="-1.125" x2="0.508" y2="1.125" layer="35"/>
<polygon width="0.2032" layer="21">
<vertex x="-3.9" y="-2"/>
<vertex x="-3.9" y="2"/>
<vertex x="-4.4" y="2"/>
<vertex x="-4.4" y="-2"/>
</polygon>
<wire x1="-3.9" y1="2" x2="-3.9" y2="-2" width="0.127" layer="39"/>
<wire x1="-3.9" y1="-2" x2="3.9" y2="-2" width="0.127" layer="39"/>
<wire x1="3.9" y1="-2" x2="3.9" y2="2" width="0.127" layer="39"/>
<wire x1="3.9" y1="2" x2="-3.9" y2="2" width="0.127" layer="39"/>
<text x="-3.81" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
</package>
<package name="CAPMPCASED">
<description>&lt;h3&gt;Capacitor Electrolytic Tantalum SMD CASE D&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Molded body Polarized Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 535BAAC CASE D
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPMP7343X310N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-4.699" y1="2.55" x2="4.55" y2="2.55" width="0.2032" layer="21"/>
<wire x1="4.55" y1="-2.55" x2="-4.699" y2="-2.55" width="0.2032" layer="21"/>
<wire x1="4.55" y1="2.55" x2="4.55" y2="-2.55" width="0.2032" layer="21"/>
<smd name="+" x="-3.1" y="0" dx="2.45" dy="2.35" layer="1"/>
<smd name="-" x="3.1" y="0" dx="2.45" dy="2.35" layer="1" roundness="20" rot="R180"/>
<rectangle x1="-0.635" y1="-1.225" x2="0.635" y2="1.225" layer="35"/>
<polygon width="0.2032" layer="21">
<vertex x="-5.05" y="2.55"/>
<vertex x="-5.05" y="-2.55"/>
<vertex x="-4.55" y="-2.55"/>
<vertex x="-4.55" y="2.55"/>
</polygon>
<text x="-5.08" y="3.81" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-4.55" y1="2.55" x2="-4.55" y2="-2.55" width="0.127" layer="39"/>
<wire x1="-4.55" y1="-2.55" x2="4.55" y2="-2.55" width="0.127" layer="39"/>
<wire x1="4.55" y1="-2.55" x2="4.55" y2="2.55" width="0.127" layer="39"/>
<wire x1="4.55" y1="2.55" x2="-4.55" y2="2.55" width="0.127" layer="39"/>
</package>
<package name="CAPMPCASEE">
<description>&lt;h3&gt;Capacitor Electrolytic Tantalum SMD CASE E&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Molded body Polarized Capacitor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 535BAAC CASE E
&lt;b&gt;IPC Name:&lt;/b&gt;	CAPMP7360X380N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-4.699" y1="3.4" x2="4.55" y2="3.4" width="0.2032" layer="21"/>
<wire x1="4.55" y1="-3.4" x2="-4.699" y2="-3.4" width="0.2032" layer="21"/>
<wire x1="4.55" y1="-3.4" x2="4.55" y2="3.4" width="0.2032" layer="21"/>
<smd name="+" x="-3.1" y="0" dx="2.35" dy="4.15" layer="1"/>
<smd name="-" x="3.1" y="0" dx="2.35" dy="4.15" layer="1" roundness="20" rot="R180"/>
<rectangle x1="-0.508" y1="-2.075" x2="0.508" y2="2.075" layer="35"/>
<polygon width="0.2032" layer="21">
<vertex x="-5.05" y="-3.4"/>
<vertex x="-5.05" y="3.4"/>
<vertex x="-4.55" y="3.4"/>
<vertex x="-4.55" y="-3.4"/>
</polygon>
<text x="-5.08" y="3.81" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-5.08" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-4.55" y1="3.4" x2="-4.55" y2="-3.4" width="0.127" layer="39"/>
<wire x1="-4.55" y1="-3.4" x2="4.55" y2="-3.4" width="0.127" layer="39"/>
<wire x1="4.55" y1="-3.4" x2="4.55" y2="3.4" width="0.127" layer="39"/>
<wire x1="4.55" y1="3.4" x2="-4.55" y2="3.4" width="0.127" layer="39"/>
</package>
<package name="CAPE10X5">
<text x="-3.81" y="5.08" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-6.35" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-5.43046875" y1="5.3890625" x2="-5.43046875" y2="-5.3890625" width="0.127" layer="39"/>
<wire x1="-5.43046875" y1="-5.3890625" x2="5.43046875" y2="-5.3890625" width="0.127" layer="39"/>
<wire x1="5.43046875" y1="-5.3890625" x2="5.43046875" y2="5.3890625" width="0.127" layer="39"/>
<wire x1="5.43046875" y1="5.3890625" x2="-5.43046875" y2="5.3890625" width="0.127" layer="39"/>
<pad name="-" x="-2.5" y="0" drill="0.8" shape="octagon"/>
<pad name="+" x="2.5" y="0" drill="0.8" shape="octagon"/>
<circle x="0" y="0" radius="5" width="0.3048" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-1.27" y="4.76046875"/>
<vertex x="-1.27" y="1.27"/>
<vertex x="-3.175" y="1.27"/>
<vertex x="-3.81" y="0.635"/>
<vertex x="-3.81" y="-0.635"/>
<vertex x="-3.175" y="-1.27"/>
<vertex x="-1.27" y="-1.27"/>
<vertex x="-1.27" y="-4.76046875"/>
<vertex x="-3.1790625" y="-3.801875"/>
<vertex x="-4.129528125" y="-2.85140625"/>
<vertex x="-5.088128125" y="-0.303275"/>
<vertex x="-4.76046875" y="1.6057875"/>
<vertex x="-3.16280625" y="3.82625625"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="C">
<wire x1="0" y1="2.54" x2="0" y2="2.032" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="0.508" width="0.1524" layer="94"/>
<text x="1.524" y="2.921" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-2.159" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="0.508" x2="2.032" y2="1.016" layer="94"/>
<rectangle x1="-2.032" y1="1.524" x2="2.032" y2="2.032" layer="94"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
<symbol name="L">
<wire x1="-5.08" y1="0" x2="-2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="-2.54" y1="0" x2="0" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.254" layer="94" curve="-180"/>
<wire x1="2.54" y1="0" x2="5.08" y2="0" width="0.254" layer="94" curve="-180"/>
<text x="-7.62" y="2.54" size="1.778" layer="95">&gt;NAME</text>
<text x="-7.62" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P$1" x="-7.62" y="0" visible="off" length="short" direction="pas"/>
<pin name="P$2" x="7.62" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="R-TRIM">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="4.0386" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="3" x="-5.08" y="2.54" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0.9525" width="0.1524" layer="94"/>
<wire x1="0" y1="0.9525" x2="-0.3175" y2="1.5875" width="0.1524" layer="94"/>
<wire x1="0" y1="0.9525" x2="0.3175" y2="1.5875" width="0.1524" layer="94"/>
</symbol>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
<symbol name="FUSE">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="CPOL">
<wire x1="-1.524" y1="1.651" x2="1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="1.524" y1="1.651" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="-1.524" y2="1.651" width="0.254" layer="94"/>
<wire x1="-1.524" y1="2.54" x2="1.524" y2="2.54" width="0.254" layer="94"/>
<text x="1.143" y="3.0226" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.5842" y="2.9464" size="1.27" layer="94" rot="R90">+</text>
<text x="1.143" y="-2.0574" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-1.651" y1="0" x2="1.651" y2="0.889" layer="94"/>
<pin name="-" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="+" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CAPN" prefix="C" uservalue="yes">
<description>&lt;h3&gt;Unpolarized Capacitor&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Name:&lt;/b&gt;	Capacitor
&lt;b&gt;Type:&lt;/b&gt;	Capacitor
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<gates>
<gate name="G$1" symbol="C" x="0" y="0"/>
</gates>
<devices>
<device name="C2013" package="CAPC0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="CAPC1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="CAPC0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RES" prefix="R" uservalue="yes">
<description>&lt;h3&gt;Resistor&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Name:&lt;/b&gt;	Resistor
&lt;b&gt;Type:&lt;/b&gt;	Resistor
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<gates>
<gate name="&gt;NAME" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="C2013" package="RESC0805">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="RESC1206">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="RESC0603">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-PWR-TO221" package="R-TO-221">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="L" prefix="L" uservalue="yes">
<gates>
<gate name="G$1" symbol="L" x="0" y="0"/>
</gates>
<devices>
<device name="'NECO'" package="L-1A">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-1210'" package="INDC1210">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-DE0704'" package="INDCDE0704">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-0805'" package="INDC0805">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-DC0604'" package="INDCDC0604">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-WE-PD'" package="INDC-WE-PD-SMD">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-DE0703'" package="INDCDE0703">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="'-1206'" package="INDC1206">
<connects>
<connect gate="G$1" pin="P$1" pad="1"/>
<connect gate="G$1" pin="P$2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="R-TRIM" prefix="TR">
<gates>
<gate name="G$1" symbol="R-TRIM" x="0" y="0"/>
</gates>
<devices>
<device name="TC33X" package="R-VAR-TC33X">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="CA14V" package="R-VAR-CA14V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;h3&gt;Light Emitting Diode&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	LED
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="LED-0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FUSE" prefix="F">
<gates>
<gate name="G$1" symbol="FUSE" x="0" y="0"/>
</gates>
<devices>
<device name="-0805" package="RESC0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RX250" package="F-PTC-RX250">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-1210" package="F-PTC-1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="-RB250" package="F-PTC-RB250">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPP" prefix="C" uservalue="yes">
<description>&lt;h3&gt;Polarized Capacitor&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Name:&lt;/b&gt;	Capacitor
&lt;b&gt;Type:&lt;/b&gt;	Polarized
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<gates>
<gate name="G$1" symbol="CPOL" x="0" y="0"/>
</gates>
<devices>
<device name="AE330X540N" package="CAPAECASEA">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AE430X540N" package="CAPAECASEB">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AE530X540N" package="CAPAECASEC">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AE660X540N" package="CAPAECASED">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AE830X620N" package="CAPAECASEE">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AE830X1020N" package="CAPAECASEF">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="AE1030X1020N" package="CAPAECASEG">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MP3216X180N" package="CAPMPCASEA">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MP3528X210N" package="CAPMPCASEB">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MP6032X280N" package="CAPMPCASEC">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MP7343X310N" package="CAPMPCASED">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MP7360X380N" package="CAPMPCASEE">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="10X5" package="CAPE10X5">
<connects>
<connect gate="G$1" pin="+" pad="+"/>
<connect gate="G$1" pin="-" pad="-"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ph-ic">
<description>&lt;h2&gt;PetrH's Integrated Circuits Library&lt;/h2&gt;
&lt;h4&gt;© 2012 Petr Havlíček&lt;/h4&gt;
&lt;p&gt;
High quality hand made libraries by Petr Havlíček. Land patterns and silkscreens are based on IPC-7351 through Mentor Graphics IPC-7351 LP Viewer.&lt;/p&gt;</description>
<packages>
<package name="SOP65P780X200-16N">
<description>&lt;h3&gt;SMD SSOP-16 IC&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Name:&lt;/b&gt;	Shrink Small Outline Integrated Circuit
&lt;b&gt;IPC Name:&lt;/b&gt;	SOP65P780X200-16N
&lt;b&gt;Standart:&lt;/b&gt;	JEDEC MO-150AC
&lt;b&gt;Type:&lt;/b&gt;	Integrate Circuit
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Pins:&lt;/b&gt;	16</description>
<smd name="1" x="-3.5" y="2.275" dx="1.85" dy="0.45" layer="1"/>
<smd name="2" x="-3.5" y="1.625" dx="1.85" dy="0.45" layer="1"/>
<smd name="3" x="-3.5" y="0.975" dx="1.85" dy="0.45" layer="1"/>
<smd name="4" x="-3.5" y="0.325" dx="1.85" dy="0.45" layer="1"/>
<smd name="5" x="-3.5" y="-0.325" dx="1.85" dy="0.45" layer="1"/>
<smd name="6" x="-3.5" y="-0.975" dx="1.85" dy="0.45" layer="1"/>
<smd name="7" x="-3.5" y="-1.625" dx="1.85" dy="0.45" layer="1"/>
<smd name="8" x="-3.5" y="-2.275" dx="1.85" dy="0.45" layer="1"/>
<smd name="9" x="3.5" y="-2.275" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<smd name="10" x="3.5" y="-1.625" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<smd name="11" x="3.5" y="-0.975" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<smd name="12" x="3.5" y="-0.325" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<smd name="13" x="3.5" y="0.325" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<smd name="14" x="3.5" y="0.975" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<smd name="15" x="3.5" y="1.625" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<smd name="16" x="3.5" y="2.275" dx="1.85" dy="0.45" layer="1" rot="R180"/>
<wire x1="-2.25" y1="3.1" x2="-2.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="-2.25" y1="-3.1" x2="2.25" y2="-3.1" width="0.2032" layer="21"/>
<wire x1="2.25" y1="-3.1" x2="2.25" y2="3.1" width="0.2032" layer="21"/>
<wire x1="2.25" y1="3.1" x2="-2.25" y2="3.1" width="0.2032" layer="21"/>
<rectangle x1="-0.65" y1="-0.65" x2="0.65" y2="0.65" layer="35"/>
<text x="-3.2662" y="3.851" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-3.2799" y="-5.121" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-4.7" y1="3.5" x2="-4.7" y2="-3.5" width="0.2032" layer="39"/>
<wire x1="-4.7" y1="-3.5" x2="4.7" y2="-3.5" width="0.2032" layer="39"/>
<wire x1="4.7" y1="-3.5" x2="4.7" y2="3.5" width="0.2032" layer="39"/>
<wire x1="4.7" y1="3.5" x2="-4.7" y2="3.5" width="0.2032" layer="39"/>
<wire x1="-2.25" y1="3.1" x2="-4.2" y2="3.1" width="0.2032" layer="21"/>
</package>
<package name="TO-220">
<text x="-5.08" y="2.855" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-4.125" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<pad name="1" x="-2.54" y="0" drill="1.4" shape="long" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.4" shape="long" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.4" shape="long" rot="R90"/>
<wire x1="-5.25" y1="2.35" x2="-5.25" y2="-2.35" width="0.127" layer="21"/>
<wire x1="-5.25" y1="-2.35" x2="5.25" y2="-2.35" width="0.127" layer="21"/>
<wire x1="5.25" y1="-2.35" x2="5.25" y2="2.35" width="0.127" layer="21"/>
<wire x1="5.25" y1="2.35" x2="-5.25" y2="2.35" width="0.127" layer="21"/>
<polygon width="0.127" layer="21">
<vertex x="-5.25" y="2.34"/>
<vertex x="-5.25" y="1.27"/>
<vertex x="-4.1" y="1.27"/>
<vertex x="-3.1" y="2.34"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="5.25" y="2.34"/>
<vertex x="5.25" y="1.27"/>
<vertex x="4.1" y="1.27"/>
<vertex x="3.1" y="2.34"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="-1.905" y="2.35"/>
<vertex x="-1.27" y="1.715"/>
<vertex x="-0.635" y="2.35"/>
</polygon>
<polygon width="0.127" layer="21">
<vertex x="0.635" y="2.35"/>
<vertex x="1.27" y="1.715"/>
<vertex x="1.905" y="2.35"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="FT230X">
<wire x1="-15.24" y1="12.7" x2="-15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="-15.24" y1="-12.7" x2="15.24" y2="-12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="-12.7" x2="15.24" y2="12.7" width="0.254" layer="94"/>
<wire x1="15.24" y1="12.7" x2="-15.24" y2="12.7" width="0.254" layer="94"/>
<pin name="VCC" x="0" y="17.78" length="middle" rot="R270"/>
<pin name="!CTS!" x="20.32" y="2.54" length="middle" rot="R180"/>
<pin name="CBUS0" x="20.32" y="-2.54" length="middle" rot="R180"/>
<pin name="3V3OUT" x="-20.32" y="-10.16" length="middle"/>
<pin name="D-" x="-20.32" y="7.62" length="middle"/>
<pin name="D+" x="-20.32" y="5.08" length="middle"/>
<pin name="!RESET!" x="-20.32" y="-7.62" length="middle"/>
<pin name="!RTS!" x="20.32" y="5.08" length="middle" rot="R180"/>
<pin name="CBUS1" x="20.32" y="-5.08" length="middle" rot="R180"/>
<pin name="CBUS2" x="20.32" y="-7.62" length="middle" rot="R180"/>
<pin name="CBUS3" x="20.32" y="-10.16" length="middle" rot="R180"/>
<pin name="VCCIO" x="-20.32" y="-5.08" length="middle"/>
<pin name="GND" x="0" y="-17.78" length="middle" rot="R90"/>
<text x="-15.24" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="15.24" y="-13.335" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<pin name="TXD" x="20.32" y="10.16" length="middle" rot="R180"/>
<pin name="RXD" x="20.32" y="7.62" length="middle" rot="R180"/>
</symbol>
<symbol name="78XX">
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="VIN" x="-15.24" y="2.54" length="middle"/>
<pin name="VOUT" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="GND" x="0" y="-12.7" length="middle" rot="R90"/>
<text x="-10.16" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="79XX">
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="VIN" x="-15.24" y="-5.08" length="middle"/>
<pin name="VOUT" x="15.24" y="-5.08" length="middle" rot="R180"/>
<pin name="GND" x="0" y="10.16" length="middle" rot="R270"/>
<text x="-10.16" y="5.715" size="1.778" layer="95">&gt;NAME</text>
<text x="2.54" y="-10.16" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="LDO">
<text x="-10.287" y="5.334" size="1.778" layer="95">&gt;NAME</text>
<text x="2.413" y="-9.8425" size="1.778" layer="96">&gt;VALUE</text>
<pin name="VIN" x="-15.24" y="2.54" length="middle" direction="sup"/>
<pin name="VOUT" x="15.24" y="2.54" length="middle" direction="sup" rot="R180"/>
<pin name="GND" x="0" y="-12.7" length="middle" rot="R90"/>
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="10.16" y2="-7.62" width="0.254" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="FT230X" prefix="IC">
<gates>
<gate name="G$1" symbol="FT230X" x="0" y="0"/>
</gates>
<devices>
<device name="S" package="SOP65P780X200-16N">
<connects>
<connect gate="G$1" pin="!CTS!" pad="6"/>
<connect gate="G$1" pin="!RESET!" pad="11"/>
<connect gate="G$1" pin="!RTS!" pad="2"/>
<connect gate="G$1" pin="3V3OUT" pad="10"/>
<connect gate="G$1" pin="CBUS0" pad="15"/>
<connect gate="G$1" pin="CBUS1" pad="14"/>
<connect gate="G$1" pin="CBUS2" pad="7"/>
<connect gate="G$1" pin="CBUS3" pad="16"/>
<connect gate="G$1" pin="D+" pad="8"/>
<connect gate="G$1" pin="D-" pad="9"/>
<connect gate="G$1" pin="GND" pad="5 13"/>
<connect gate="G$1" pin="RXD" pad="4"/>
<connect gate="G$1" pin="TXD" pad="1"/>
<connect gate="G$1" pin="VCC" pad="12"/>
<connect gate="G$1" pin="VCCIO" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="78XX" prefix="IC">
<gates>
<gate name="G$1" symbol="78XX" x="0" y="0"/>
</gates>
<devices>
<device name="TO220" package="TO-220">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="VIN" pad="1"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="79XX" prefix="IC">
<gates>
<gate name="G$1" symbol="79XX" x="0" y="2.54"/>
</gates>
<devices>
<device name="TO220" package="TO-220">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="2"/>
<connect gate="G$1" pin="VOUT" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="LD1117" prefix="IC">
<gates>
<gate name="G$1" symbol="LDO" x="0" y="0"/>
</gates>
<devices>
<device name="TO220" package="TO-220">
<connects>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VIN" pad="3"/>
<connect gate="G$1" pin="VOUT" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="dis">
<description>&lt;h2&gt;PetrH's Dicretes Library&lt;/h2&gt;
&lt;h4&gt;© 2012 Petr Havlíček&lt;/h4&gt;
&lt;p&gt;
High quality hand made libraries by Petr Havlíček. Land patterns and silkscreens are based on IPC-7351 through Mentor Graphics IPC-7351 LP Viewer.&lt;/p&gt;</description>
<packages>
<package name="LED-0805">
<description>&lt;h3&gt;SMD 0805 LED&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	LED
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Size:&lt;/b&gt;	0805
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<wire x1="-1.9304" y1="-0.8636" x2="-1.9304" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="-1.9304" y1="0.8636" x2="1.9304" y2="0.8636" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="0.8636" x2="1.9304" y2="-0.8636" width="0.2032" layer="21"/>
<wire x1="1.9304" y1="-0.8636" x2="-1.9304" y2="-0.8636" width="0.2032" layer="21"/>
<wire x1="-0.925" y1="-0.35" x2="-0.925" y2="0.35" width="0.1016" layer="51" curve="162.394521"/>
<wire x1="0.925" y1="-0.35" x2="0.925" y2="0.35" width="0.1016" layer="51" curve="-162.394521"/>
<wire x1="-0.525" y1="0.575" x2="0.525" y2="0.575" width="0.1016" layer="51"/>
<wire x1="0.5" y1="-0.575" x2="-0.925" y2="-0.575" width="0.1016" layer="51"/>
<circle x="-0.85" y="-0.45" radius="0.0889" width="0.1016" layer="51"/>
<smd name="C" x="-1.0668" y="0" dx="1.2192" dy="1.2192" layer="1" rot="R90"/>
<smd name="A" x="1.0668" y="0" dx="1.2192" dy="1.2192" layer="1" roundness="20" rot="R90"/>
<text x="-2.54" y="1.27" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-0.9125" y1="0.2125" x2="-0.5875" y2="0.7125" layer="51" rot="R90"/>
<rectangle x1="-0.6855" y1="-0.376" x2="-0.5511" y2="-0.1394" layer="51" rot="R90"/>
<rectangle x1="-0.6855" y1="0.1394" x2="-0.5511" y2="0.376" layer="51" rot="R90"/>
<rectangle x1="-0.7875" y1="-0.0875" x2="-0.3875" y2="0.0875" layer="51" rot="R90"/>
<rectangle x1="0.5875" y1="0.2125" x2="0.9125" y2="0.7125" layer="51" rot="R90"/>
<rectangle x1="0.5875" y1="-0.7125" x2="0.9125" y2="-0.2125" layer="51" rot="R90"/>
<rectangle x1="0.5511" y1="0.1394" x2="0.6855" y2="0.376" layer="51" rot="R90"/>
<rectangle x1="0.5511" y1="-0.376" x2="0.6855" y2="-0.1394" layer="51" rot="R90"/>
<rectangle x1="0.3875" y1="-0.0875" x2="0.7875" y2="0.0875" layer="51" rot="R90"/>
<rectangle x1="-0.2" y1="-0.1" x2="0" y2="0.1" layer="21" rot="R90"/>
<rectangle x1="-0.8" y1="-0.6" x2="-0.5" y2="-0.3" layer="51" rot="R90"/>
<rectangle x1="-1.125" y1="-0.5" x2="-0.8" y2="-0.425" layer="51" rot="R90"/>
<polygon width="0.2032" layer="21">
<vertex x="-1.9304" y="0.8636"/>
<vertex x="-2.4384" y="0.8636"/>
<vertex x="-2.4384" y="-0.8636"/>
<vertex x="-1.9304" y="-0.8636"/>
</polygon>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="35"/>
</package>
<package name="LED-1206">
<description>&lt;h3&gt;SMD 1206 LED&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	LED
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Size:&lt;/b&gt;	1206
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<wire x1="2.794" y1="-1.0414" x2="2.794" y2="1.0414" width="0.2032" layer="21"/>
<wire x1="2.794" y1="1.0414" x2="-2.794" y2="1.0414" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="1.0414" x2="-2.794" y2="-1.0414" width="0.2032" layer="21"/>
<wire x1="-2.794" y1="-1.0414" x2="2.794" y2="-1.0414" width="0.2032" layer="21"/>
<wire x1="-1.6" y1="-0.4" x2="-1.6" y2="0.4" width="0.1016" layer="51" curve="172.619069"/>
<wire x1="0.95" y1="-0.8" x2="-0.95" y2="-0.8" width="0.1016" layer="51"/>
<wire x1="-0.95" y1="0.8" x2="0.95" y2="0.8" width="0.1016" layer="51"/>
<circle x="-1.425" y="-0.55" radius="0.1" width="0.1016" layer="51"/>
<smd name="C" x="-1.7526" y="0" dx="1.524" dy="1.524" layer="1" rot="R90"/>
<smd name="A" x="1.7526" y="0" dx="1.524" dy="1.524" layer="1" roundness="20" rot="R90"/>
<text x="-2.54" y="1.27" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.016" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8375" y1="-0.6625" x2="-1.3375" y2="-0.5375" layer="51" rot="R90"/>
<rectangle x1="-1.5" y1="-0.9" x2="-1.275" y2="-0.575" layer="51" rot="R90"/>
<rectangle x1="-1.4" y1="-0.5" x2="-1.275" y2="-0.275" layer="51" rot="R90"/>
<rectangle x1="-1.5" y1="-0.5" x2="-1.075" y2="-0.375" layer="51" rot="R90"/>
<rectangle x1="-1.725" y1="0.425" x2="-1.225" y2="0.775" layer="51" rot="R90"/>
<rectangle x1="-1.5875" y1="0.4875" x2="-0.9875" y2="0.6125" layer="51" rot="R90"/>
<rectangle x1="-1.95" y1="-0.15" x2="-0.25" y2="0.15" layer="51" rot="R90"/>
<rectangle x1="0.45" y1="-0.35" x2="2.15" y2="0.35" layer="51" rot="R90"/>
<rectangle x1="-0.725" y1="-0.9" x2="-0.4" y2="-0.475" layer="21" rot="R90"/>
<rectangle x1="-0.725" y1="0.475" x2="-0.4" y2="0.9" layer="21" rot="R90"/>
<rectangle x1="-0.35" y1="-0.175" x2="0" y2="0.175" layer="21" rot="R90"/>
<polygon width="0.2032" layer="21">
<vertex x="-3.302" y="-1.0414"/>
<vertex x="-3.302" y="1.0414"/>
<vertex x="-2.794" y="1.0414"/>
<vertex x="-2.794" y="-1.0414"/>
</polygon>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="35"/>
</package>
<package name="LED-5060">
<text x="-3.97" y="3.81" size="1.016" layer="25">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.016" layer="27" rot="R180">&gt;VALUE</text>
<smd name="1" x="-2.7" y="2" dx="3" dy="1.6" layer="1" rot="R180"/>
<smd name="2" x="-2.7" y="0" dx="3" dy="1.6" layer="1" rot="R180"/>
<smd name="3" x="-2.7" y="-2" dx="3" dy="1.6" layer="1" rot="R180"/>
<smd name="4" x="2.7" y="-2" dx="3" dy="1.6" layer="1" rot="R180"/>
<smd name="5" x="2.7" y="0" dx="3" dy="1.6" layer="1" rot="R180"/>
<smd name="6" x="2.7" y="2" dx="3" dy="1.6" layer="1" rot="R180"/>
<wire x1="4.65" y1="-3.2" x2="4.65" y2="3.2" width="0.2032" layer="21"/>
<wire x1="4.65" y1="3.2" x2="-4.65" y2="3.2" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="3.2" x2="-4.65" y2="-3.2" width="0.2032" layer="21"/>
<wire x1="-4.65" y1="-3.2" x2="4.65" y2="-3.2" width="0.2032" layer="21"/>
<polygon width="0.2032" layer="21">
<vertex x="-4.65" y="3.2"/>
<vertex x="-5.65" y="3.2"/>
<vertex x="-4.65" y="4.2"/>
</polygon>
</package>
<package name="LED-3MM">
<description>&lt;B&gt;LED&lt;/B&gt;&lt;p&gt;
3 mm, round</description>
<wire x1="0" y1="2.032" x2="1.561" y2="1.3009" width="0.2032" layer="21" curve="-50.193108"/>
<wire x1="-1.7929" y1="0.9562" x2="0" y2="2.032" width="0.2032" layer="21" curve="-70"/>
<wire x1="0" y1="-2.032" x2="1.5512" y2="-1.3126" width="0.2032" layer="21" curve="49.763022"/>
<wire x1="-1.7643" y1="-1.0082" x2="0" y2="-2.032" width="0.2032" layer="21" curve="60.255215"/>
<pad name="A" x="-1.27" y="0" drill="0.9" shape="square"/>
<pad name="C" x="1.27" y="0" drill="0.9" shape="octagon"/>
<wire x1="1.561" y1="1.3009" x2="1.561" y2="0.9" width="0.2032" layer="21"/>
<wire x1="1.5512" y1="-0.9" x2="1.5512" y2="-1.3126" width="0.2032" layer="21"/>
<text x="-2.54" y="2.54" size="1.016" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.4925" size="1.016" layer="27">&gt;VALUE</text>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="2.5" width="0.127" layer="51"/>
<wire x1="2.5" y1="2.5" x2="-2.5" y2="2.5" width="0.127" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="LED">
<wire x1="1.27" y1="0" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="0" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-2.032" y1="-0.762" x2="-3.429" y2="-2.159" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="-3.302" y2="-3.302" width="0.1524" layer="94"/>
<text x="3.556" y="-4.572" size="1.778" layer="95" rot="R90">&gt;NAME</text>
<text x="5.715" y="-4.572" size="1.778" layer="96" rot="R90">&gt;VALUE</text>
<pin name="C" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="A" x="0" y="2.54" visible="off" length="short" direction="pas" rot="R270"/>
<polygon width="0.1524" layer="94">
<vertex x="-3.429" y="-2.159"/>
<vertex x="-3.048" y="-1.27"/>
<vertex x="-2.54" y="-1.778"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="-3.302" y="-3.302"/>
<vertex x="-2.921" y="-2.413"/>
<vertex x="-2.413" y="-2.921"/>
</polygon>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED" prefix="LED" uservalue="yes">
<description>&lt;h3&gt;Light Emitting Diode&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	LED
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<gates>
<gate name="G$1" symbol="LED" x="0" y="0"/>
</gates>
<devices>
<device name="0805" package="LED-0805">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1206" package="LED-1206">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="5060" package="LED-5060">
<connects>
<connect gate="G$1" pin="A" pad="4 5 6"/>
<connect gate="G$1" pin="C" pad="1 2 3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="3MM" package="LED-3MM">
<connects>
<connect gate="G$1" pin="A" pad="A"/>
<connect gate="G$1" pin="C" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="pas">
<description>&lt;h2&gt;PetrH's Passive Components Library&lt;/h2&gt;
&lt;h4&gt;© 2012 Petr Havlíček&lt;/h4&gt;
&lt;p&gt;
High quality hand made libraries by Petr Havlíček. Land patterns and silkscreens are based on IPC-7351 through Mentor Graphics IPC-7351 LP Viewer.&lt;/p&gt;</description>
<packages>
<package name="RESC0805">
<description>&lt;h3&gt;Resistor SMD 0805&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Thin Film Chip Resistor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 0805
&lt;b&gt;IPC Name:&lt;/b&gt;	RESC2013X65N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="1.7" y1="1" x2="1.7" y2="-1" width="0.2032" layer="21"/>
<wire x1="1.7" y1="-1" x2="-1.7" y2="-1" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="-1" x2="-1.7" y2="1" width="0.2032" layer="21"/>
<smd name="1" x="-0.95" y="0" dx="1" dy="1.45" layer="1" roundness="20"/>
<smd name="2" x="0.95" y="0" dx="1" dy="1.45" layer="1" roundness="20"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.7" y1="1" x2="1.7" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.7" y1="1" x2="-1.7" y2="-1" width="0.127" layer="39"/>
<wire x1="-1.7" y1="-1" x2="1.7" y2="-1" width="0.127" layer="39"/>
<wire x1="1.7" y1="-1" x2="1.7" y2="1" width="0.127" layer="39"/>
<wire x1="1.7" y1="1" x2="-1.7" y2="1" width="0.127" layer="39"/>
<rectangle x1="-0.1016" y1="-0.4064" x2="0.1016" y2="0.4064" layer="35"/>
</package>
<package name="RESC1206">
<description>&lt;h3&gt;Resistor SMD 1206&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Thin Film Chip Resistor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 1206
&lt;b&gt;IPC Name:&lt;/b&gt;	RESC3216X84N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="-2.3" y1="1.15" x2="2.3" y2="1.15" width="0.2032" layer="21"/>
<wire x1="2.3" y1="1.15" x2="2.3" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="2.3" y1="-1.15" x2="-2.3" y2="-1.15" width="0.2032" layer="21"/>
<wire x1="-2.3" y1="-1.15" x2="-2.3" y2="1.15" width="0.2032" layer="21"/>
<smd name="2" x="1.5" y="0" dx="1.05" dy="1.75" layer="1" roundness="20"/>
<smd name="1" x="-1.5" y="0" dx="1.05" dy="1.75" layer="1" roundness="20"/>
<text x="-2.54" y="1.3335" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-2.6035" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-0.2032" y1="-0.635" x2="0.2032" y2="0.635" layer="35"/>
<wire x1="-2.3" y1="1.15" x2="-2.3" y2="-1.15" width="0.127" layer="39"/>
<wire x1="-2.3" y1="-1.15" x2="2.3" y2="-1.15" width="0.127" layer="39"/>
<wire x1="2.3" y1="-1.15" x2="2.3" y2="1.15" width="0.127" layer="39"/>
<wire x1="2.3" y1="1.15" x2="-2.3" y2="1.15" width="0.127" layer="39"/>
</package>
<package name="RESC0603">
<description>&lt;h3&gt;Resistor SMD 0603&lt;/h3&gt;

&lt;pre&gt;
&lt;b&gt;Type:&lt;/b&gt;	Thin Film Chip Resistor
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Standart:&lt;/b&gt;	EIA 0603
&lt;b&gt;IPC Name:&lt;/b&gt;	RESC1608X84N
&lt;b&gt;Pins:&lt;/b&gt;	2
&lt;/pre&gt;</description>
<wire x1="1.5" y1="0.75" x2="1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="1.5" y1="-0.75" x2="-1.5" y2="-0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="-0.75" x2="-1.5" y2="0.75" width="0.2032" layer="21"/>
<smd name="1" x="-0.85" y="0" dx="0.8" dy="1" layer="1" roundness="20"/>
<smd name="2" x="0.85" y="0" dx="0.8" dy="1" layer="1" roundness="20"/>
<text x="-1.905" y="1.27" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<wire x1="-1.5" y1="0.75" x2="1.5" y2="0.75" width="0.2032" layer="21"/>
<wire x1="-1.5" y1="0.75" x2="-1.5" y2="-0.75" width="0.127" layer="39"/>
<wire x1="-1.5" y1="-0.75" x2="1.5" y2="-0.75" width="0.127" layer="39"/>
<wire x1="1.5" y1="-0.75" x2="1.5" y2="0.75" width="0.127" layer="39"/>
<wire x1="1.5" y1="0.75" x2="-1.5" y2="0.75" width="0.127" layer="39"/>
<rectangle x1="-0.1016" y1="-0.2032" x2="0.1016" y2="0.2032" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RES" prefix="R" uservalue="yes">
<description>&lt;h3&gt;Resistor&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Name:&lt;/b&gt;	Resistor
&lt;b&gt;Type:&lt;/b&gt;	Resistor
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<gates>
<gate name="&gt;NAME" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="C2013" package="RESC0805">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C3216" package="RESC1206">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="C1608" package="RESC0603">
<connects>
<connect gate="&gt;NAME" pin="1" pad="1"/>
<connect gate="&gt;NAME" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="ph-dis">
<description>&lt;h2&gt;PetrH's Dicretes Library&lt;/h2&gt;
&lt;h4&gt;© 2012 Petr Havlíček&lt;/h4&gt;
&lt;p&gt;
High quality hand made libraries by Petr Havlíček. Land patterns and silkscreens are based on IPC-7351 through Mentor Graphics IPC-7351 LP Viewer.&lt;/p&gt;</description>
<packages>
<package name="LED-5050-4">
<wire x1="-2.5" y1="-2.5" x2="2.5" y2="-2.5" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="-2.5" y2="2.5" width="0.127" layer="21"/>
<smd name="1" x="-2.45" y="1.65" dx="1.5" dy="0.9" layer="1"/>
<smd name="2" x="-2.45" y="-1.65" dx="1.5" dy="0.9" layer="1"/>
<smd name="3" x="2.45" y="-1.65" dx="1.5" dy="0.9" layer="1"/>
<smd name="4" x="2.45" y="1.65" dx="1.5" dy="0.9" layer="1"/>
<circle x="0" y="0" radius="1.27" width="0.127" layer="21"/>
<text x="-3.97" y="2.54" size="1.27" layer="25" font="vector" ratio="10">&gt;NAME</text>
<text x="-5.08" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.127" layer="21">
<vertex x="2.5" y="-2.5"/>
<vertex x="2.5" y="-3.81"/>
<vertex x="3.81" y="-2.5"/>
</polygon>
<wire x1="-2.5" y1="1" x2="-2.5" y2="-1" width="0.127" layer="21"/>
<wire x1="2.5" y1="1" x2="2.5" y2="-1" width="0.127" layer="21"/>
<wire x1="-2.5" y1="-2.5" x2="-2.5" y2="-2.3" width="0.127" layer="21"/>
<wire x1="2.5" y1="-2.5" x2="2.5" y2="-2.3" width="0.127" layer="21"/>
<wire x1="2.5" y1="2.5" x2="2.5" y2="2.3" width="0.127" layer="21"/>
<wire x1="-2.5" y1="2.5" x2="-2.5" y2="2.3" width="0.127" layer="21"/>
</package>
<package name="D-DIOM5336X240N">
<description>&lt;h3&gt;SMD DO-214AA SMB Diode&lt;/h3&gt;&lt;pre&gt;
&lt;b&gt;Name:&lt;/b&gt;	Shrink Outline Diode
&lt;b&gt;IPC Name:&lt;/b&gt;	DIOM5336X240N
&lt;b&gt;Standart:&lt;/b&gt;	JEDEC DO-214AA
&lt;b&gt;Type:&lt;/b&gt;	Diode
&lt;b&gt;Mounting:&lt;/b&gt;	SMT
&lt;b&gt;Pins:&lt;/b&gt;	2</description>
<smd name="C" x="-2.25" y="0" dx="2.1" dy="2.1" layer="1"/>
<smd name="A" x="2.25" y="0" dx="2.1" dy="2.1" layer="1"/>
<wire x1="-2.65" y1="1.8" x2="2.65" y2="1.8" width="0.2032" layer="21"/>
<wire x1="2.65" y1="1.8" x2="2.65" y2="1.27" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.27" x2="2.65" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="2.65" y1="-1.8" x2="-2.65" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="-3.55" y1="2.15" x2="-3.55" y2="-2.15" width="0.2032" layer="39"/>
<wire x1="-3.55" y1="-2.15" x2="3.55" y2="-2.15" width="0.2032" layer="39"/>
<wire x1="3.55" y1="-2.15" x2="3.55" y2="2.15" width="0.2032" layer="39"/>
<wire x1="3.55" y1="2.15" x2="-3.55" y2="2.15" width="0.2032" layer="39"/>
<text x="-3.81" y="2.8575" size="1.27" layer="25" font="vector" ratio="10" rot="SR0">&gt;NAME</text>
<text x="-3.81" y="-3.81" size="1.27" layer="27" font="vector" ratio="10">&gt;VALUE</text>
<polygon width="0.2032" layer="21">
<vertex x="-3.5" y="1.8"/>
<vertex x="-3.5" y="1.27"/>
<vertex x="-2.65" y="1.27"/>
<vertex x="-2.65" y="1.8"/>
</polygon>
<polygon width="0.2032" layer="21">
<vertex x="-3.5" y="-1.27"/>
<vertex x="-3.5" y="-1.8"/>
<vertex x="-2.65" y="-1.8"/>
<vertex x="-2.65" y="-1.27"/>
</polygon>
<rectangle x1="-0.3175" y1="-0.3175" x2="0.3175" y2="0.3175" layer="35"/>
</package>
</packages>
<symbols>
<symbol name="LED-WS2812B">
<wire x1="-10.16" y1="5.08" x2="-10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="5.08" width="0.254" layer="94"/>
<wire x1="10.16" y1="5.08" x2="-10.16" y2="5.08" width="0.254" layer="94"/>
<pin name="VDD" x="15.24" y="2.54" length="middle" rot="R180"/>
<pin name="DOUT" x="-15.24" y="-2.54" length="middle"/>
<pin name="DIN" x="-15.24" y="2.54" length="middle"/>
<pin name="VSS" x="15.24" y="-2.54" length="middle" rot="R180"/>
<text x="-10.16" y="6.35" size="1.778" layer="95">&gt;NAME</text>
<text x="10.16" y="-6.35" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
</symbol>
<symbol name="D-TVS">
<wire x1="-2.54" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.27" x2="-2.54" y2="-1.27" width="0.254" layer="94"/>
<pin name="C1" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="A1" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<wire x1="0" y1="1.27" x2="0.5715" y2="1.778" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-0.5715" y2="-1.778" width="0.254" layer="94"/>
<text x="-5.08" y="4.699" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="2.54" size="1.778" layer="96">&gt;VALUE</text>
<wire x1="2.54" y1="-1.27" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="2.54" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-1.27" x2="2.54" y2="1.27" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="LED-WS2812B" prefix="LED">
<gates>
<gate name="G$1" symbol="LED-WS2812B" x="0" y="0"/>
</gates>
<devices>
<device name="" package="LED-5050-4">
<connects>
<connect gate="G$1" pin="DIN" pad="4"/>
<connect gate="G$1" pin="DOUT" pad="2"/>
<connect gate="G$1" pin="VDD" pad="1"/>
<connect gate="G$1" pin="VSS" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D-TVS" prefix="D">
<gates>
<gate name="G$1" symbol="D-TVS" x="0" y="0"/>
</gates>
<devices>
<device name="-SMB" package="D-DIOM5336X240N">
<connects>
<connect gate="G$1" pin="A1" pad="A"/>
<connect gate="G$1" pin="C1" pad="C"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME2" library="ph-oth" deviceset="FRAME-A4-PORTRAIT" device="" value="A4P"/>
<part name="FRAME1" library="ph-oth" deviceset="FRAME-A4-PORTRAIT" device="" value="A4P"/>
<part name="J18" library="ph-con" deviceset="PINHD-2X02" device="" value="ZL202-4G"/>
<part name="J16" library="ph-con" deviceset="PINHD-2X04" device="" value="ZL202-8G"/>
<part name="+3V37" library="ph-oth" deviceset="+3V3" device=""/>
<part name="P+14" library="ph-oth" deviceset="+5V" device=""/>
<part name="P+15" library="ph-oth" deviceset="+12V" device=""/>
<part name="GND12" library="ph-oth" deviceset="GND" device=""/>
<part name="GND13" library="ph-oth" deviceset="GND" device=""/>
<part name="SUP15" library="ph-oth" deviceset="-12V" device=""/>
<part name="SUP14" library="ph-oth" deviceset="-5V" device=""/>
<part name="J15" library="ph-con" deviceset="PINHD-2X02" device="" value="ZL202-4G"/>
<part name="J13" library="ph-con" deviceset="PINHD-2X04" device="" value="ZL202-8G"/>
<part name="+3V36" library="ph-oth" deviceset="+3V3" device=""/>
<part name="P+12" library="ph-oth" deviceset="+5V" device=""/>
<part name="P+13" library="ph-oth" deviceset="+12V" device=""/>
<part name="GND10" library="ph-oth" deviceset="GND" device=""/>
<part name="GND11" library="ph-oth" deviceset="GND" device=""/>
<part name="SUP13" library="ph-oth" deviceset="-12V" device=""/>
<part name="SUP12" library="ph-oth" deviceset="-5V" device=""/>
<part name="J12" library="ph-con" deviceset="PINHD-2X02" device="" value="ZL202-4G"/>
<part name="J10" library="ph-con" deviceset="PINHD-2X04" device="" value="ZL202-8G"/>
<part name="+3V35" library="ph-oth" deviceset="+3V3" device=""/>
<part name="P+10" library="ph-oth" deviceset="+5V" device=""/>
<part name="P+11" library="ph-oth" deviceset="+12V" device=""/>
<part name="GND8" library="ph-oth" deviceset="GND" device=""/>
<part name="GND9" library="ph-oth" deviceset="GND" device=""/>
<part name="SUP11" library="ph-oth" deviceset="-12V" device=""/>
<part name="SUP10" library="ph-oth" deviceset="-5V" device=""/>
<part name="J9" library="ph-con" deviceset="PINHD-2X02" device="" value="ZL202-4G"/>
<part name="J7" library="ph-con" deviceset="PINHD-2X04" device="" value="ZL202-8G"/>
<part name="+3V34" library="ph-oth" deviceset="+3V3" device=""/>
<part name="P+8" library="ph-oth" deviceset="+5V" device=""/>
<part name="P+9" library="ph-oth" deviceset="+12V" device=""/>
<part name="GND5" library="ph-oth" deviceset="GND" device=""/>
<part name="GND6" library="ph-oth" deviceset="GND" device=""/>
<part name="SUP9" library="ph-oth" deviceset="-12V" device=""/>
<part name="SUP8" library="ph-oth" deviceset="-5V" device=""/>
<part name="J6" library="ph-con" deviceset="PINHD-2X02" device="" value="ZL202-4G"/>
<part name="J4" library="ph-con" deviceset="PINHD-2X04" device="" value="ZL202-8G"/>
<part name="+3V33" library="ph-oth" deviceset="+3V3" device=""/>
<part name="P+6" library="ph-oth" deviceset="+5V" device=""/>
<part name="P+7" library="ph-oth" deviceset="+12V" device=""/>
<part name="GND3" library="ph-oth" deviceset="GND" device=""/>
<part name="GND4" library="ph-oth" deviceset="GND" device=""/>
<part name="SUP7" library="ph-oth" deviceset="-12V" device=""/>
<part name="SUP6" library="ph-oth" deviceset="-5V" device=""/>
<part name="GND19" library="ph-oth" deviceset="GND" device=""/>
<part name="IC4" library="ph-ic" deviceset="FT230X" device="S"/>
<part name="GND20" library="ph-oth" deviceset="GND" device=""/>
<part name="C22" library="ph-pas" deviceset="CAPN" device="C3216" value="10nF"/>
<part name="C27" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="GND22" library="ph-oth" deviceset="GND" device=""/>
<part name="C23" library="ph-pas" deviceset="CAPN" device="C3216" value="47pF"/>
<part name="C24" library="ph-pas" deviceset="CAPN" device="C3216" value="47pF"/>
<part name="R26" library="ph-pas" deviceset="RES" device="C3216" value="27R"/>
<part name="R27" library="ph-pas" deviceset="RES" device="C3216" value="27R"/>
<part name="C25" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C26" library="ph-pas" deviceset="CAPN" device="C3216" value="4.7uF/6.3V"/>
<part name="GND21" library="ph-oth" deviceset="GND" device=""/>
<part name="CON1" library="ph-con" deviceset="USB-B-F" device="-TH"/>
<part name="L1" library="ph-pas" deviceset="L" device="'-1206'" value="Bead-1206"/>
<part name="C12" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C9" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C14" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C16" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C17" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="J23" library="ph-con" deviceset="PINHD-1X04" device="" value="ZL262-4SG"/>
<part name="SW1" library="ph-oth" deviceset="SW-TACT" device="KS01-B" value="KS01-B-B"/>
<part name="C18" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="GND15" library="ph-oth" deviceset="GND" device=""/>
<part name="R22" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="SW2" library="ph-oth" deviceset="SW-TACT" device="KS01-B" value="KS01-B-B"/>
<part name="C19" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="GND16" library="ph-oth" deviceset="GND" device=""/>
<part name="R23" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="SW3" library="ph-oth" deviceset="SW-TACT" device="KS01-B" value="KS01-B-B"/>
<part name="C20" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="GND17" library="ph-oth" deviceset="GND" device=""/>
<part name="R24" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="SW4" library="ph-oth" deviceset="SW-TACT" device="KS01-B" value="KS01-B-B"/>
<part name="C21" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="GND18" library="ph-oth" deviceset="GND" device=""/>
<part name="R25" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="LED17" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED10" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED11" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED12" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED13" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED14" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED15" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED16" library="dis" deviceset="LED" device="1206" value="GREEN"/>
<part name="LED18" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="LED19" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="LED20" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="LED21" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="LED22" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="LED23" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="LED24" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="LED25" library="dis" deviceset="LED" device="1206" value="RED"/>
<part name="R6" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R8" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R10" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R12" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R14" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R16" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R18" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R20" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R7" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R9" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R11" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R13" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R15" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R17" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R19" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="R21" library="pas" deviceset="RES" device="C3216" value="330R"/>
<part name="J20" library="ph-con" deviceset="PINHD-1X08" device="" value="ZL262-8SG"/>
<part name="GND14" library="ph-oth" deviceset="GND" device=""/>
<part name="ENC1" library="ph-oth" deviceset="ENCODER" device="" value="EC12E24204A9"/>
<part name="R31" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="R32" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="GND26" library="ph-oth" deviceset="GND" device=""/>
<part name="J33" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL262-2SG"/>
<part name="SUP20" library="ph-oth" deviceset="VCC" device=""/>
<part name="SUP19" library="ph-oth" deviceset="VCC" device=""/>
<part name="+3V39" library="ph-oth" deviceset="+3V3" device=""/>
<part name="P+18" library="ph-oth" deviceset="+5V" device=""/>
<part name="J21" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL201-02G"/>
<part name="J19" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL201-02G"/>
<part name="SUP17" library="ph-oth" deviceset="VCC" device=""/>
<part name="SUP16" library="ph-oth" deviceset="VCC" device=""/>
<part name="J32" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL201-02G"/>
<part name="LED6" library="ph-dis" deviceset="LED-WS2812B" device="" value="WS2812B"/>
<part name="SUP5" library="ph-oth" deviceset="VCC" device=""/>
<part name="J3" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL201-02G"/>
<part name="LED7" library="ph-dis" deviceset="LED-WS2812B" device="" value="WS2812B"/>
<part name="LED8" library="ph-dis" deviceset="LED-WS2812B" device="" value="WS2812B"/>
<part name="LED9" library="ph-dis" deviceset="LED-WS2812B" device="" value="WS2812B"/>
<part name="GND7" library="ph-oth" deviceset="GND" device=""/>
<part name="C11" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C13" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C15" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C10" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="J2" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL262-2SG"/>
<part name="P+17" library="ph-oth" deviceset="+5V_USB" device=""/>
<part name="P+16" library="ph-oth" deviceset="+5V_USB" device=""/>
<part name="J29" library="ph-oth" deviceset="MOD-ESP8266-01" device="" value="ESP8266-01"/>
<part name="GND25" library="ph-oth" deviceset="GND" device=""/>
<part name="J30" library="ph-con" deviceset="PINHD-1X04" device="" value="ZL262-4SG"/>
<part name="R30" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="R29" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="R28" library="ph-pas" deviceset="RES" device="C3216" value="10k"/>
<part name="+3V38" library="ph-oth" deviceset="+3V3" device=""/>
<part name="C28" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="GND24" library="ph-oth" deviceset="GND" device=""/>
<part name="J28" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL201-02G"/>
<part name="FRAME3" library="ph-oth" deviceset="FRAME-A4-PORTRAIT" device="" value="A4P"/>
<part name="P1" library="ph-con" deviceset="PAD" device="-BAN4MM" value="PJ3230-R"/>
<part name="P2" library="ph-con" deviceset="PAD" device="-BAN4MM" value="PJ3230-G"/>
<part name="P3" library="ph-con" deviceset="PAD" device="-BAN4MM" value="PJ3230-B"/>
<part name="GND1" library="ph-oth" deviceset="GND" device=""/>
<part name="SUP2" library="ph-oth" deviceset="-12V" device=""/>
<part name="P+3" library="ph-oth" deviceset="+12V" device=""/>
<part name="CON2" library="ph-con" deviceset="SVOR-2" device="-AKZ120-2" value="AKZ120-2"/>
<part name="CON3" library="ph-con" deviceset="SVOR-2" device="-AKZ120-2" value="AKZ120-2"/>
<part name="CON4" library="ph-con" deviceset="SVOR-2" device="-AKZ120-2" value="AKZ120-2"/>
<part name="J27" library="ph-con" deviceset="PINHD-1X06" device="" value="ZL262-6SG"/>
<part name="TR1" library="ph-pas" deviceset="R-TRIM" device="CA14V" value="CA14NV12,5-10KA2020"/>
<part name="J26" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL201-02G"/>
<part name="J25" library="ph-con" deviceset="PINHD-1X02" device="" value="ZL201-02G"/>
<part name="SUP18" library="ph-oth" deviceset="VCC" device=""/>
<part name="J24" library="ph-con" deviceset="PINHD-1X03" device="" value="ZL262-3SG"/>
<part name="J1" library="ph-con" deviceset="PINHD-1X03" device="" value="ZL201-03G"/>
<part name="P+1" library="ph-oth" deviceset="+5V" device=""/>
<part name="P+2" library="ph-oth" deviceset="+5V_USB" device=""/>
<part name="IC1" library="ph-ic" deviceset="78XX" device="TO220" value="7805ACV"/>
<part name="IC3" library="ph-ic" deviceset="79XX" device="TO220" value="7905ACV"/>
<part name="IC2" library="ph-ic" deviceset="LD1117" device="TO220" value="LD1117AV33"/>
<part name="+3V31" library="ph-oth" deviceset="+3V3" device=""/>
<part name="C2" library="ph-pas" deviceset="CAPN" device="C3216" value="330nF"/>
<part name="C7" library="ph-pas" deviceset="CAPN" device="C3216" value="2.2uF"/>
<part name="C3" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C8" library="ph-pas" deviceset="CAPN" device="C3216" value="1uF"/>
<part name="SUP3" library="ph-oth" deviceset="-5V" device=""/>
<part name="C4" library="ph-pas" deviceset="CAPN" device="C3216" value="100nF"/>
<part name="C5" library="ph-pas" deviceset="CAPN" device="C3216" value="10uF"/>
<part name="LED1" library="ph-pas" deviceset="LED" device="1206" value="Green"/>
<part name="LED2" library="ph-pas" deviceset="LED" device="1206" value="Green"/>
<part name="LED3" library="ph-pas" deviceset="LED" device="1206" value="Green"/>
<part name="LED5" library="ph-pas" deviceset="LED" device="1206" value="Green"/>
<part name="LED4" library="ph-pas" deviceset="LED" device="1206" value="Green"/>
<part name="+3V32" library="ph-oth" deviceset="+3V3" device=""/>
<part name="P+5" library="ph-oth" deviceset="+5V" device=""/>
<part name="P+4" library="ph-oth" deviceset="+12V" device=""/>
<part name="R2" library="ph-pas" deviceset="RES" device="C3216" value="4k7"/>
<part name="R3" library="ph-pas" deviceset="RES" device="C3216" value="1k5"/>
<part name="R4" library="ph-pas" deviceset="RES" device="C3216" value="680R"/>
<part name="GND2" library="ph-oth" deviceset="GND" device=""/>
<part name="R1" library="ph-pas" deviceset="RES" device="C3216" value="1k5"/>
<part name="R5" library="ph-pas" deviceset="RES" device="C3216" value="4k7"/>
<part name="SUP4" library="ph-oth" deviceset="-12V" device=""/>
<part name="SUP1" library="ph-oth" deviceset="-5V" device=""/>
<part name="HS1" library="ph-oth" deviceset="HEATSINK" device="ICK35SA" value="ICK35SA"/>
<part name="HS3" library="ph-oth" deviceset="HEATSINK" device="ICK35SA" value="ICK35SA"/>
<part name="HS2" library="ph-oth" deviceset="HEATSINK" device="ICK35SA" value="ICK35SA"/>
<part name="J31" library="ph-con" deviceset="PINHD-1X03" device="" value="ZL201-03G"/>
<part name="J22" library="ph-con" deviceset="PINHD-1X04" device="" value="ZL262-4SG"/>
<part name="GND23" library="ph-oth" deviceset="GND" device=""/>
<part name="J5" library="ph-con" deviceset="PINHD-2X03" device="" value="ZL202-6G"/>
<part name="J8" library="ph-con" deviceset="PINHD-2X03" device="" value="ZL202-6G"/>
<part name="J11" library="ph-con" deviceset="PINHD-2X03" device="" value="ZL202-6G"/>
<part name="J14" library="ph-con" deviceset="PINHD-2X03" device="" value="ZL202-6G"/>
<part name="J17" library="ph-con" deviceset="PINHD-2X03" device="" value="ZL202-6G"/>
<part name="LOGO1" library="ph-oth" deviceset="LOGO" device="-SILK"/>
<part name="F1" library="ph-pas" deviceset="FUSE" device="-RB250" value="RB250-30"/>
<part name="F2" library="ph-pas" deviceset="FUSE" device="-RB250" value="RB250-30"/>
<part name="D1" library="ph-dis" deviceset="D-TVS" device="-SMB" value="SM6T15CA"/>
<part name="D2" library="ph-dis" deviceset="D-TVS" device="-SMB" value="SM6T15CA"/>
<part name="C1" library="ph-pas" deviceset="CAPP" device="10X5" value="470uF/35"/>
<part name="C6" library="ph-pas" deviceset="CAPP" device="10X5" value="470uF/35"/>
<part name="LOGO2" library="ph-oth" deviceset="LOGO" device="-SILK"/>
<part name="LOGO3" library="ph-oth" deviceset="LOGO" device="-SILK"/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="5.08" y1="177.8" x2="111.76" y2="177.8" width="0.4064" layer="97"/>
<text x="7.62" y="175.26" size="3.81" layer="97" font="vector" rot="MR180">Voltage selectors</text>
<wire x1="5.08" y1="5.08" x2="5.08" y2="177.8" width="0.4064" layer="97"/>
<wire x1="111.76" y1="5.08" x2="111.76" y2="177.8" width="0.4064" layer="97"/>
<wire x1="5.08" y1="5.08" x2="111.76" y2="5.08" width="0.4064" layer="97"/>
<wire x1="114.3" y1="177.8" x2="177.8" y2="177.8" width="0.4064" layer="97"/>
<wire x1="114.3" y1="78.74" x2="114.3" y2="177.8" width="0.4064" layer="97"/>
<text x="116.84" y="175.26" size="3.81" layer="97" font="vector" rot="MR180">RGB LEDs</text>
<wire x1="114.3" y1="78.74" x2="177.8" y2="78.74" width="0.4064" layer="97"/>
<wire x1="177.8" y1="78.74" x2="177.8" y2="177.8" width="0.4064" layer="97"/>
<text x="7.62" y="256.54" size="3.81" layer="97" font="vector" rot="MR180">PSU</text>
<wire x1="5.08" y1="259.08" x2="177.8" y2="259.08" width="0.4064" layer="97"/>
<wire x1="177.8" y1="180.34" x2="177.8" y2="259.08" width="0.4064" layer="97"/>
<wire x1="5.08" y1="180.34" x2="177.8" y2="180.34" width="0.4064" layer="97"/>
<wire x1="5.08" y1="180.34" x2="5.08" y2="259.08" width="0.4064" layer="97"/>
<text x="167.64" y="5.08" size="2.1844" layer="94" font="vector">v1.0c</text>
<polygon width="0.00038125" layer="94">
<vertex x="172.184784375" y="38.340025" curve="9.499998"/>
<vertex x="172.956115625" y="38.7372875"/>
<vertex x="174.871340625" y="37.175359375"/>
<vertex x="176.184640625" y="38.488659375"/>
<vertex x="174.6227125" y="40.403884375" curve="19.000007"/>
<vertex x="175.28448125" y="42.0015375"/>
<vertex x="177.743203125" y="42.25135625"/>
<vertex x="177.743203125" y="44.10864375"/>
<vertex x="175.28448125" y="44.3584625" curve="19.000007"/>
<vertex x="174.6227125" y="45.956115625"/>
<vertex x="176.184640625" y="47.871340625"/>
<vertex x="174.871340625" y="49.184640625"/>
<vertex x="172.956115625" y="47.6227125" curve="19.000007"/>
<vertex x="171.3584625" y="48.28448125"/>
<vertex x="171.10864375" y="50.743203125"/>
<vertex x="169.25135625" y="50.743203125"/>
<vertex x="169.0015375" y="48.28448125" curve="19.000007"/>
<vertex x="167.403884375" y="47.6227125"/>
<vertex x="165.488659375" y="49.184640625"/>
<vertex x="164.175359375" y="47.871340625"/>
<vertex x="165.7372875" y="45.956115625" curve="19.000007"/>
<vertex x="165.07551875" y="44.3584625"/>
<vertex x="162.616796875" y="44.10864375"/>
<vertex x="162.616796875" y="42.25135625"/>
<vertex x="165.07551875" y="42.0015375" curve="19.000007"/>
<vertex x="165.7372875" y="40.403884375"/>
<vertex x="164.175359375" y="38.488659375"/>
<vertex x="165.488659375" y="37.175359375"/>
<vertex x="167.403884375" y="38.7372875" curve="9.499998"/>
<vertex x="168.175215625" y="38.340025"/>
<vertex x="169.268734375" y="40.9800125" curve="-67.499973"/>
<vertex x="167.79875" y="43.18" curve="-247.499909"/>
<vertex x="171.091265625" y="40.9800125"/>
</polygon>
<text x="180.34" y="35.56" size="3.81" layer="94" rot="R180">License
CC-BY-SA</text>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="J18" gate="G$1" x="60.96" y="17.78"/>
<instance part="J16" gate="G$1" x="40.64" y="25.4"/>
<instance part="+3V37" gate="G$1" x="15.24" y="33.02"/>
<instance part="P+14" gate="1" x="22.86" y="33.02"/>
<instance part="P+15" gate="1" x="30.48" y="33.02"/>
<instance part="GND12" gate="1" x="30.48" y="15.24"/>
<instance part="GND13" gate="1" x="91.44" y="15.24"/>
<instance part="SUP15" gate="G$1" x="106.68" y="17.78" rot="R180"/>
<instance part="SUP14" gate="G$1" x="99.06" y="17.78" rot="R180"/>
<instance part="J15" gate="G$1" x="60.96" y="50.8"/>
<instance part="J13" gate="G$1" x="40.64" y="58.42"/>
<instance part="+3V36" gate="G$1" x="15.24" y="66.04"/>
<instance part="P+12" gate="1" x="22.86" y="66.04"/>
<instance part="P+13" gate="1" x="30.48" y="66.04"/>
<instance part="GND10" gate="1" x="30.48" y="48.26"/>
<instance part="GND11" gate="1" x="91.44" y="48.26"/>
<instance part="SUP13" gate="G$1" x="106.68" y="50.8" rot="R180"/>
<instance part="SUP12" gate="G$1" x="99.06" y="50.8" rot="R180"/>
<instance part="J12" gate="G$1" x="60.96" y="83.82"/>
<instance part="J10" gate="G$1" x="40.64" y="91.44"/>
<instance part="+3V35" gate="G$1" x="15.24" y="99.06"/>
<instance part="P+10" gate="1" x="22.86" y="99.06"/>
<instance part="P+11" gate="1" x="30.48" y="99.06"/>
<instance part="GND8" gate="1" x="30.48" y="81.28"/>
<instance part="GND9" gate="1" x="91.44" y="81.28"/>
<instance part="SUP11" gate="G$1" x="106.68" y="83.82" rot="R180"/>
<instance part="SUP10" gate="G$1" x="99.06" y="83.82" rot="R180"/>
<instance part="J9" gate="G$1" x="60.96" y="116.84"/>
<instance part="J7" gate="G$1" x="40.64" y="124.46"/>
<instance part="+3V34" gate="G$1" x="15.24" y="132.08"/>
<instance part="P+8" gate="1" x="22.86" y="132.08"/>
<instance part="P+9" gate="1" x="30.48" y="132.08"/>
<instance part="GND5" gate="1" x="30.48" y="114.3"/>
<instance part="GND6" gate="1" x="91.44" y="114.3"/>
<instance part="SUP9" gate="G$1" x="106.68" y="116.84" rot="R180"/>
<instance part="SUP8" gate="G$1" x="99.06" y="116.84" rot="R180"/>
<instance part="J6" gate="G$1" x="60.96" y="149.86"/>
<instance part="J4" gate="G$1" x="40.64" y="157.48"/>
<instance part="+3V33" gate="G$1" x="15.24" y="165.1"/>
<instance part="P+6" gate="1" x="22.86" y="165.1"/>
<instance part="P+7" gate="1" x="30.48" y="165.1"/>
<instance part="GND3" gate="1" x="30.48" y="147.32"/>
<instance part="GND4" gate="1" x="91.44" y="147.32"/>
<instance part="SUP7" gate="G$1" x="106.68" y="149.86" rot="R180"/>
<instance part="SUP6" gate="G$1" x="99.06" y="149.86" rot="R180"/>
<instance part="C12" gate="G$1" x="60.96" y="127" rot="R90"/>
<instance part="C9" gate="G$1" x="60.96" y="160.02" rot="R90"/>
<instance part="C14" gate="G$1" x="60.96" y="93.98" rot="R90"/>
<instance part="C16" gate="G$1" x="60.96" y="60.96" rot="R90"/>
<instance part="C17" gate="G$1" x="60.96" y="27.94" rot="R90"/>
<instance part="LED6" gate="G$1" x="137.16" y="144.78"/>
<instance part="SUP5" gate="G$1" x="157.48" y="170.18"/>
<instance part="J3" gate="G$1" x="167.64" y="162.56" smashed="yes">
<attribute name="NAME" x="167.64" y="168.275" size="1.778" layer="95"/>
<attribute name="VALUE" x="177.8" y="154.94" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED7" gate="G$1" x="137.16" y="127"/>
<instance part="LED8" gate="G$1" x="137.16" y="109.22"/>
<instance part="LED9" gate="G$1" x="137.16" y="91.44"/>
<instance part="GND7" gate="1" x="160.02" y="83.82"/>
<instance part="C11" gate="G$1" x="165.1" y="129.54"/>
<instance part="C13" gate="G$1" x="165.1" y="111.76"/>
<instance part="C15" gate="G$1" x="165.1" y="93.98"/>
<instance part="C10" gate="G$1" x="165.1" y="147.32"/>
<instance part="J2" gate="G$1" x="127" y="162.56" rot="MR180"/>
<instance part="P1" gate="G$1" x="12.7" y="236.22" rot="MR0"/>
<instance part="P2" gate="G$1" x="12.7" y="218.44" rot="MR0"/>
<instance part="P3" gate="G$1" x="12.7" y="200.66" rot="MR0"/>
<instance part="GND1" gate="1" x="96.52" y="205.74"/>
<instance part="SUP2" gate="G$1" x="45.72" y="195.58" rot="R180"/>
<instance part="P+3" gate="1" x="45.72" y="243.84"/>
<instance part="J1" gate="G$1" x="93.98" y="246.38" rot="MR0"/>
<instance part="P+1" gate="1" x="104.14" y="254"/>
<instance part="P+2" gate="G$1" x="114.3" y="254"/>
<instance part="IC1" gate="G$1" x="66.04" y="233.68"/>
<instance part="IC3" gate="G$1" x="66.04" y="205.74"/>
<instance part="IC2" gate="G$1" x="121.92" y="226.06"/>
<instance part="+3V31" gate="G$1" x="144.78" y="233.68"/>
<instance part="C2" gate="G$1" x="45.72" y="226.06" smashed="yes">
<attribute name="NAME" x="50.419" y="222.504" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="52.959" y="222.504" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C7" gate="G$1" x="45.72" y="208.28" smashed="yes">
<attribute name="NAME" x="50.419" y="204.724" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="52.959" y="204.724" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C3" gate="G$1" x="83.82" y="226.06"/>
<instance part="C8" gate="G$1" x="83.82" y="208.28"/>
<instance part="SUP3" gate="G$1" x="88.9" y="195.58" rot="R180"/>
<instance part="C4" gate="G$1" x="104.14" y="218.44"/>
<instance part="C5" gate="G$1" x="139.7" y="218.44"/>
<instance part="LED1" gate="G$1" x="149.86" y="213.36"/>
<instance part="LED2" gate="G$1" x="160.02" y="213.36"/>
<instance part="LED3" gate="G$1" x="170.18" y="213.36"/>
<instance part="LED5" gate="G$1" x="137.16" y="193.04" rot="R270"/>
<instance part="LED4" gate="G$1" x="137.16" y="203.2" rot="R270"/>
<instance part="+3V32" gate="G$1" x="170.18" y="220.98"/>
<instance part="P+5" gate="1" x="160.02" y="220.98"/>
<instance part="P+4" gate="1" x="149.86" y="220.98"/>
<instance part="R2" gate="&gt;NAME" x="149.86" y="200.66" rot="R90"/>
<instance part="R3" gate="&gt;NAME" x="160.02" y="200.66" rot="R90"/>
<instance part="R4" gate="&gt;NAME" x="170.18" y="200.66" rot="R90"/>
<instance part="GND2" gate="1" x="170.18" y="187.96"/>
<instance part="R1" gate="&gt;NAME" x="124.46" y="203.2"/>
<instance part="R5" gate="&gt;NAME" x="124.46" y="193.04"/>
<instance part="SUP4" gate="G$1" x="116.84" y="190.5" rot="R180"/>
<instance part="SUP1" gate="G$1" x="111.76" y="200.66" rot="R180"/>
<instance part="HS1" gate="G$1" x="68.58" y="246.38" rot="R90"/>
<instance part="HS3" gate="G$1" x="63.5" y="190.5" rot="R270"/>
<instance part="HS2" gate="G$1" x="124.46" y="238.76" rot="R90"/>
<instance part="J5" gate="G$1" x="81.28" y="157.48"/>
<instance part="J8" gate="G$1" x="81.28" y="124.46"/>
<instance part="J11" gate="G$1" x="81.28" y="91.44"/>
<instance part="J14" gate="G$1" x="81.28" y="58.42"/>
<instance part="J17" gate="G$1" x="81.28" y="25.4"/>
<instance part="LOGO1" gate="G$1" x="149.86" y="40.64"/>
<instance part="F1" gate="G$1" x="22.86" y="236.22"/>
<instance part="F2" gate="G$1" x="22.86" y="200.66"/>
<instance part="D1" gate="G$1" x="30.48" y="226.06" rot="R90"/>
<instance part="D2" gate="G$1" x="30.48" y="210.82" rot="MR270"/>
<instance part="C1" gate="G$1" x="38.1" y="226.06" smashed="yes">
<attribute name="NAME" x="40.1574" y="222.123" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="42.6974" y="222.123" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="C6" gate="G$1" x="38.1" y="208.28" smashed="yes">
<attribute name="NAME" x="40.1574" y="204.343" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="42.6974" y="204.343" size="1.778" layer="96" rot="R90"/>
</instance>
</instances>
<busses>
</busses>
<nets>
<net name="N$12" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="8"/>
<pinref part="J18" gate="G$1" pin="1"/>
<wire x1="48.26" y1="20.32" x2="50.8" y2="20.32" width="0.1524" layer="91"/>
<pinref part="J18" gate="G$1" pin="3"/>
<wire x1="50.8" y1="20.32" x2="53.34" y2="20.32" width="0.1524" layer="91"/>
<wire x1="53.34" y1="15.24" x2="50.8" y2="15.24" width="0.1524" layer="91"/>
<wire x1="50.8" y1="15.24" x2="50.8" y2="20.32" width="0.1524" layer="91"/>
<junction x="50.8" y="20.32"/>
<pinref part="J16" gate="G$1" pin="6"/>
<wire x1="48.26" y1="22.86" x2="50.8" y2="22.86" width="0.1524" layer="91"/>
<wire x1="50.8" y1="22.86" x2="50.8" y2="20.32" width="0.1524" layer="91"/>
<pinref part="J16" gate="G$1" pin="4"/>
<wire x1="48.26" y1="25.4" x2="50.8" y2="25.4" width="0.1524" layer="91"/>
<wire x1="50.8" y1="25.4" x2="50.8" y2="22.86" width="0.1524" layer="91"/>
<junction x="50.8" y="22.86"/>
<pinref part="J16" gate="G$1" pin="2"/>
<wire x1="48.26" y1="27.94" x2="50.8" y2="27.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="27.94" x2="50.8" y2="25.4" width="0.1524" layer="91"/>
<junction x="50.8" y="25.4"/>
<pinref part="C17" gate="G$1" pin="1"/>
<wire x1="50.8" y1="27.94" x2="55.88" y2="27.94" width="0.1524" layer="91"/>
<junction x="50.8" y="27.94"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="J18" gate="G$1" pin="2"/>
<wire x1="68.58" y1="20.32" x2="71.12" y2="20.32" width="0.1524" layer="91"/>
<pinref part="J18" gate="G$1" pin="4"/>
<wire x1="68.58" y1="15.24" x2="71.12" y2="15.24" width="0.1524" layer="91"/>
<wire x1="71.12" y1="15.24" x2="71.12" y2="20.32" width="0.1524" layer="91"/>
<junction x="71.12" y="20.32"/>
<wire x1="73.66" y1="22.86" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
<wire x1="71.12" y1="22.86" x2="71.12" y2="20.32" width="0.1524" layer="91"/>
<wire x1="73.66" y1="25.4" x2="71.12" y2="25.4" width="0.1524" layer="91"/>
<wire x1="71.12" y1="25.4" x2="71.12" y2="22.86" width="0.1524" layer="91"/>
<junction x="71.12" y="22.86"/>
<wire x1="73.66" y1="27.94" x2="71.12" y2="27.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="27.94" x2="71.12" y2="25.4" width="0.1524" layer="91"/>
<junction x="71.12" y="25.4"/>
<pinref part="C17" gate="G$1" pin="2"/>
<wire x1="63.5" y1="27.94" x2="71.12" y2="27.94" width="0.1524" layer="91"/>
<junction x="71.12" y="27.94"/>
<pinref part="J17" gate="G$1" pin="1"/>
<pinref part="J17" gate="G$1" pin="3"/>
<pinref part="J17" gate="G$1" pin="5"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="7"/>
<pinref part="GND12" gate="1" pin="GND"/>
<wire x1="33.02" y1="20.32" x2="30.48" y2="20.32" width="0.1524" layer="91"/>
<wire x1="30.48" y1="20.32" x2="30.48" y2="17.78" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J13" gate="G$1" pin="7"/>
<pinref part="GND10" gate="1" pin="GND"/>
<wire x1="33.02" y1="53.34" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="30.48" y1="53.34" x2="30.48" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="7"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="33.02" y1="86.36" x2="30.48" y2="86.36" width="0.1524" layer="91"/>
<wire x1="30.48" y1="86.36" x2="30.48" y2="83.82" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="7"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="33.02" y1="119.38" x2="30.48" y2="119.38" width="0.1524" layer="91"/>
<wire x1="30.48" y1="119.38" x2="30.48" y2="116.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="7"/>
<pinref part="GND3" gate="1" pin="GND"/>
<wire x1="33.02" y1="152.4" x2="30.48" y2="152.4" width="0.1524" layer="91"/>
<wire x1="30.48" y1="152.4" x2="30.48" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="LED6" gate="G$1" pin="VSS"/>
<wire x1="152.4" y1="142.24" x2="160.02" y2="142.24" width="0.1524" layer="91"/>
<pinref part="LED9" gate="G$1" pin="VSS"/>
<wire x1="152.4" y1="88.9" x2="160.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="160.02" y1="88.9" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<pinref part="LED8" gate="G$1" pin="VSS"/>
<wire x1="160.02" y1="106.68" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="160.02" y1="124.46" x2="160.02" y2="142.24" width="0.1524" layer="91"/>
<wire x1="152.4" y1="106.68" x2="160.02" y2="106.68" width="0.1524" layer="91"/>
<junction x="160.02" y="106.68"/>
<pinref part="LED7" gate="G$1" pin="VSS"/>
<wire x1="152.4" y1="124.46" x2="160.02" y2="124.46" width="0.1524" layer="91"/>
<junction x="160.02" y="124.46"/>
<pinref part="GND7" gate="1" pin="GND"/>
<wire x1="160.02" y1="86.36" x2="160.02" y2="88.9" width="0.1524" layer="91"/>
<junction x="160.02" y="88.9"/>
<pinref part="C15" gate="G$1" pin="2"/>
<wire x1="165.1" y1="91.44" x2="165.1" y2="88.9" width="0.1524" layer="91"/>
<wire x1="165.1" y1="88.9" x2="160.02" y2="88.9" width="0.1524" layer="91"/>
<pinref part="C13" gate="G$1" pin="2"/>
<wire x1="160.02" y1="106.68" x2="165.1" y2="106.68" width="0.1524" layer="91"/>
<wire x1="165.1" y1="106.68" x2="165.1" y2="109.22" width="0.1524" layer="91"/>
<pinref part="C11" gate="G$1" pin="2"/>
<wire x1="160.02" y1="124.46" x2="165.1" y2="124.46" width="0.1524" layer="91"/>
<wire x1="165.1" y1="124.46" x2="165.1" y2="127" width="0.1524" layer="91"/>
<pinref part="C10" gate="G$1" pin="2"/>
<wire x1="160.02" y1="142.24" x2="165.1" y2="142.24" width="0.1524" layer="91"/>
<wire x1="165.1" y1="142.24" x2="165.1" y2="144.78" width="0.1524" layer="91"/>
<junction x="160.02" y="142.24"/>
</segment>
<segment>
<pinref part="P2" gate="G$1" pin="1"/>
<pinref part="IC1" gate="G$1" pin="GND"/>
<wire x1="12.7" y1="218.44" x2="30.48" y2="218.44" width="0.1524" layer="91"/>
<wire x1="30.48" y1="218.44" x2="38.1" y2="218.44" width="0.1524" layer="91"/>
<wire x1="38.1" y1="218.44" x2="45.72" y2="218.44" width="0.1524" layer="91"/>
<wire x1="45.72" y1="218.44" x2="66.04" y2="218.44" width="0.1524" layer="91"/>
<wire x1="66.04" y1="218.44" x2="66.04" y2="220.98" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="GND"/>
<wire x1="66.04" y1="215.9" x2="66.04" y2="218.44" width="0.1524" layer="91"/>
<junction x="66.04" y="218.44"/>
<pinref part="GND1" gate="1" pin="GND"/>
<wire x1="66.04" y1="218.44" x2="83.82" y2="218.44" width="0.1524" layer="91"/>
<wire x1="83.82" y1="218.44" x2="96.52" y2="218.44" width="0.1524" layer="91"/>
<wire x1="96.52" y1="218.44" x2="96.52" y2="210.82" width="0.1524" layer="91"/>
<pinref part="IC2" gate="G$1" pin="GND"/>
<wire x1="96.52" y1="210.82" x2="96.52" y2="208.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="210.82" x2="104.14" y2="210.82" width="0.1524" layer="91"/>
<wire x1="104.14" y1="210.82" x2="121.92" y2="210.82" width="0.1524" layer="91"/>
<wire x1="121.92" y1="210.82" x2="121.92" y2="213.36" width="0.1524" layer="91"/>
<junction x="96.52" y="210.82"/>
<pinref part="C2" gate="G$1" pin="2"/>
<wire x1="45.72" y1="223.52" x2="45.72" y2="218.44" width="0.1524" layer="91"/>
<junction x="45.72" y="218.44"/>
<pinref part="C3" gate="G$1" pin="2"/>
<wire x1="83.82" y1="223.52" x2="83.82" y2="218.44" width="0.1524" layer="91"/>
<junction x="83.82" y="218.44"/>
<pinref part="C7" gate="G$1" pin="1"/>
<wire x1="45.72" y1="213.36" x2="45.72" y2="218.44" width="0.1524" layer="91"/>
<pinref part="C8" gate="G$1" pin="1"/>
<wire x1="83.82" y1="218.44" x2="83.82" y2="213.36" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="2"/>
<wire x1="104.14" y1="215.9" x2="104.14" y2="210.82" width="0.1524" layer="91"/>
<junction x="104.14" y="210.82"/>
<pinref part="C5" gate="G$1" pin="2"/>
<wire x1="121.92" y1="210.82" x2="139.7" y2="210.82" width="0.1524" layer="91"/>
<wire x1="139.7" y1="210.82" x2="139.7" y2="215.9" width="0.1524" layer="91"/>
<junction x="121.92" y="210.82"/>
<junction x="30.48" y="218.44"/>
<pinref part="D1" gate="G$1" pin="A1"/>
<wire x1="30.48" y1="218.44" x2="30.48" y2="220.98" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="+"/>
<wire x1="38.1" y1="213.36" x2="38.1" y2="218.44" width="0.1524" layer="91"/>
<junction x="38.1" y="218.44"/>
<pinref part="C1" gate="G$1" pin="-"/>
<wire x1="38.1" y1="218.44" x2="38.1" y2="223.52" width="0.1524" layer="91"/>
<pinref part="D2" gate="G$1" pin="A1"/>
<wire x1="30.48" y1="218.44" x2="30.48" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R2" gate="&gt;NAME" pin="1"/>
<wire x1="149.86" y1="195.58" x2="149.86" y2="193.04" width="0.1524" layer="91"/>
<pinref part="R4" gate="&gt;NAME" pin="1"/>
<wire x1="149.86" y1="193.04" x2="160.02" y2="193.04" width="0.1524" layer="91"/>
<wire x1="160.02" y1="193.04" x2="170.18" y2="193.04" width="0.1524" layer="91"/>
<wire x1="170.18" y1="193.04" x2="170.18" y2="195.58" width="0.1524" layer="91"/>
<pinref part="R3" gate="&gt;NAME" pin="1"/>
<wire x1="160.02" y1="195.58" x2="160.02" y2="193.04" width="0.1524" layer="91"/>
<junction x="160.02" y="193.04"/>
<pinref part="GND2" gate="1" pin="GND"/>
<pinref part="LED5" gate="G$1" pin="A"/>
<wire x1="139.7" y1="193.04" x2="142.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="142.24" y1="193.04" x2="142.24" y2="203.2" width="0.1524" layer="91"/>
<pinref part="LED4" gate="G$1" pin="A"/>
<wire x1="142.24" y1="203.2" x2="139.7" y2="203.2" width="0.1524" layer="91"/>
<wire x1="149.86" y1="193.04" x2="142.24" y2="193.04" width="0.1524" layer="91"/>
<junction x="149.86" y="193.04"/>
<junction x="142.24" y="193.04"/>
<wire x1="170.18" y1="190.5" x2="170.18" y2="193.04" width="0.1524" layer="91"/>
<junction x="170.18" y="193.04"/>
</segment>
<segment>
<pinref part="GND13" gate="1" pin="GND"/>
<wire x1="91.44" y1="17.78" x2="91.44" y2="22.86" width="0.1524" layer="91"/>
<pinref part="J17" gate="G$1" pin="6"/>
<wire x1="91.44" y1="22.86" x2="88.9" y2="22.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="1" pin="GND"/>
<wire x1="91.44" y1="50.8" x2="91.44" y2="55.88" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="6"/>
<wire x1="91.44" y1="55.88" x2="88.9" y2="55.88" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="1" pin="GND"/>
<wire x1="91.44" y1="83.82" x2="91.44" y2="88.9" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="6"/>
<wire x1="91.44" y1="88.9" x2="88.9" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GND"/>
<wire x1="91.44" y1="116.84" x2="91.44" y2="121.92" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="6"/>
<wire x1="91.44" y1="121.92" x2="88.9" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND4" gate="1" pin="GND"/>
<wire x1="91.44" y1="149.86" x2="91.44" y2="154.94" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="6"/>
<wire x1="91.44" y1="154.94" x2="88.9" y2="154.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+12V" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="1"/>
<pinref part="P+15" gate="1" pin="+12V"/>
<wire x1="33.02" y1="27.94" x2="30.48" y2="27.94" width="0.1524" layer="91"/>
<wire x1="30.48" y1="27.94" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J13" gate="G$1" pin="1"/>
<pinref part="P+13" gate="1" pin="+12V"/>
<wire x1="33.02" y1="60.96" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<wire x1="30.48" y1="60.96" x2="30.48" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="1"/>
<pinref part="P+11" gate="1" pin="+12V"/>
<wire x1="33.02" y1="93.98" x2="30.48" y2="93.98" width="0.1524" layer="91"/>
<wire x1="30.48" y1="93.98" x2="30.48" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="1"/>
<pinref part="P+9" gate="1" pin="+12V"/>
<wire x1="33.02" y1="127" x2="30.48" y2="127" width="0.1524" layer="91"/>
<wire x1="30.48" y1="127" x2="30.48" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="1"/>
<pinref part="P+7" gate="1" pin="+12V"/>
<wire x1="33.02" y1="160.02" x2="30.48" y2="160.02" width="0.1524" layer="91"/>
<wire x1="30.48" y1="160.02" x2="30.48" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="P+3" gate="1" pin="+12V"/>
<wire x1="45.72" y1="236.22" x2="45.72" y2="241.3" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VIN"/>
<junction x="45.72" y="236.22"/>
<pinref part="C2" gate="G$1" pin="1"/>
<wire x1="45.72" y1="236.22" x2="50.8" y2="236.22" width="0.1524" layer="91"/>
<wire x1="45.72" y1="236.22" x2="45.72" y2="231.14" width="0.1524" layer="91"/>
<junction x="45.72" y="236.22"/>
<pinref part="F1" gate="G$1" pin="2"/>
<wire x1="27.94" y1="236.22" x2="30.48" y2="236.22" width="0.1524" layer="91"/>
<pinref part="D1" gate="G$1" pin="C1"/>
<wire x1="30.48" y1="236.22" x2="38.1" y2="236.22" width="0.1524" layer="91"/>
<wire x1="38.1" y1="236.22" x2="45.72" y2="236.22" width="0.1524" layer="91"/>
<wire x1="30.48" y1="231.14" x2="30.48" y2="236.22" width="0.1524" layer="91"/>
<junction x="30.48" y="236.22"/>
<pinref part="C1" gate="G$1" pin="+"/>
<wire x1="38.1" y1="231.14" x2="38.1" y2="236.22" width="0.1524" layer="91"/>
<junction x="38.1" y="236.22"/>
</segment>
<segment>
<pinref part="LED1" gate="G$1" pin="A"/>
<pinref part="P+4" gate="1" pin="+12V"/>
<wire x1="149.86" y1="215.9" x2="149.86" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="3"/>
<pinref part="P+14" gate="1" pin="+5V"/>
<wire x1="33.02" y1="25.4" x2="22.86" y2="25.4" width="0.1524" layer="91"/>
<wire x1="22.86" y1="25.4" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J13" gate="G$1" pin="3"/>
<pinref part="P+12" gate="1" pin="+5V"/>
<wire x1="33.02" y1="58.42" x2="22.86" y2="58.42" width="0.1524" layer="91"/>
<wire x1="22.86" y1="58.42" x2="22.86" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="3"/>
<pinref part="P+10" gate="1" pin="+5V"/>
<wire x1="33.02" y1="91.44" x2="22.86" y2="91.44" width="0.1524" layer="91"/>
<wire x1="22.86" y1="91.44" x2="22.86" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="3"/>
<pinref part="P+8" gate="1" pin="+5V"/>
<wire x1="33.02" y1="124.46" x2="22.86" y2="124.46" width="0.1524" layer="91"/>
<wire x1="22.86" y1="124.46" x2="22.86" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="3"/>
<pinref part="P+6" gate="1" pin="+5V"/>
<wire x1="33.02" y1="157.48" x2="22.86" y2="157.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="157.48" x2="22.86" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J1" gate="G$1" pin="2"/>
<pinref part="P+1" gate="1" pin="+5V"/>
<wire x1="96.52" y1="246.38" x2="104.14" y2="246.38" width="0.1524" layer="91"/>
<wire x1="104.14" y1="246.38" x2="104.14" y2="251.46" width="0.1524" layer="91"/>
<pinref part="C4" gate="G$1" pin="1"/>
<pinref part="IC2" gate="G$1" pin="VIN"/>
<wire x1="106.68" y1="228.6" x2="104.14" y2="228.6" width="0.1524" layer="91"/>
<wire x1="104.14" y1="228.6" x2="104.14" y2="223.52" width="0.1524" layer="91"/>
<junction x="104.14" y="228.6"/>
<wire x1="104.14" y1="246.38" x2="104.14" y2="228.6" width="0.1524" layer="91"/>
<junction x="104.14" y="246.38"/>
</segment>
<segment>
<pinref part="LED2" gate="G$1" pin="A"/>
<pinref part="P+5" gate="1" pin="+5V"/>
<wire x1="160.02" y1="215.9" x2="160.02" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="J16" gate="G$1" pin="5"/>
<pinref part="+3V37" gate="G$1" pin="+3V3"/>
<wire x1="33.02" y1="22.86" x2="15.24" y2="22.86" width="0.1524" layer="91"/>
<wire x1="15.24" y1="22.86" x2="15.24" y2="30.48" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J13" gate="G$1" pin="5"/>
<pinref part="+3V36" gate="G$1" pin="+3V3"/>
<wire x1="33.02" y1="55.88" x2="15.24" y2="55.88" width="0.1524" layer="91"/>
<wire x1="15.24" y1="55.88" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J10" gate="G$1" pin="5"/>
<pinref part="+3V35" gate="G$1" pin="+3V3"/>
<wire x1="33.02" y1="88.9" x2="15.24" y2="88.9" width="0.1524" layer="91"/>
<wire x1="15.24" y1="88.9" x2="15.24" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J7" gate="G$1" pin="5"/>
<pinref part="+3V34" gate="G$1" pin="+3V3"/>
<wire x1="33.02" y1="121.92" x2="15.24" y2="121.92" width="0.1524" layer="91"/>
<wire x1="15.24" y1="121.92" x2="15.24" y2="129.54" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J4" gate="G$1" pin="5"/>
<pinref part="+3V33" gate="G$1" pin="+3V3"/>
<wire x1="33.02" y1="154.94" x2="15.24" y2="154.94" width="0.1524" layer="91"/>
<wire x1="15.24" y1="154.94" x2="15.24" y2="162.56" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="IC2" gate="G$1" pin="VOUT"/>
<pinref part="+3V31" gate="G$1" pin="+3V3"/>
<wire x1="137.16" y1="228.6" x2="139.7" y2="228.6" width="0.1524" layer="91"/>
<wire x1="139.7" y1="228.6" x2="144.78" y2="228.6" width="0.1524" layer="91"/>
<wire x1="144.78" y1="228.6" x2="144.78" y2="231.14" width="0.1524" layer="91"/>
<pinref part="C5" gate="G$1" pin="1"/>
<wire x1="139.7" y1="223.52" x2="139.7" y2="228.6" width="0.1524" layer="91"/>
<junction x="139.7" y="228.6"/>
</segment>
<segment>
<pinref part="LED3" gate="G$1" pin="A"/>
<pinref part="+3V32" gate="G$1" pin="+3V3"/>
<wire x1="170.18" y1="215.9" x2="170.18" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="-12V" class="0">
<segment>
<pinref part="SUP15" gate="G$1" pin="-12V"/>
<wire x1="88.9" y1="27.94" x2="106.68" y2="27.94" width="0.1524" layer="91"/>
<wire x1="106.68" y1="27.94" x2="106.68" y2="17.78" width="0.1524" layer="91"/>
<pinref part="J17" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUP13" gate="G$1" pin="-12V"/>
<wire x1="88.9" y1="60.96" x2="106.68" y2="60.96" width="0.1524" layer="91"/>
<wire x1="106.68" y1="60.96" x2="106.68" y2="50.8" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUP11" gate="G$1" pin="-12V"/>
<wire x1="88.9" y1="93.98" x2="106.68" y2="93.98" width="0.1524" layer="91"/>
<wire x1="106.68" y1="93.98" x2="106.68" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUP9" gate="G$1" pin="-12V"/>
<wire x1="88.9" y1="127" x2="106.68" y2="127" width="0.1524" layer="91"/>
<wire x1="106.68" y1="127" x2="106.68" y2="116.84" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUP7" gate="G$1" pin="-12V"/>
<wire x1="88.9" y1="160.02" x2="106.68" y2="160.02" width="0.1524" layer="91"/>
<wire x1="106.68" y1="160.02" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="SUP2" gate="G$1" pin="-12V"/>
<wire x1="45.72" y1="200.66" x2="45.72" y2="195.58" width="0.1524" layer="91"/>
<pinref part="IC3" gate="G$1" pin="VIN"/>
<wire x1="50.8" y1="200.66" x2="45.72" y2="200.66" width="0.1524" layer="91"/>
<junction x="45.72" y="200.66"/>
<pinref part="C7" gate="G$1" pin="2"/>
<wire x1="45.72" y1="200.66" x2="45.72" y2="205.74" width="0.1524" layer="91"/>
<junction x="45.72" y="200.66"/>
<pinref part="F2" gate="G$1" pin="2"/>
<wire x1="27.94" y1="200.66" x2="30.48" y2="200.66" width="0.1524" layer="91"/>
<wire x1="30.48" y1="200.66" x2="38.1" y2="200.66" width="0.1524" layer="91"/>
<wire x1="38.1" y1="200.66" x2="45.72" y2="200.66" width="0.1524" layer="91"/>
<pinref part="C6" gate="G$1" pin="-"/>
<wire x1="38.1" y1="200.66" x2="38.1" y2="205.74" width="0.1524" layer="91"/>
<junction x="38.1" y="200.66"/>
<pinref part="D2" gate="G$1" pin="C1"/>
<wire x1="30.48" y1="205.74" x2="30.48" y2="200.66" width="0.1524" layer="91"/>
<junction x="30.48" y="200.66"/>
</segment>
<segment>
<pinref part="R5" gate="&gt;NAME" pin="1"/>
<pinref part="SUP4" gate="G$1" pin="-12V"/>
<wire x1="119.38" y1="193.04" x2="116.84" y2="193.04" width="0.1524" layer="91"/>
<wire x1="116.84" y1="193.04" x2="116.84" y2="190.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="-5V" class="0">
<segment>
<pinref part="SUP14" gate="G$1" pin="-5V"/>
<wire x1="88.9" y1="25.4" x2="99.06" y2="25.4" width="0.1524" layer="91"/>
<wire x1="99.06" y1="25.4" x2="99.06" y2="17.78" width="0.1524" layer="91"/>
<pinref part="J17" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="SUP12" gate="G$1" pin="-5V"/>
<wire x1="88.9" y1="58.42" x2="99.06" y2="58.42" width="0.1524" layer="91"/>
<wire x1="99.06" y1="58.42" x2="99.06" y2="50.8" width="0.1524" layer="91"/>
<pinref part="J14" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="SUP10" gate="G$1" pin="-5V"/>
<wire x1="88.9" y1="91.44" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="99.06" y1="91.44" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<pinref part="J11" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="SUP8" gate="G$1" pin="-5V"/>
<wire x1="88.9" y1="124.46" x2="99.06" y2="124.46" width="0.1524" layer="91"/>
<wire x1="99.06" y1="124.46" x2="99.06" y2="116.84" width="0.1524" layer="91"/>
<pinref part="J8" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="SUP6" gate="G$1" pin="-5V"/>
<wire x1="88.9" y1="157.48" x2="99.06" y2="157.48" width="0.1524" layer="91"/>
<wire x1="99.06" y1="157.48" x2="99.06" y2="149.86" width="0.1524" layer="91"/>
<pinref part="J5" gate="G$1" pin="4"/>
</segment>
<segment>
<pinref part="IC3" gate="G$1" pin="VOUT"/>
<pinref part="C8" gate="G$1" pin="2"/>
<wire x1="81.28" y1="200.66" x2="83.82" y2="200.66" width="0.1524" layer="91"/>
<wire x1="83.82" y1="200.66" x2="83.82" y2="205.74" width="0.1524" layer="91"/>
<pinref part="SUP3" gate="G$1" pin="-5V"/>
<wire x1="83.82" y1="200.66" x2="88.9" y2="200.66" width="0.1524" layer="91"/>
<wire x1="88.9" y1="200.66" x2="88.9" y2="195.58" width="0.1524" layer="91"/>
<junction x="83.82" y="200.66"/>
</segment>
<segment>
<pinref part="R1" gate="&gt;NAME" pin="1"/>
<pinref part="SUP1" gate="G$1" pin="-5V"/>
<wire x1="119.38" y1="203.2" x2="111.76" y2="203.2" width="0.1524" layer="91"/>
<wire x1="111.76" y1="203.2" x2="111.76" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<pinref part="J13" gate="G$1" pin="8"/>
<pinref part="J15" gate="G$1" pin="1"/>
<wire x1="48.26" y1="53.34" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<pinref part="J15" gate="G$1" pin="3"/>
<wire x1="50.8" y1="53.34" x2="53.34" y2="53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="48.26" x2="50.8" y2="48.26" width="0.1524" layer="91"/>
<wire x1="50.8" y1="48.26" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<junction x="50.8" y="53.34"/>
<pinref part="J13" gate="G$1" pin="6"/>
<wire x1="48.26" y1="55.88" x2="50.8" y2="55.88" width="0.1524" layer="91"/>
<wire x1="50.8" y1="55.88" x2="50.8" y2="53.34" width="0.1524" layer="91"/>
<pinref part="J13" gate="G$1" pin="4"/>
<wire x1="48.26" y1="58.42" x2="50.8" y2="58.42" width="0.1524" layer="91"/>
<wire x1="50.8" y1="58.42" x2="50.8" y2="55.88" width="0.1524" layer="91"/>
<junction x="50.8" y="55.88"/>
<pinref part="J13" gate="G$1" pin="2"/>
<wire x1="48.26" y1="60.96" x2="50.8" y2="60.96" width="0.1524" layer="91"/>
<wire x1="50.8" y1="60.96" x2="50.8" y2="58.42" width="0.1524" layer="91"/>
<junction x="50.8" y="58.42"/>
<pinref part="C16" gate="G$1" pin="1"/>
<wire x1="50.8" y1="60.96" x2="55.88" y2="60.96" width="0.1524" layer="91"/>
<junction x="50.8" y="60.96"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="J15" gate="G$1" pin="2"/>
<wire x1="68.58" y1="53.34" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<pinref part="J15" gate="G$1" pin="4"/>
<wire x1="68.58" y1="48.26" x2="71.12" y2="48.26" width="0.1524" layer="91"/>
<wire x1="71.12" y1="48.26" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<junction x="71.12" y="53.34"/>
<wire x1="73.66" y1="55.88" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<wire x1="71.12" y1="55.88" x2="71.12" y2="53.34" width="0.1524" layer="91"/>
<wire x1="73.66" y1="58.42" x2="71.12" y2="58.42" width="0.1524" layer="91"/>
<wire x1="71.12" y1="58.42" x2="71.12" y2="55.88" width="0.1524" layer="91"/>
<junction x="71.12" y="55.88"/>
<wire x1="73.66" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<wire x1="71.12" y1="60.96" x2="71.12" y2="58.42" width="0.1524" layer="91"/>
<junction x="71.12" y="58.42"/>
<pinref part="C16" gate="G$1" pin="2"/>
<wire x1="63.5" y1="60.96" x2="71.12" y2="60.96" width="0.1524" layer="91"/>
<junction x="71.12" y="60.96"/>
<pinref part="J14" gate="G$1" pin="1"/>
<pinref part="J14" gate="G$1" pin="3"/>
<pinref part="J14" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="J10" gate="G$1" pin="8"/>
<pinref part="J12" gate="G$1" pin="1"/>
<wire x1="48.26" y1="86.36" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J12" gate="G$1" pin="3"/>
<wire x1="50.8" y1="86.36" x2="53.34" y2="86.36" width="0.1524" layer="91"/>
<wire x1="53.34" y1="81.28" x2="50.8" y2="81.28" width="0.1524" layer="91"/>
<wire x1="50.8" y1="81.28" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<junction x="50.8" y="86.36"/>
<pinref part="J10" gate="G$1" pin="6"/>
<wire x1="48.26" y1="88.9" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<wire x1="50.8" y1="88.9" x2="50.8" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J10" gate="G$1" pin="4"/>
<wire x1="48.26" y1="91.44" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<wire x1="50.8" y1="91.44" x2="50.8" y2="88.9" width="0.1524" layer="91"/>
<junction x="50.8" y="88.9"/>
<pinref part="J10" gate="G$1" pin="2"/>
<wire x1="48.26" y1="93.98" x2="50.8" y2="93.98" width="0.1524" layer="91"/>
<wire x1="50.8" y1="93.98" x2="50.8" y2="91.44" width="0.1524" layer="91"/>
<junction x="50.8" y="91.44"/>
<pinref part="C14" gate="G$1" pin="1"/>
<wire x1="50.8" y1="93.98" x2="55.88" y2="93.98" width="0.1524" layer="91"/>
<junction x="50.8" y="93.98"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="J12" gate="G$1" pin="2"/>
<wire x1="68.58" y1="86.36" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<pinref part="J12" gate="G$1" pin="4"/>
<wire x1="68.58" y1="81.28" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="71.12" y1="81.28" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<junction x="71.12" y="86.36"/>
<wire x1="73.66" y1="88.9" x2="71.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="71.12" y1="88.9" x2="71.12" y2="86.36" width="0.1524" layer="91"/>
<wire x1="73.66" y1="91.44" x2="71.12" y2="91.44" width="0.1524" layer="91"/>
<wire x1="71.12" y1="91.44" x2="71.12" y2="88.9" width="0.1524" layer="91"/>
<junction x="71.12" y="88.9"/>
<wire x1="73.66" y1="93.98" x2="71.12" y2="93.98" width="0.1524" layer="91"/>
<wire x1="71.12" y1="93.98" x2="71.12" y2="91.44" width="0.1524" layer="91"/>
<junction x="71.12" y="91.44"/>
<pinref part="C14" gate="G$1" pin="2"/>
<wire x1="63.5" y1="93.98" x2="71.12" y2="93.98" width="0.1524" layer="91"/>
<junction x="71.12" y="93.98"/>
<pinref part="J11" gate="G$1" pin="1"/>
<pinref part="J11" gate="G$1" pin="3"/>
<pinref part="J11" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="J7" gate="G$1" pin="8"/>
<pinref part="J9" gate="G$1" pin="1"/>
<wire x1="48.26" y1="119.38" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="3"/>
<wire x1="50.8" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="114.3" x2="50.8" y2="114.3" width="0.1524" layer="91"/>
<wire x1="50.8" y1="114.3" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
<junction x="50.8" y="119.38"/>
<pinref part="J7" gate="G$1" pin="6"/>
<wire x1="48.26" y1="121.92" x2="50.8" y2="121.92" width="0.1524" layer="91"/>
<wire x1="50.8" y1="121.92" x2="50.8" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J7" gate="G$1" pin="4"/>
<wire x1="48.26" y1="124.46" x2="50.8" y2="124.46" width="0.1524" layer="91"/>
<wire x1="50.8" y1="124.46" x2="50.8" y2="121.92" width="0.1524" layer="91"/>
<junction x="50.8" y="121.92"/>
<pinref part="J7" gate="G$1" pin="2"/>
<wire x1="48.26" y1="127" x2="50.8" y2="127" width="0.1524" layer="91"/>
<wire x1="50.8" y1="127" x2="50.8" y2="124.46" width="0.1524" layer="91"/>
<junction x="50.8" y="124.46"/>
<pinref part="C12" gate="G$1" pin="1"/>
<wire x1="50.8" y1="127" x2="55.88" y2="127" width="0.1524" layer="91"/>
<junction x="50.8" y="127"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="J9" gate="G$1" pin="2"/>
<wire x1="68.58" y1="119.38" x2="71.12" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J9" gate="G$1" pin="4"/>
<wire x1="68.58" y1="114.3" x2="71.12" y2="114.3" width="0.1524" layer="91"/>
<wire x1="71.12" y1="114.3" x2="71.12" y2="119.38" width="0.1524" layer="91"/>
<junction x="71.12" y="119.38"/>
<wire x1="73.66" y1="121.92" x2="71.12" y2="121.92" width="0.1524" layer="91"/>
<wire x1="71.12" y1="121.92" x2="71.12" y2="119.38" width="0.1524" layer="91"/>
<wire x1="73.66" y1="124.46" x2="71.12" y2="124.46" width="0.1524" layer="91"/>
<wire x1="71.12" y1="124.46" x2="71.12" y2="121.92" width="0.1524" layer="91"/>
<junction x="71.12" y="121.92"/>
<wire x1="73.66" y1="127" x2="71.12" y2="127" width="0.1524" layer="91"/>
<wire x1="71.12" y1="127" x2="71.12" y2="124.46" width="0.1524" layer="91"/>
<junction x="71.12" y="124.46"/>
<pinref part="C12" gate="G$1" pin="2"/>
<wire x1="63.5" y1="127" x2="71.12" y2="127" width="0.1524" layer="91"/>
<junction x="71.12" y="127"/>
<pinref part="J8" gate="G$1" pin="1"/>
<pinref part="J8" gate="G$1" pin="3"/>
<pinref part="J8" gate="G$1" pin="5"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<pinref part="J4" gate="G$1" pin="8"/>
<pinref part="J6" gate="G$1" pin="1"/>
<wire x1="48.26" y1="152.4" x2="50.8" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="3"/>
<wire x1="50.8" y1="152.4" x2="53.34" y2="152.4" width="0.1524" layer="91"/>
<wire x1="53.34" y1="147.32" x2="50.8" y2="147.32" width="0.1524" layer="91"/>
<wire x1="50.8" y1="147.32" x2="50.8" y2="152.4" width="0.1524" layer="91"/>
<junction x="50.8" y="152.4"/>
<pinref part="J4" gate="G$1" pin="6"/>
<wire x1="48.26" y1="154.94" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<wire x1="50.8" y1="154.94" x2="50.8" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J4" gate="G$1" pin="4"/>
<wire x1="48.26" y1="157.48" x2="50.8" y2="157.48" width="0.1524" layer="91"/>
<wire x1="50.8" y1="157.48" x2="50.8" y2="154.94" width="0.1524" layer="91"/>
<junction x="50.8" y="154.94"/>
<pinref part="J4" gate="G$1" pin="2"/>
<wire x1="48.26" y1="160.02" x2="50.8" y2="160.02" width="0.1524" layer="91"/>
<wire x1="50.8" y1="160.02" x2="50.8" y2="157.48" width="0.1524" layer="91"/>
<junction x="50.8" y="157.48"/>
<pinref part="C9" gate="G$1" pin="1"/>
<wire x1="50.8" y1="160.02" x2="55.88" y2="160.02" width="0.1524" layer="91"/>
<junction x="50.8" y="160.02"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<pinref part="J6" gate="G$1" pin="2"/>
<wire x1="68.58" y1="152.4" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J6" gate="G$1" pin="4"/>
<wire x1="68.58" y1="147.32" x2="71.12" y2="147.32" width="0.1524" layer="91"/>
<wire x1="71.12" y1="147.32" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<junction x="71.12" y="152.4"/>
<wire x1="73.66" y1="154.94" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<wire x1="71.12" y1="154.94" x2="71.12" y2="152.4" width="0.1524" layer="91"/>
<wire x1="73.66" y1="157.48" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<wire x1="71.12" y1="157.48" x2="71.12" y2="154.94" width="0.1524" layer="91"/>
<junction x="71.12" y="154.94"/>
<wire x1="73.66" y1="160.02" x2="71.12" y2="160.02" width="0.1524" layer="91"/>
<wire x1="71.12" y1="160.02" x2="71.12" y2="157.48" width="0.1524" layer="91"/>
<junction x="71.12" y="157.48"/>
<pinref part="C9" gate="G$1" pin="2"/>
<wire x1="63.5" y1="160.02" x2="71.12" y2="160.02" width="0.1524" layer="91"/>
<junction x="71.12" y="160.02"/>
<pinref part="J5" gate="G$1" pin="1"/>
<pinref part="J5" gate="G$1" pin="3"/>
<pinref part="J5" gate="G$1" pin="5"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="J3" gate="G$1" pin="1"/>
<pinref part="SUP5" gate="G$1" pin="VCC"/>
<wire x1="167.64" y1="165.1" x2="157.48" y2="165.1" width="0.1524" layer="91"/>
<wire x1="157.48" y1="165.1" x2="157.48" y2="170.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$53" class="0">
<segment>
<pinref part="LED6" gate="G$1" pin="VDD"/>
<wire x1="165.1" y1="154.94" x2="157.48" y2="154.94" width="0.1524" layer="91"/>
<wire x1="157.48" y1="154.94" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<wire x1="152.4" y1="147.32" x2="154.94" y2="147.32" width="0.1524" layer="91"/>
<wire x1="154.94" y1="147.32" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="LED9" gate="G$1" pin="VDD"/>
<wire x1="152.4" y1="93.98" x2="157.48" y2="93.98" width="0.1524" layer="91"/>
<wire x1="157.48" y1="154.94" x2="157.48" y2="137.16" width="0.1524" layer="91"/>
<junction x="157.48" y="154.94"/>
<pinref part="LED8" gate="G$1" pin="VDD"/>
<wire x1="157.48" y1="137.16" x2="157.48" y2="129.54" width="0.1524" layer="91"/>
<wire x1="157.48" y1="129.54" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<wire x1="157.48" y1="119.38" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<wire x1="157.48" y1="111.76" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<wire x1="157.48" y1="101.6" x2="157.48" y2="93.98" width="0.1524" layer="91"/>
<wire x1="152.4" y1="111.76" x2="157.48" y2="111.76" width="0.1524" layer="91"/>
<junction x="157.48" y="111.76"/>
<pinref part="LED7" gate="G$1" pin="VDD"/>
<wire x1="152.4" y1="129.54" x2="157.48" y2="129.54" width="0.1524" layer="91"/>
<junction x="157.48" y="129.54"/>
<pinref part="C11" gate="G$1" pin="1"/>
<wire x1="165.1" y1="134.62" x2="165.1" y2="137.16" width="0.1524" layer="91"/>
<wire x1="165.1" y1="137.16" x2="157.48" y2="137.16" width="0.1524" layer="91"/>
<junction x="157.48" y="137.16"/>
<pinref part="C13" gate="G$1" pin="1"/>
<wire x1="165.1" y1="116.84" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<wire x1="165.1" y1="119.38" x2="157.48" y2="119.38" width="0.1524" layer="91"/>
<junction x="157.48" y="119.38"/>
<pinref part="C15" gate="G$1" pin="1"/>
<wire x1="165.1" y1="99.06" x2="165.1" y2="101.6" width="0.1524" layer="91"/>
<wire x1="165.1" y1="101.6" x2="157.48" y2="101.6" width="0.1524" layer="91"/>
<junction x="157.48" y="101.6"/>
<pinref part="C10" gate="G$1" pin="1"/>
<wire x1="165.1" y1="152.4" x2="165.1" y2="154.94" width="0.1524" layer="91"/>
<pinref part="J3" gate="G$1" pin="2"/>
<wire x1="167.64" y1="160.02" x2="165.1" y2="160.02" width="0.1524" layer="91"/>
<wire x1="165.1" y1="160.02" x2="165.1" y2="154.94" width="0.1524" layer="91"/>
<junction x="165.1" y="154.94"/>
</segment>
</net>
<net name="N$56" class="0">
<segment>
<pinref part="LED6" gate="G$1" pin="DOUT"/>
<wire x1="121.92" y1="142.24" x2="119.38" y2="142.24" width="0.1524" layer="91"/>
<wire x1="119.38" y1="142.24" x2="119.38" y2="129.54" width="0.1524" layer="91"/>
<pinref part="LED7" gate="G$1" pin="DIN"/>
<wire x1="119.38" y1="129.54" x2="121.92" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="LED8" gate="G$1" pin="DOUT"/>
<wire x1="121.92" y1="106.68" x2="119.38" y2="106.68" width="0.1524" layer="91"/>
<wire x1="119.38" y1="106.68" x2="119.38" y2="93.98" width="0.1524" layer="91"/>
<pinref part="LED9" gate="G$1" pin="DIN"/>
<wire x1="119.38" y1="93.98" x2="121.92" y2="93.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$54" class="0">
<segment>
<pinref part="LED6" gate="G$1" pin="DIN"/>
<pinref part="J2" gate="G$1" pin="1"/>
<wire x1="121.92" y1="147.32" x2="119.38" y2="147.32" width="0.1524" layer="91"/>
<wire x1="119.38" y1="147.32" x2="119.38" y2="160.02" width="0.1524" layer="91"/>
<wire x1="119.38" y1="160.02" x2="127" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$57" class="0">
<segment>
<pinref part="LED8" gate="G$1" pin="DIN"/>
<wire x1="121.92" y1="111.76" x2="116.84" y2="111.76" width="0.1524" layer="91"/>
<wire x1="116.84" y1="111.76" x2="116.84" y2="165.1" width="0.1524" layer="91"/>
<pinref part="J2" gate="G$1" pin="2"/>
<wire x1="116.84" y1="165.1" x2="127" y2="165.1" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V_USB" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="3"/>
<pinref part="P+2" gate="G$1" pin="+5V_USB"/>
<wire x1="96.52" y1="243.84" x2="114.3" y2="243.84" width="0.1524" layer="91"/>
<wire x1="114.3" y1="243.84" x2="114.3" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$86" class="0">
<segment>
<pinref part="LED1" gate="G$1" pin="C"/>
<pinref part="R2" gate="&gt;NAME" pin="2"/>
<wire x1="149.86" y1="208.28" x2="149.86" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$87" class="0">
<segment>
<pinref part="LED2" gate="G$1" pin="C"/>
<pinref part="R3" gate="&gt;NAME" pin="2"/>
<wire x1="160.02" y1="208.28" x2="160.02" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$88" class="0">
<segment>
<pinref part="LED3" gate="G$1" pin="C"/>
<pinref part="R4" gate="&gt;NAME" pin="2"/>
<wire x1="170.18" y1="208.28" x2="170.18" y2="205.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<pinref part="LED4" gate="G$1" pin="C"/>
<pinref part="R1" gate="&gt;NAME" pin="2"/>
<wire x1="132.08" y1="203.2" x2="129.54" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<pinref part="LED5" gate="G$1" pin="C"/>
<pinref part="R5" gate="&gt;NAME" pin="2"/>
<wire x1="132.08" y1="193.04" x2="129.54" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$85" class="0">
<segment>
<pinref part="J1" gate="G$1" pin="1"/>
<wire x1="96.52" y1="248.92" x2="99.06" y2="248.92" width="0.1524" layer="91"/>
<wire x1="99.06" y1="248.92" x2="99.06" y2="256.54" width="0.1524" layer="91"/>
<wire x1="99.06" y1="256.54" x2="83.82" y2="256.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="256.54" x2="83.82" y2="236.22" width="0.1524" layer="91"/>
<pinref part="IC1" gate="G$1" pin="VOUT"/>
<wire x1="81.28" y1="236.22" x2="83.82" y2="236.22" width="0.1524" layer="91"/>
<pinref part="C3" gate="G$1" pin="1"/>
<wire x1="83.82" y1="236.22" x2="83.82" y2="231.14" width="0.1524" layer="91"/>
<junction x="83.82" y="236.22"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="P1" gate="G$1" pin="1"/>
<pinref part="F1" gate="G$1" pin="1"/>
<wire x1="12.7" y1="236.22" x2="17.78" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<pinref part="P3" gate="G$1" pin="1"/>
<pinref part="F2" gate="G$1" pin="1"/>
<wire x1="12.7" y1="200.66" x2="17.78" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="5.08" y1="88.9" x2="137.16" y2="88.9" width="0.4064" layer="97"/>
<wire x1="137.16" y1="88.9" x2="137.16" y2="10.16" width="0.4064" layer="97"/>
<wire x1="137.16" y1="10.16" x2="5.08" y2="10.16" width="0.4064" layer="97"/>
<wire x1="5.08" y1="10.16" x2="5.08" y2="88.9" width="0.4064" layer="97"/>
<text x="7.62" y="86.36" size="3.81" layer="97" font="vector" rot="MR180">USB to Serial</text>
<wire x1="5.08" y1="170.18" x2="177.8" y2="170.18" width="0.4064" layer="97"/>
<wire x1="5.08" y1="96.52" x2="5.08" y2="170.18" width="0.4064" layer="97"/>
<wire x1="177.8" y1="170.18" x2="177.8" y2="96.52" width="0.4064" layer="97"/>
<wire x1="177.8" y1="96.52" x2="5.08" y2="96.52" width="0.4064" layer="97"/>
<wire x1="5.08" y1="254" x2="177.8" y2="254" width="0.4064" layer="97"/>
<wire x1="5.08" y1="177.8" x2="177.8" y2="177.8" width="0.4064" layer="97"/>
<wire x1="5.08" y1="177.8" x2="5.08" y2="254" width="0.4064" layer="97"/>
<wire x1="177.8" y1="177.8" x2="177.8" y2="254" width="0.4064" layer="97"/>
<text x="7.62" y="167.64" size="3.81" layer="97" font="vector" rot="MR180">Buttons</text>
<text x="7.62" y="251.46" size="3.81" layer="97" font="vector" rot="MR180">LEDs</text>
<text x="167.64" y="5.08" size="2.1844" layer="94" font="vector">v1.0c</text>
<polygon width="0.00038125" layer="94">
<vertex x="172.184784375" y="38.340025" curve="9.499998"/>
<vertex x="172.956115625" y="38.7372875"/>
<vertex x="174.871340625" y="37.175359375"/>
<vertex x="176.184640625" y="38.488659375"/>
<vertex x="174.6227125" y="40.403884375" curve="19.000007"/>
<vertex x="175.28448125" y="42.0015375"/>
<vertex x="177.743203125" y="42.25135625"/>
<vertex x="177.743203125" y="44.10864375"/>
<vertex x="175.28448125" y="44.3584625" curve="19.000007"/>
<vertex x="174.6227125" y="45.956115625"/>
<vertex x="176.184640625" y="47.871340625"/>
<vertex x="174.871340625" y="49.184640625"/>
<vertex x="172.956115625" y="47.6227125" curve="19.000007"/>
<vertex x="171.3584625" y="48.28448125"/>
<vertex x="171.10864375" y="50.743203125"/>
<vertex x="169.25135625" y="50.743203125"/>
<vertex x="169.0015375" y="48.28448125" curve="19.000007"/>
<vertex x="167.403884375" y="47.6227125"/>
<vertex x="165.488659375" y="49.184640625"/>
<vertex x="164.175359375" y="47.871340625"/>
<vertex x="165.7372875" y="45.956115625" curve="19.000007"/>
<vertex x="165.07551875" y="44.3584625"/>
<vertex x="162.616796875" y="44.10864375"/>
<vertex x="162.616796875" y="42.25135625"/>
<vertex x="165.07551875" y="42.0015375" curve="19.000007"/>
<vertex x="165.7372875" y="40.403884375"/>
<vertex x="164.175359375" y="38.488659375"/>
<vertex x="165.488659375" y="37.175359375"/>
<vertex x="167.403884375" y="38.7372875" curve="9.499998"/>
<vertex x="168.175215625" y="38.340025"/>
<vertex x="169.268734375" y="40.9800125" curve="-67.499973"/>
<vertex x="167.79875" y="43.18" curve="-247.499909"/>
<vertex x="171.091265625" y="40.9800125"/>
</polygon>
<text x="180.34" y="35.56" size="3.81" layer="94" rot="R180">License
CC-BY-SA</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="GND19" gate="1" x="35.56" y="50.8"/>
<instance part="IC4" gate="G$1" x="101.6" y="48.26"/>
<instance part="GND20" gate="1" x="58.42" y="30.48"/>
<instance part="C22" gate="G$1" x="45.72" y="60.96"/>
<instance part="C27" gate="G$1" x="76.2" y="30.48" rot="MR0"/>
<instance part="GND22" gate="1" x="76.2" y="20.32"/>
<instance part="C23" gate="G$1" x="58.42" y="43.18" rot="MR0"/>
<instance part="C24" gate="G$1" x="63.5" y="43.18"/>
<instance part="R26" gate="&gt;NAME" x="73.66" y="55.88" smashed="yes">
<attribute name="NAME" x="69.85" y="57.3786" size="1.778" layer="95"/>
<attribute name="VALUE" x="74.93" y="57.658" size="1.778" layer="96"/>
</instance>
<instance part="R27" gate="&gt;NAME" x="73.66" y="53.34" smashed="yes">
<attribute name="NAME" x="69.85" y="49.7586" size="1.778" layer="95"/>
<attribute name="VALUE" x="74.93" y="50.038" size="1.778" layer="96"/>
</instance>
<instance part="C25" gate="G$1" x="17.78" y="35.56" rot="MR0"/>
<instance part="C26" gate="G$1" x="22.86" y="35.56"/>
<instance part="GND21" gate="1" x="20.32" y="25.4"/>
<instance part="CON1" gate="G$1" x="17.78" y="66.04" rot="R270"/>
<instance part="L1" gate="G$1" x="73.66" y="68.58" smashed="yes">
<attribute name="NAME" x="66.04" y="71.12" size="1.778" layer="95"/>
<attribute name="VALUE" x="66.04" y="66.04" size="1.778" layer="96"/>
</instance>
<instance part="J23" gate="G$1" x="127" y="48.26" smashed="yes">
<attribute name="NAME" x="124.46" y="61.595" size="1.778" layer="95"/>
<attribute name="VALUE" x="134.62" y="48.26" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="SW1" gate="&gt;NAME" x="33.02" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="35.56" y="135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="35.56" y="125.73" size="1.778" layer="96"/>
</instance>
<instance part="C18" gate="G$1" x="22.86" y="129.54" rot="MR0"/>
<instance part="GND15" gate="1" x="33.02" y="119.38"/>
<instance part="R22" gate="&gt;NAME" x="33.02" y="147.32" rot="R90"/>
<instance part="SW2" gate="&gt;NAME" x="68.58" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="71.12" y="135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="71.12" y="125.73" size="1.778" layer="96"/>
</instance>
<instance part="C19" gate="G$1" x="60.96" y="129.54" rot="MR0"/>
<instance part="GND16" gate="1" x="68.58" y="119.38"/>
<instance part="R23" gate="&gt;NAME" x="68.58" y="147.32" rot="R90"/>
<instance part="SW3" gate="&gt;NAME" x="104.14" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="106.68" y="135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="106.68" y="125.73" size="1.778" layer="96"/>
</instance>
<instance part="C20" gate="G$1" x="93.98" y="129.54" rot="MR0"/>
<instance part="GND17" gate="1" x="104.14" y="119.38"/>
<instance part="R24" gate="&gt;NAME" x="104.14" y="147.32" rot="R90"/>
<instance part="SW4" gate="&gt;NAME" x="142.24" y="132.08" smashed="yes" rot="R270">
<attribute name="NAME" x="144.78" y="135.89" size="1.778" layer="95"/>
<attribute name="VALUE" x="144.78" y="125.73" size="1.778" layer="96"/>
</instance>
<instance part="C21" gate="G$1" x="129.54" y="129.54" rot="MR0"/>
<instance part="GND18" gate="1" x="142.24" y="119.38"/>
<instance part="R25" gate="&gt;NAME" x="142.24" y="147.32" rot="R90"/>
<instance part="LED17" gate="G$1" x="12.7" y="208.28" smashed="yes">
<attribute name="NAME" x="15.494" y="205.232" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="13.335" y="205.232" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED10" gate="G$1" x="33.02" y="210.82" smashed="yes">
<attribute name="NAME" x="35.814" y="207.772" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="33.655" y="207.772" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED11" gate="G$1" x="53.34" y="210.82" smashed="yes">
<attribute name="NAME" x="56.134" y="207.772" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="53.975" y="207.772" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED12" gate="G$1" x="73.66" y="210.82" smashed="yes">
<attribute name="NAME" x="76.454" y="207.772" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="74.295" y="207.772" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED13" gate="G$1" x="99.06" y="210.82" smashed="yes">
<attribute name="NAME" x="101.854" y="207.772" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="99.695" y="207.772" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED14" gate="G$1" x="119.38" y="210.82" smashed="yes">
<attribute name="NAME" x="122.174" y="207.772" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="120.015" y="207.772" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED15" gate="G$1" x="139.7" y="210.82" smashed="yes">
<attribute name="NAME" x="142.494" y="207.772" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="140.335" y="207.772" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED16" gate="G$1" x="160.02" y="210.82" smashed="yes">
<attribute name="NAME" x="162.814" y="207.772" size="1.778" layer="95" rot="R270"/>
<attribute name="VALUE" x="160.655" y="207.772" size="1.778" layer="96" rot="R270"/>
</instance>
<instance part="LED18" gate="G$1" x="20.32" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="17.526" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="19.685" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED19" gate="G$1" x="40.64" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="37.846" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="40.005" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED20" gate="G$1" x="60.96" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="58.166" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="60.325" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED21" gate="G$1" x="81.28" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="78.486" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="80.645" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED22" gate="G$1" x="106.68" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="103.886" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="106.045" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED23" gate="G$1" x="127" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="124.206" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="126.365" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED24" gate="G$1" x="147.32" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="144.526" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="146.685" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="LED25" gate="G$1" x="167.64" y="205.74" smashed="yes" rot="R180">
<attribute name="NAME" x="164.846" y="208.788" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="167.005" y="208.788" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R6" gate="&gt;NAME" x="12.7" y="220.98" rot="R270"/>
<instance part="R8" gate="&gt;NAME" x="33.02" y="220.98" rot="R270"/>
<instance part="R10" gate="&gt;NAME" x="53.34" y="220.98" rot="R270"/>
<instance part="R12" gate="&gt;NAME" x="73.66" y="220.98" rot="R270"/>
<instance part="R14" gate="&gt;NAME" x="99.06" y="220.98" rot="R270"/>
<instance part="R16" gate="&gt;NAME" x="119.38" y="220.98" rot="R270"/>
<instance part="R18" gate="&gt;NAME" x="139.7" y="220.98" rot="R270"/>
<instance part="R20" gate="&gt;NAME" x="160.02" y="220.98" rot="R270"/>
<instance part="R7" gate="&gt;NAME" x="20.32" y="220.98" rot="R90"/>
<instance part="R9" gate="&gt;NAME" x="40.64" y="220.98" rot="R90"/>
<instance part="R11" gate="&gt;NAME" x="60.96" y="220.98" rot="R90"/>
<instance part="R13" gate="&gt;NAME" x="81.28" y="220.98" rot="R90"/>
<instance part="R15" gate="&gt;NAME" x="106.68" y="220.98" rot="R90"/>
<instance part="R17" gate="&gt;NAME" x="127" y="220.98" rot="R90"/>
<instance part="R19" gate="&gt;NAME" x="147.32" y="220.98" rot="R90"/>
<instance part="R21" gate="&gt;NAME" x="167.64" y="220.98" rot="R90"/>
<instance part="J20" gate="G$1" x="91.44" y="187.96" smashed="yes" rot="MR270">
<attribute name="NAME" x="76.2" y="183.515" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="76.2" y="182.88" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND14" gate="1" x="172.72" y="190.5"/>
<instance part="J21" gate="G$1" x="154.94" y="157.48"/>
<instance part="J19" gate="G$1" x="88.9" y="241.3" smashed="yes" rot="MR270">
<attribute name="NAME" x="81.28" y="236.855" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="81.28" y="236.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="SUP17" gate="G$1" x="149.86" y="162.56"/>
<instance part="SUP16" gate="G$1" x="86.36" y="243.84"/>
<instance part="P+17" gate="G$1" x="20.32" y="48.26"/>
<instance part="P+16" gate="G$1" x="101.6" y="73.66"/>
<instance part="J22" gate="G$1" x="160.02" y="104.14" smashed="yes" rot="MR270">
<attribute name="NAME" x="144.78" y="102.235" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="144.78" y="101.6" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LOGO2" gate="G$1" x="149.86" y="40.64"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="GND19" gate="1" pin="GND"/>
<wire x1="35.56" y1="60.96" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="2"/>
<wire x1="35.56" y1="55.88" x2="35.56" y2="53.34" width="0.1524" layer="91"/>
<wire x1="45.72" y1="58.42" x2="45.72" y2="55.88" width="0.1524" layer="91"/>
<wire x1="45.72" y1="55.88" x2="35.56" y2="55.88" width="0.1524" layer="91"/>
<junction x="35.56" y="55.88"/>
<pinref part="CON1" gate="G$1" pin="GND"/>
<wire x1="33.02" y1="60.96" x2="35.56" y2="60.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C23" gate="G$1" pin="2"/>
<wire x1="58.42" y1="35.56" x2="58.42" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="2"/>
<wire x1="58.42" y1="35.56" x2="63.5" y2="35.56" width="0.1524" layer="91"/>
<wire x1="63.5" y1="35.56" x2="63.5" y2="40.64" width="0.1524" layer="91"/>
<junction x="58.42" y="35.56"/>
<pinref part="GND20" gate="1" pin="GND"/>
<wire x1="58.42" y1="35.56" x2="58.42" y2="33.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C27" gate="G$1" pin="2"/>
<pinref part="GND22" gate="1" pin="GND"/>
<wire x1="76.2" y1="27.94" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="GND"/>
<wire x1="76.2" y1="25.4" x2="76.2" y2="22.86" width="0.1524" layer="91"/>
<wire x1="76.2" y1="25.4" x2="101.6" y2="25.4" width="0.1524" layer="91"/>
<wire x1="101.6" y1="25.4" x2="101.6" y2="30.48" width="0.1524" layer="91"/>
<junction x="76.2" y="25.4"/>
</segment>
<segment>
<pinref part="C26" gate="G$1" pin="2"/>
<wire x1="17.78" y1="30.48" x2="20.32" y2="30.48" width="0.1524" layer="91"/>
<wire x1="20.32" y1="30.48" x2="22.86" y2="30.48" width="0.1524" layer="91"/>
<wire x1="22.86" y1="30.48" x2="22.86" y2="33.02" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="2"/>
<wire x1="17.78" y1="33.02" x2="17.78" y2="30.48" width="0.1524" layer="91"/>
<pinref part="GND21" gate="1" pin="GND"/>
<wire x1="20.32" y1="27.94" x2="20.32" y2="30.48" width="0.1524" layer="91"/>
<junction x="20.32" y="30.48"/>
</segment>
<segment>
<pinref part="SW1" gate="&gt;NAME" pin="S1"/>
<wire x1="30.48" y1="127" x2="30.48" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SW1" gate="&gt;NAME" pin="S"/>
<wire x1="30.48" y1="124.46" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<wire x1="33.02" y1="124.46" x2="33.02" y2="127" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="2"/>
<wire x1="30.48" y1="124.46" x2="22.86" y2="124.46" width="0.1524" layer="91"/>
<wire x1="22.86" y1="124.46" x2="22.86" y2="127" width="0.1524" layer="91"/>
<junction x="30.48" y="124.46"/>
<pinref part="GND15" gate="1" pin="GND"/>
<wire x1="33.02" y1="121.92" x2="33.02" y2="124.46" width="0.1524" layer="91"/>
<junction x="33.02" y="124.46"/>
</segment>
<segment>
<pinref part="SW2" gate="&gt;NAME" pin="S1"/>
<wire x1="66.04" y1="127" x2="66.04" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SW2" gate="&gt;NAME" pin="S"/>
<wire x1="66.04" y1="124.46" x2="68.58" y2="124.46" width="0.1524" layer="91"/>
<wire x1="68.58" y1="124.46" x2="68.58" y2="127" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="2"/>
<wire x1="66.04" y1="124.46" x2="60.96" y2="124.46" width="0.1524" layer="91"/>
<wire x1="60.96" y1="124.46" x2="60.96" y2="127" width="0.1524" layer="91"/>
<junction x="66.04" y="124.46"/>
<pinref part="GND16" gate="1" pin="GND"/>
<wire x1="68.58" y1="121.92" x2="68.58" y2="124.46" width="0.1524" layer="91"/>
<junction x="68.58" y="124.46"/>
</segment>
<segment>
<pinref part="SW3" gate="&gt;NAME" pin="S1"/>
<wire x1="101.6" y1="127" x2="101.6" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SW3" gate="&gt;NAME" pin="S"/>
<wire x1="101.6" y1="124.46" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<wire x1="104.14" y1="124.46" x2="104.14" y2="127" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="2"/>
<wire x1="101.6" y1="124.46" x2="93.98" y2="124.46" width="0.1524" layer="91"/>
<wire x1="93.98" y1="124.46" x2="93.98" y2="127" width="0.1524" layer="91"/>
<junction x="101.6" y="124.46"/>
<pinref part="GND17" gate="1" pin="GND"/>
<wire x1="104.14" y1="121.92" x2="104.14" y2="124.46" width="0.1524" layer="91"/>
<junction x="104.14" y="124.46"/>
</segment>
<segment>
<pinref part="SW4" gate="&gt;NAME" pin="S1"/>
<wire x1="139.7" y1="127" x2="139.7" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SW4" gate="&gt;NAME" pin="S"/>
<wire x1="139.7" y1="124.46" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<wire x1="142.24" y1="124.46" x2="142.24" y2="127" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="2"/>
<wire x1="139.7" y1="124.46" x2="129.54" y2="124.46" width="0.1524" layer="91"/>
<wire x1="129.54" y1="124.46" x2="129.54" y2="127" width="0.1524" layer="91"/>
<junction x="139.7" y="124.46"/>
<pinref part="GND18" gate="1" pin="GND"/>
<wire x1="142.24" y1="121.92" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<junction x="142.24" y="124.46"/>
</segment>
<segment>
<pinref part="R7" gate="&gt;NAME" pin="2"/>
<wire x1="20.32" y1="226.06" x2="20.32" y2="228.6" width="0.1524" layer="91"/>
<pinref part="R21" gate="&gt;NAME" pin="2"/>
<wire x1="20.32" y1="228.6" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<wire x1="40.64" y1="228.6" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="228.6" x2="81.28" y2="228.6" width="0.1524" layer="91"/>
<wire x1="81.28" y1="228.6" x2="106.68" y2="228.6" width="0.1524" layer="91"/>
<wire x1="106.68" y1="228.6" x2="127" y2="228.6" width="0.1524" layer="91"/>
<wire x1="127" y1="228.6" x2="147.32" y2="228.6" width="0.1524" layer="91"/>
<wire x1="147.32" y1="228.6" x2="167.64" y2="228.6" width="0.1524" layer="91"/>
<wire x1="167.64" y1="228.6" x2="167.64" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R19" gate="&gt;NAME" pin="2"/>
<wire x1="147.32" y1="226.06" x2="147.32" y2="228.6" width="0.1524" layer="91"/>
<junction x="147.32" y="228.6"/>
<pinref part="R17" gate="&gt;NAME" pin="2"/>
<wire x1="127" y1="226.06" x2="127" y2="228.6" width="0.1524" layer="91"/>
<junction x="127" y="228.6"/>
<pinref part="R15" gate="&gt;NAME" pin="2"/>
<wire x1="106.68" y1="226.06" x2="106.68" y2="228.6" width="0.1524" layer="91"/>
<junction x="106.68" y="228.6"/>
<pinref part="R13" gate="&gt;NAME" pin="2"/>
<wire x1="81.28" y1="226.06" x2="81.28" y2="228.6" width="0.1524" layer="91"/>
<junction x="81.28" y="228.6"/>
<pinref part="R11" gate="&gt;NAME" pin="2"/>
<wire x1="60.96" y1="226.06" x2="60.96" y2="228.6" width="0.1524" layer="91"/>
<junction x="60.96" y="228.6"/>
<pinref part="R9" gate="&gt;NAME" pin="2"/>
<wire x1="40.64" y1="226.06" x2="40.64" y2="228.6" width="0.1524" layer="91"/>
<junction x="40.64" y="228.6"/>
<wire x1="167.64" y1="228.6" x2="172.72" y2="228.6" width="0.1524" layer="91"/>
<junction x="167.64" y="228.6"/>
<pinref part="GND14" gate="1" pin="GND"/>
<wire x1="172.72" y1="228.6" x2="172.72" y2="193.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D-" class="0">
<segment>
<label x="35.56" y="66.04" size="1.27" layer="95" xref="yes"/>
<pinref part="CON1" gate="G$1" pin="D-"/>
<wire x1="33.02" y1="66.04" x2="35.56" y2="66.04" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="53.34" y1="55.88" x2="58.42" y2="55.88" width="0.1524" layer="91"/>
<pinref part="C23" gate="G$1" pin="1"/>
<wire x1="58.42" y1="55.88" x2="58.42" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R26" gate="&gt;NAME" pin="1"/>
<wire x1="58.42" y1="55.88" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<junction x="58.42" y="55.88"/>
<label x="53.34" y="55.88" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<label x="35.56" y="63.5" size="1.27" layer="95" xref="yes"/>
<pinref part="CON1" gate="G$1" pin="D+"/>
<wire x1="33.02" y1="63.5" x2="35.56" y2="63.5" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="53.34" y1="53.34" x2="63.5" y2="53.34" width="0.1524" layer="91"/>
<pinref part="C24" gate="G$1" pin="1"/>
<wire x1="63.5" y1="53.34" x2="63.5" y2="48.26" width="0.1524" layer="91"/>
<pinref part="R27" gate="&gt;NAME" pin="1"/>
<wire x1="63.5" y1="53.34" x2="68.58" y2="53.34" width="0.1524" layer="91"/>
<junction x="63.5" y="53.34"/>
<label x="53.34" y="53.34" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="VCCIO"/>
<pinref part="C27" gate="G$1" pin="1"/>
<wire x1="81.28" y1="43.18" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<wire x1="76.2" y1="43.18" x2="76.2" y2="40.64" width="0.1524" layer="91"/>
<pinref part="IC4" gate="G$1" pin="!RESET!"/>
<wire x1="76.2" y1="40.64" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<wire x1="76.2" y1="38.1" x2="76.2" y2="35.56" width="0.1524" layer="91"/>
<wire x1="81.28" y1="40.64" x2="76.2" y2="40.64" width="0.1524" layer="91"/>
<junction x="76.2" y="40.64"/>
<pinref part="IC4" gate="G$1" pin="3V3OUT"/>
<wire x1="81.28" y1="38.1" x2="76.2" y2="38.1" width="0.1524" layer="91"/>
<junction x="76.2" y="38.1"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="R27" gate="&gt;NAME" pin="2"/>
<pinref part="IC4" gate="G$1" pin="D+"/>
<wire x1="78.74" y1="53.34" x2="81.28" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="R26" gate="&gt;NAME" pin="2"/>
<pinref part="IC4" gate="G$1" pin="D-"/>
<wire x1="78.74" y1="55.88" x2="81.28" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VBUS" class="0">
<segment>
<wire x1="33.02" y1="68.58" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
<pinref part="C22" gate="G$1" pin="1"/>
<wire x1="45.72" y1="68.58" x2="45.72" y2="66.04" width="0.1524" layer="91"/>
<junction x="45.72" y="68.58"/>
<pinref part="CON1" gate="G$1" pin="VBUS"/>
<pinref part="L1" gate="G$1" pin="P$1"/>
<wire x1="66.04" y1="68.58" x2="45.72" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<pinref part="SW1" gate="&gt;NAME" pin="P1"/>
<wire x1="30.48" y1="137.16" x2="30.48" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SW1" gate="&gt;NAME" pin="P"/>
<wire x1="30.48" y1="139.7" x2="33.02" y2="139.7" width="0.1524" layer="91"/>
<wire x1="33.02" y1="139.7" x2="33.02" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C18" gate="G$1" pin="1"/>
<wire x1="30.48" y1="139.7" x2="22.86" y2="139.7" width="0.1524" layer="91"/>
<wire x1="22.86" y1="139.7" x2="22.86" y2="134.62" width="0.1524" layer="91"/>
<junction x="30.48" y="139.7"/>
<pinref part="R22" gate="&gt;NAME" pin="1"/>
<wire x1="33.02" y1="139.7" x2="33.02" y2="142.24" width="0.1524" layer="91"/>
<junction x="33.02" y="139.7"/>
<wire x1="33.02" y1="139.7" x2="50.8" y2="139.7" width="0.1524" layer="91"/>
<wire x1="50.8" y1="139.7" x2="50.8" y2="109.22" width="0.1524" layer="91"/>
<wire x1="50.8" y1="109.22" x2="149.86" y2="109.22" width="0.1524" layer="91"/>
<wire x1="149.86" y1="109.22" x2="149.86" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J22" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$10" class="0">
<segment>
<pinref part="SW2" gate="&gt;NAME" pin="P1"/>
<wire x1="66.04" y1="137.16" x2="66.04" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SW2" gate="&gt;NAME" pin="P"/>
<wire x1="66.04" y1="139.7" x2="68.58" y2="139.7" width="0.1524" layer="91"/>
<wire x1="68.58" y1="139.7" x2="68.58" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C19" gate="G$1" pin="1"/>
<wire x1="66.04" y1="139.7" x2="60.96" y2="139.7" width="0.1524" layer="91"/>
<wire x1="60.96" y1="139.7" x2="60.96" y2="134.62" width="0.1524" layer="91"/>
<junction x="66.04" y="139.7"/>
<pinref part="R23" gate="&gt;NAME" pin="1"/>
<wire x1="68.58" y1="139.7" x2="68.58" y2="142.24" width="0.1524" layer="91"/>
<junction x="68.58" y="139.7"/>
<wire x1="68.58" y1="139.7" x2="83.82" y2="139.7" width="0.1524" layer="91"/>
<wire x1="83.82" y1="139.7" x2="83.82" y2="111.76" width="0.1524" layer="91"/>
<wire x1="83.82" y1="111.76" x2="152.4" y2="111.76" width="0.1524" layer="91"/>
<wire x1="152.4" y1="111.76" x2="152.4" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J22" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="SW3" gate="&gt;NAME" pin="P1"/>
<wire x1="101.6" y1="137.16" x2="101.6" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SW3" gate="&gt;NAME" pin="P"/>
<wire x1="101.6" y1="139.7" x2="104.14" y2="139.7" width="0.1524" layer="91"/>
<wire x1="104.14" y1="139.7" x2="104.14" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C20" gate="G$1" pin="1"/>
<wire x1="101.6" y1="139.7" x2="93.98" y2="139.7" width="0.1524" layer="91"/>
<wire x1="93.98" y1="139.7" x2="93.98" y2="134.62" width="0.1524" layer="91"/>
<junction x="101.6" y="139.7"/>
<pinref part="R24" gate="&gt;NAME" pin="1"/>
<wire x1="104.14" y1="139.7" x2="104.14" y2="142.24" width="0.1524" layer="91"/>
<junction x="104.14" y="139.7"/>
<wire x1="104.14" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<wire x1="119.38" y1="139.7" x2="119.38" y2="114.3" width="0.1524" layer="91"/>
<wire x1="119.38" y1="114.3" x2="154.94" y2="114.3" width="0.1524" layer="91"/>
<wire x1="154.94" y1="114.3" x2="154.94" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J22" gate="G$1" pin="3"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="SW4" gate="&gt;NAME" pin="P1"/>
<wire x1="139.7" y1="137.16" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SW4" gate="&gt;NAME" pin="P"/>
<wire x1="139.7" y1="139.7" x2="142.24" y2="139.7" width="0.1524" layer="91"/>
<wire x1="142.24" y1="139.7" x2="142.24" y2="137.16" width="0.1524" layer="91"/>
<pinref part="C21" gate="G$1" pin="1"/>
<wire x1="139.7" y1="139.7" x2="129.54" y2="139.7" width="0.1524" layer="91"/>
<wire x1="129.54" y1="139.7" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
<junction x="139.7" y="139.7"/>
<pinref part="R25" gate="&gt;NAME" pin="1"/>
<wire x1="142.24" y1="139.7" x2="142.24" y2="142.24" width="0.1524" layer="91"/>
<junction x="142.24" y="139.7"/>
<wire x1="142.24" y1="139.7" x2="157.48" y2="139.7" width="0.1524" layer="91"/>
<wire x1="157.48" y1="139.7" x2="157.48" y2="106.68" width="0.1524" layer="91"/>
<pinref part="J22" gate="G$1" pin="4"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<pinref part="R6" gate="&gt;NAME" pin="2"/>
<pinref part="LED17" gate="G$1" pin="A"/>
<wire x1="12.7" y1="215.9" x2="12.7" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$29" class="0">
<segment>
<pinref part="R8" gate="&gt;NAME" pin="2"/>
<pinref part="LED10" gate="G$1" pin="A"/>
<wire x1="33.02" y1="215.9" x2="33.02" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$30" class="0">
<segment>
<pinref part="R10" gate="&gt;NAME" pin="2"/>
<pinref part="LED11" gate="G$1" pin="A"/>
<wire x1="53.34" y1="215.9" x2="53.34" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<pinref part="R12" gate="&gt;NAME" pin="2"/>
<pinref part="LED12" gate="G$1" pin="A"/>
<wire x1="73.66" y1="215.9" x2="73.66" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$32" class="0">
<segment>
<pinref part="R14" gate="&gt;NAME" pin="2"/>
<pinref part="LED13" gate="G$1" pin="A"/>
<wire x1="99.06" y1="215.9" x2="99.06" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$33" class="0">
<segment>
<pinref part="R16" gate="&gt;NAME" pin="2"/>
<pinref part="LED14" gate="G$1" pin="A"/>
<wire x1="119.38" y1="215.9" x2="119.38" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$34" class="0">
<segment>
<pinref part="R18" gate="&gt;NAME" pin="2"/>
<pinref part="LED15" gate="G$1" pin="A"/>
<wire x1="139.7" y1="215.9" x2="139.7" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$35" class="0">
<segment>
<pinref part="R20" gate="&gt;NAME" pin="2"/>
<pinref part="LED16" gate="G$1" pin="A"/>
<wire x1="160.02" y1="215.9" x2="160.02" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$36" class="0">
<segment>
<pinref part="LED18" gate="G$1" pin="C"/>
<pinref part="R7" gate="&gt;NAME" pin="1"/>
<wire x1="20.32" y1="210.82" x2="20.32" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$37" class="0">
<segment>
<pinref part="LED19" gate="G$1" pin="C"/>
<pinref part="R9" gate="&gt;NAME" pin="1"/>
<wire x1="40.64" y1="210.82" x2="40.64" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$38" class="0">
<segment>
<pinref part="LED20" gate="G$1" pin="C"/>
<pinref part="R11" gate="&gt;NAME" pin="1"/>
<wire x1="60.96" y1="210.82" x2="60.96" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$39" class="0">
<segment>
<pinref part="LED21" gate="G$1" pin="C"/>
<pinref part="R13" gate="&gt;NAME" pin="1"/>
<wire x1="81.28" y1="210.82" x2="81.28" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$40" class="0">
<segment>
<pinref part="LED22" gate="G$1" pin="C"/>
<pinref part="R15" gate="&gt;NAME" pin="1"/>
<wire x1="106.68" y1="210.82" x2="106.68" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<pinref part="LED23" gate="G$1" pin="C"/>
<pinref part="R17" gate="&gt;NAME" pin="1"/>
<wire x1="127" y1="210.82" x2="127" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<pinref part="LED24" gate="G$1" pin="C"/>
<pinref part="R19" gate="&gt;NAME" pin="1"/>
<wire x1="147.32" y1="210.82" x2="147.32" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<pinref part="LED25" gate="G$1" pin="C"/>
<pinref part="R21" gate="&gt;NAME" pin="1"/>
<wire x1="167.64" y1="210.82" x2="167.64" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<pinref part="LED17" gate="G$1" pin="C"/>
<wire x1="12.7" y1="203.2" x2="12.7" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED18" gate="G$1" pin="A"/>
<wire x1="12.7" y1="200.66" x2="20.32" y2="200.66" width="0.1524" layer="91"/>
<wire x1="20.32" y1="200.66" x2="20.32" y2="203.2" width="0.1524" layer="91"/>
<wire x1="20.32" y1="200.66" x2="20.32" y2="190.5" width="0.1524" layer="91"/>
<junction x="20.32" y="200.66"/>
<pinref part="J20" gate="G$1" pin="1"/>
<wire x1="20.32" y1="190.5" x2="81.28" y2="190.5" width="0.1524" layer="91"/>
<wire x1="81.28" y1="190.5" x2="81.28" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<pinref part="LED10" gate="G$1" pin="C"/>
<wire x1="33.02" y1="205.74" x2="33.02" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED19" gate="G$1" pin="A"/>
<wire x1="33.02" y1="200.66" x2="40.64" y2="200.66" width="0.1524" layer="91"/>
<wire x1="40.64" y1="200.66" x2="40.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="40.64" y1="200.66" x2="40.64" y2="193.04" width="0.1524" layer="91"/>
<junction x="40.64" y="200.66"/>
<pinref part="J20" gate="G$1" pin="2"/>
<wire x1="40.64" y1="193.04" x2="83.82" y2="193.04" width="0.1524" layer="91"/>
<wire x1="83.82" y1="193.04" x2="83.82" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<pinref part="LED11" gate="G$1" pin="C"/>
<wire x1="53.34" y1="205.74" x2="53.34" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED20" gate="G$1" pin="A"/>
<wire x1="53.34" y1="200.66" x2="60.96" y2="200.66" width="0.1524" layer="91"/>
<wire x1="60.96" y1="200.66" x2="60.96" y2="203.2" width="0.1524" layer="91"/>
<wire x1="60.96" y1="200.66" x2="60.96" y2="195.58" width="0.1524" layer="91"/>
<junction x="60.96" y="200.66"/>
<pinref part="J20" gate="G$1" pin="3"/>
<wire x1="60.96" y1="195.58" x2="86.36" y2="195.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="195.58" x2="86.36" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$47" class="0">
<segment>
<pinref part="LED12" gate="G$1" pin="C"/>
<wire x1="73.66" y1="205.74" x2="73.66" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED21" gate="G$1" pin="A"/>
<wire x1="73.66" y1="200.66" x2="81.28" y2="200.66" width="0.1524" layer="91"/>
<wire x1="81.28" y1="200.66" x2="81.28" y2="203.2" width="0.1524" layer="91"/>
<wire x1="81.28" y1="200.66" x2="81.28" y2="198.12" width="0.1524" layer="91"/>
<junction x="81.28" y="200.66"/>
<pinref part="J20" gate="G$1" pin="4"/>
<wire x1="81.28" y1="198.12" x2="88.9" y2="198.12" width="0.1524" layer="91"/>
<wire x1="88.9" y1="198.12" x2="88.9" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$48" class="0">
<segment>
<pinref part="LED13" gate="G$1" pin="C"/>
<wire x1="99.06" y1="205.74" x2="99.06" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED22" gate="G$1" pin="A"/>
<wire x1="99.06" y1="200.66" x2="106.68" y2="200.66" width="0.1524" layer="91"/>
<wire x1="106.68" y1="200.66" x2="106.68" y2="203.2" width="0.1524" layer="91"/>
<pinref part="J20" gate="G$1" pin="5"/>
<wire x1="91.44" y1="187.96" x2="91.44" y2="198.12" width="0.1524" layer="91"/>
<wire x1="91.44" y1="198.12" x2="99.06" y2="198.12" width="0.1524" layer="91"/>
<wire x1="99.06" y1="198.12" x2="99.06" y2="200.66" width="0.1524" layer="91"/>
<junction x="99.06" y="200.66"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<pinref part="LED14" gate="G$1" pin="C"/>
<wire x1="119.38" y1="205.74" x2="119.38" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED23" gate="G$1" pin="A"/>
<wire x1="119.38" y1="200.66" x2="127" y2="200.66" width="0.1524" layer="91"/>
<wire x1="127" y1="200.66" x2="127" y2="203.2" width="0.1524" layer="91"/>
<wire x1="119.38" y1="200.66" x2="119.38" y2="195.58" width="0.1524" layer="91"/>
<junction x="119.38" y="200.66"/>
<pinref part="J20" gate="G$1" pin="6"/>
<wire x1="119.38" y1="195.58" x2="93.98" y2="195.58" width="0.1524" layer="91"/>
<wire x1="93.98" y1="195.58" x2="93.98" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$50" class="0">
<segment>
<pinref part="LED15" gate="G$1" pin="C"/>
<wire x1="139.7" y1="205.74" x2="139.7" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED24" gate="G$1" pin="A"/>
<wire x1="139.7" y1="200.66" x2="147.32" y2="200.66" width="0.1524" layer="91"/>
<wire x1="147.32" y1="200.66" x2="147.32" y2="203.2" width="0.1524" layer="91"/>
<wire x1="139.7" y1="200.66" x2="139.7" y2="193.04" width="0.1524" layer="91"/>
<junction x="139.7" y="200.66"/>
<pinref part="J20" gate="G$1" pin="7"/>
<wire x1="139.7" y1="193.04" x2="96.52" y2="193.04" width="0.1524" layer="91"/>
<wire x1="96.52" y1="193.04" x2="96.52" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$51" class="0">
<segment>
<pinref part="LED16" gate="G$1" pin="C"/>
<wire x1="160.02" y1="205.74" x2="160.02" y2="200.66" width="0.1524" layer="91"/>
<pinref part="LED25" gate="G$1" pin="A"/>
<wire x1="160.02" y1="200.66" x2="167.64" y2="200.66" width="0.1524" layer="91"/>
<wire x1="167.64" y1="200.66" x2="167.64" y2="203.2" width="0.1524" layer="91"/>
<wire x1="160.02" y1="200.66" x2="160.02" y2="190.5" width="0.1524" layer="91"/>
<junction x="160.02" y="200.66"/>
<pinref part="J20" gate="G$1" pin="8"/>
<wire x1="160.02" y1="190.5" x2="99.06" y2="190.5" width="0.1524" layer="91"/>
<wire x1="99.06" y1="190.5" x2="99.06" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="SUP17" gate="G$1" pin="VCC"/>
<wire x1="149.86" y1="162.56" x2="149.86" y2="160.02" width="0.1524" layer="91"/>
<pinref part="J21" gate="G$1" pin="1"/>
<wire x1="149.86" y1="160.02" x2="154.94" y2="160.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP16" gate="G$1" pin="VCC"/>
<pinref part="J19" gate="G$1" pin="1"/>
<wire x1="86.36" y1="243.84" x2="86.36" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="R22" gate="&gt;NAME" pin="2"/>
<wire x1="33.02" y1="154.94" x2="33.02" y2="152.4" width="0.1524" layer="91"/>
<wire x1="33.02" y1="154.94" x2="68.58" y2="154.94" width="0.1524" layer="91"/>
<wire x1="68.58" y1="154.94" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<wire x1="104.14" y1="154.94" x2="142.24" y2="154.94" width="0.1524" layer="91"/>
<wire x1="142.24" y1="154.94" x2="154.94" y2="154.94" width="0.1524" layer="91"/>
<pinref part="R25" gate="&gt;NAME" pin="2"/>
<wire x1="142.24" y1="154.94" x2="142.24" y2="152.4" width="0.1524" layer="91"/>
<junction x="142.24" y="154.94"/>
<pinref part="R23" gate="&gt;NAME" pin="2"/>
<wire x1="68.58" y1="152.4" x2="68.58" y2="154.94" width="0.1524" layer="91"/>
<junction x="68.58" y="154.94"/>
<pinref part="R24" gate="&gt;NAME" pin="2"/>
<wire x1="104.14" y1="152.4" x2="104.14" y2="154.94" width="0.1524" layer="91"/>
<junction x="104.14" y="154.94"/>
<pinref part="J21" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<pinref part="R20" gate="&gt;NAME" pin="1"/>
<wire x1="12.7" y1="231.14" x2="33.02" y2="231.14" width="0.1524" layer="91"/>
<wire x1="33.02" y1="231.14" x2="53.34" y2="231.14" width="0.1524" layer="91"/>
<wire x1="53.34" y1="231.14" x2="73.66" y2="231.14" width="0.1524" layer="91"/>
<wire x1="73.66" y1="231.14" x2="99.06" y2="231.14" width="0.1524" layer="91"/>
<wire x1="99.06" y1="231.14" x2="119.38" y2="231.14" width="0.1524" layer="91"/>
<wire x1="119.38" y1="231.14" x2="139.7" y2="231.14" width="0.1524" layer="91"/>
<wire x1="139.7" y1="231.14" x2="160.02" y2="231.14" width="0.1524" layer="91"/>
<wire x1="160.02" y1="231.14" x2="160.02" y2="226.06" width="0.1524" layer="91"/>
<pinref part="R18" gate="&gt;NAME" pin="1"/>
<wire x1="139.7" y1="226.06" x2="139.7" y2="231.14" width="0.1524" layer="91"/>
<junction x="139.7" y="231.14"/>
<pinref part="R6" gate="&gt;NAME" pin="1"/>
<wire x1="12.7" y1="226.06" x2="12.7" y2="231.14" width="0.1524" layer="91"/>
<pinref part="R8" gate="&gt;NAME" pin="1"/>
<wire x1="33.02" y1="226.06" x2="33.02" y2="231.14" width="0.1524" layer="91"/>
<junction x="33.02" y="231.14"/>
<pinref part="R10" gate="&gt;NAME" pin="1"/>
<wire x1="53.34" y1="226.06" x2="53.34" y2="231.14" width="0.1524" layer="91"/>
<junction x="53.34" y="231.14"/>
<pinref part="R12" gate="&gt;NAME" pin="1"/>
<wire x1="73.66" y1="226.06" x2="73.66" y2="231.14" width="0.1524" layer="91"/>
<junction x="73.66" y="231.14"/>
<pinref part="R14" gate="&gt;NAME" pin="1"/>
<wire x1="99.06" y1="226.06" x2="99.06" y2="231.14" width="0.1524" layer="91"/>
<junction x="99.06" y="231.14"/>
<pinref part="R16" gate="&gt;NAME" pin="1"/>
<wire x1="119.38" y1="226.06" x2="119.38" y2="231.14" width="0.1524" layer="91"/>
<junction x="119.38" y="231.14"/>
<pinref part="J19" gate="G$1" pin="2"/>
<wire x1="91.44" y1="241.3" x2="91.44" y2="243.84" width="0.1524" layer="91"/>
<wire x1="91.44" y1="243.84" x2="99.06" y2="243.84" width="0.1524" layer="91"/>
<wire x1="99.06" y1="243.84" x2="99.06" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+5V_USB" class="0">
<segment>
<pinref part="C26" gate="G$1" pin="1"/>
<wire x1="17.78" y1="43.18" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<wire x1="20.32" y1="43.18" x2="22.86" y2="43.18" width="0.1524" layer="91"/>
<wire x1="22.86" y1="43.18" x2="22.86" y2="40.64" width="0.1524" layer="91"/>
<pinref part="C25" gate="G$1" pin="1"/>
<wire x1="17.78" y1="40.64" x2="17.78" y2="43.18" width="0.1524" layer="91"/>
<wire x1="20.32" y1="45.72" x2="20.32" y2="43.18" width="0.1524" layer="91"/>
<junction x="20.32" y="43.18"/>
<pinref part="P+17" gate="G$1" pin="+5V_USB"/>
</segment>
<segment>
<pinref part="IC4" gate="G$1" pin="VCC"/>
<wire x1="101.6" y1="66.04" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<wire x1="101.6" y1="68.58" x2="101.6" y2="71.12" width="0.1524" layer="91"/>
<wire x1="101.6" y1="68.58" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<junction x="101.6" y="68.58"/>
<pinref part="L1" gate="G$1" pin="P$2"/>
<pinref part="P+16" gate="G$1" pin="+5V_USB"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="TXD"/>
<pinref part="J23" gate="G$1" pin="1"/>
<wire x1="121.92" y1="58.42" x2="124.46" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="J23" gate="G$1" pin="2"/>
<pinref part="IC4" gate="G$1" pin="RXD"/>
<wire x1="124.46" y1="55.88" x2="121.92" y2="55.88" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="IC4" gate="G$1" pin="!RTS!"/>
<pinref part="J23" gate="G$1" pin="3"/>
<wire x1="121.92" y1="53.34" x2="124.46" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="J23" gate="G$1" pin="4"/>
<pinref part="IC4" gate="G$1" pin="!CTS!"/>
<wire x1="124.46" y1="50.8" x2="121.92" y2="50.8" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<plain>
<wire x1="83.82" y1="193.04" x2="160.02" y2="193.04" width="0.4064" layer="97"/>
<wire x1="83.82" y1="134.62" x2="83.82" y2="193.04" width="0.4064" layer="97"/>
<wire x1="160.02" y1="134.62" x2="160.02" y2="193.04" width="0.4064" layer="97"/>
<text x="86.36" y="190.5" size="3.81" layer="97" font="vector" rot="MR180">ESP</text>
<wire x1="83.82" y1="134.62" x2="160.02" y2="134.62" width="0.4064" layer="97"/>
<wire x1="83.82" y1="198.12" x2="160.02" y2="198.12" width="0.4064" layer="97"/>
<wire x1="160.02" y1="198.12" x2="160.02" y2="246.38" width="0.4064" layer="97"/>
<wire x1="83.82" y1="198.12" x2="83.82" y2="246.38" width="0.4064" layer="97"/>
<wire x1="83.82" y1="246.38" x2="160.02" y2="246.38" width="0.4064" layer="97"/>
<text x="86.36" y="243.84" size="3.81" layer="97" font="vector" rot="MR180">Trimer</text>
<wire x1="22.86" y1="172.72" x2="22.86" y2="246.38" width="0.4064" layer="97"/>
<wire x1="78.74" y1="172.72" x2="78.74" y2="246.38" width="0.4064" layer="97"/>
<wire x1="22.86" y1="246.38" x2="78.74" y2="246.38" width="0.4064" layer="97"/>
<wire x1="22.86" y1="172.72" x2="78.74" y2="172.72" width="0.4064" layer="97"/>
<text x="25.4" y="243.84" size="3.81" layer="97" font="vector" rot="MR180">Screw
Terminals</text>
<wire x1="22.86" y1="129.54" x2="160.02" y2="129.54" width="0.4064" layer="97"/>
<wire x1="160.02" y1="91.44" x2="22.86" y2="91.44" width="0.4064" layer="97"/>
<wire x1="160.02" y1="91.44" x2="160.02" y2="129.54" width="0.4064" layer="97"/>
<wire x1="22.86" y1="91.44" x2="22.86" y2="129.54" width="0.4064" layer="97"/>
<text x="25.4" y="127" size="3.81" layer="97" font="vector" rot="MR180">Encoder</text>
<wire x1="22.86" y1="134.62" x2="22.86" y2="167.64" width="0.4064" layer="97"/>
<wire x1="78.74" y1="134.62" x2="22.86" y2="134.62" width="0.4064" layer="97"/>
<wire x1="78.74" y1="134.62" x2="78.74" y2="167.64" width="0.4064" layer="97"/>
<wire x1="22.86" y1="167.64" x2="78.74" y2="167.64" width="0.4064" layer="97"/>
<text x="30.48" y="165.1" size="3.81" layer="97" font="vector" rot="MR180">Vcc selector</text>
<text x="167.64" y="5.08" size="2.1844" layer="94" font="vector">v1.0c</text>
<polygon width="0.00038125" layer="94">
<vertex x="172.184784375" y="38.340025" curve="9.499998"/>
<vertex x="172.956115625" y="38.7372875"/>
<vertex x="174.871340625" y="37.175359375"/>
<vertex x="176.184640625" y="38.488659375"/>
<vertex x="174.6227125" y="40.403884375" curve="19.000007"/>
<vertex x="175.28448125" y="42.0015375"/>
<vertex x="177.743203125" y="42.25135625"/>
<vertex x="177.743203125" y="44.10864375"/>
<vertex x="175.28448125" y="44.3584625" curve="19.000007"/>
<vertex x="174.6227125" y="45.956115625"/>
<vertex x="176.184640625" y="47.871340625"/>
<vertex x="174.871340625" y="49.184640625"/>
<vertex x="172.956115625" y="47.6227125" curve="19.000007"/>
<vertex x="171.3584625" y="48.28448125"/>
<vertex x="171.10864375" y="50.743203125"/>
<vertex x="169.25135625" y="50.743203125"/>
<vertex x="169.0015375" y="48.28448125" curve="19.000007"/>
<vertex x="167.403884375" y="47.6227125"/>
<vertex x="165.488659375" y="49.184640625"/>
<vertex x="164.175359375" y="47.871340625"/>
<vertex x="165.7372875" y="45.956115625" curve="19.000007"/>
<vertex x="165.07551875" y="44.3584625"/>
<vertex x="162.616796875" y="44.10864375"/>
<vertex x="162.616796875" y="42.25135625"/>
<vertex x="165.07551875" y="42.0015375" curve="19.000007"/>
<vertex x="165.7372875" y="40.403884375"/>
<vertex x="164.175359375" y="38.488659375"/>
<vertex x="165.488659375" y="37.175359375"/>
<vertex x="167.403884375" y="38.7372875" curve="9.499998"/>
<vertex x="168.175215625" y="38.340025"/>
<vertex x="169.268734375" y="40.9800125" curve="-67.499973"/>
<vertex x="167.79875" y="43.18" curve="-247.499909"/>
<vertex x="171.091265625" y="40.9800125"/>
</polygon>
<text x="180.34" y="35.56" size="3.81" layer="94" rot="R180">License
CC-BY-SA</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="CON2" gate="G$1" x="53.34" y="223.52" rot="MR0"/>
<instance part="CON3" gate="G$1" x="53.34" y="205.74" rot="MR0"/>
<instance part="CON4" gate="G$1" x="53.34" y="187.96" rot="MR0"/>
<instance part="J27" gate="G$1" x="66.04" y="205.74"/>
<instance part="TR1" gate="G$1" x="127" y="213.36" rot="MR0"/>
<instance part="J26" gate="G$1" x="142.24" y="210.82" rot="MR180"/>
<instance part="J25" gate="G$1" x="104.14" y="215.9" rot="MR0"/>
<instance part="SUP18" gate="G$1" x="106.68" y="231.14"/>
<instance part="J24" gate="G$1" x="119.38" y="233.68" smashed="yes" rot="MR90">
<attribute name="NAME" x="111.76" y="236.855" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="111.76" y="236.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GND23" gate="1" x="139.7" y="203.2"/>
<instance part="J29" gate="G$1" x="121.92" y="154.94"/>
<instance part="GND25" gate="1" x="96.52" y="139.7"/>
<instance part="J30" gate="G$1" x="147.32" y="149.86" smashed="yes">
<attribute name="NAME" x="144.78" y="163.195" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="149.86" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R30" gate="&gt;NAME" x="139.7" y="167.64" rot="R90"/>
<instance part="R29" gate="&gt;NAME" x="99.06" y="167.64" rot="R90"/>
<instance part="R28" gate="&gt;NAME" x="91.44" y="167.64" rot="R90"/>
<instance part="+3V38" gate="G$1" x="142.24" y="182.88"/>
<instance part="C28" gate="G$1" x="124.46" y="170.18"/>
<instance part="GND24" gate="1" x="124.46" y="165.1"/>
<instance part="J28" gate="G$1" x="144.78" y="175.26" smashed="yes">
<attribute name="NAME" x="144.78" y="180.975" size="1.778" layer="95"/>
<attribute name="VALUE" x="154.94" y="167.64" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="ENC1" gate="G$1" x="137.16" y="104.14" smashed="yes" rot="R270">
<attribute name="NAME" x="132.715" y="112.395" size="1.778" layer="95"/>
<attribute name="VALUE" x="132.715" y="109.855" size="1.778" layer="96"/>
</instance>
<instance part="R31" gate="&gt;NAME" x="109.22" y="114.3" rot="R90"/>
<instance part="R32" gate="&gt;NAME" x="121.92" y="114.3" rot="R90"/>
<instance part="GND26" gate="1" x="127" y="96.52"/>
<instance part="J33" gate="G$1" x="101.6" y="104.14" rot="MR0"/>
<instance part="SUP20" gate="G$1" x="71.12" y="116.84"/>
<instance part="SUP19" gate="G$1" x="45.72" y="152.4"/>
<instance part="+3V39" gate="G$1" x="38.1" y="154.94"/>
<instance part="P+18" gate="1" x="50.8" y="154.94"/>
<instance part="J32" gate="G$1" x="73.66" y="114.3" smashed="yes" rot="MR270">
<attribute name="NAME" x="66.04" y="109.855" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="66.04" y="109.22" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="J31" gate="G$1" x="58.42" y="147.32"/>
<instance part="LOGO3" gate="G$1" x="149.86" y="40.64"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="J26" gate="G$1" pin="1"/>
<wire x1="142.24" y1="208.28" x2="139.7" y2="208.28" width="0.1524" layer="91"/>
<wire x1="139.7" y1="208.28" x2="139.7" y2="205.74" width="0.1524" layer="91"/>
<pinref part="GND23" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="J29" gate="G$1" pin="GND"/>
<pinref part="GND25" gate="1" pin="GND"/>
<wire x1="106.68" y1="149.86" x2="96.52" y2="149.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="149.86" x2="96.52" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="C28" gate="G$1" pin="2"/>
<pinref part="GND24" gate="1" pin="GND"/>
</segment>
<segment>
<pinref part="ENC1" gate="G$1" pin="C"/>
<pinref part="GND26" gate="1" pin="GND"/>
<wire x1="129.54" y1="104.14" x2="127" y2="104.14" width="0.1524" layer="91"/>
<wire x1="127" y1="104.14" x2="127" y2="99.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$70" class="0">
<segment>
<pinref part="J27" gate="G$1" pin="1"/>
<wire x1="63.5" y1="213.36" x2="60.96" y2="213.36" width="0.1524" layer="91"/>
<wire x1="60.96" y1="213.36" x2="60.96" y2="226.06" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="1"/>
<wire x1="60.96" y1="226.06" x2="53.34" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<pinref part="J27" gate="G$1" pin="2"/>
<wire x1="63.5" y1="210.82" x2="58.42" y2="210.82" width="0.1524" layer="91"/>
<wire x1="58.42" y1="210.82" x2="58.42" y2="220.98" width="0.1524" layer="91"/>
<wire x1="58.42" y1="220.98" x2="53.34" y2="220.98" width="0.1524" layer="91"/>
<pinref part="CON2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$72" class="0">
<segment>
<pinref part="J27" gate="G$1" pin="3"/>
<pinref part="CON3" gate="G$1" pin="1"/>
<wire x1="63.5" y1="208.28" x2="53.34" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$73" class="0">
<segment>
<pinref part="J27" gate="G$1" pin="4"/>
<wire x1="63.5" y1="205.74" x2="55.88" y2="205.74" width="0.1524" layer="91"/>
<wire x1="55.88" y1="205.74" x2="55.88" y2="203.2" width="0.1524" layer="91"/>
<pinref part="CON3" gate="G$1" pin="2"/>
<wire x1="55.88" y1="203.2" x2="53.34" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$74" class="0">
<segment>
<pinref part="CON4" gate="G$1" pin="1"/>
<wire x1="53.34" y1="190.5" x2="58.42" y2="190.5" width="0.1524" layer="91"/>
<wire x1="58.42" y1="190.5" x2="58.42" y2="203.2" width="0.1524" layer="91"/>
<pinref part="J27" gate="G$1" pin="5"/>
<wire x1="58.42" y1="203.2" x2="63.5" y2="203.2" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$75" class="0">
<segment>
<pinref part="CON4" gate="G$1" pin="2"/>
<wire x1="53.34" y1="185.42" x2="60.96" y2="185.42" width="0.1524" layer="91"/>
<wire x1="60.96" y1="185.42" x2="60.96" y2="200.66" width="0.1524" layer="91"/>
<pinref part="J27" gate="G$1" pin="6"/>
<wire x1="60.96" y1="200.66" x2="63.5" y2="200.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$76" class="0">
<segment>
<pinref part="J25" gate="G$1" pin="2"/>
<wire x1="104.14" y1="213.36" x2="116.84" y2="213.36" width="0.1524" layer="91"/>
<pinref part="TR1" gate="G$1" pin="2"/>
<wire x1="116.84" y1="213.36" x2="121.92" y2="213.36" width="0.1524" layer="91"/>
<wire x1="116.84" y1="213.36" x2="116.84" y2="231.14" width="0.1524" layer="91"/>
<junction x="116.84" y="213.36"/>
<pinref part="J24" gate="G$1" pin="3"/>
</segment>
</net>
<net name="VCC" class="0">
<segment>
<pinref part="SUP18" gate="G$1" pin="VCC"/>
<pinref part="J25" gate="G$1" pin="1"/>
<wire x1="104.14" y1="218.44" x2="106.68" y2="218.44" width="0.1524" layer="91"/>
<wire x1="106.68" y1="218.44" x2="106.68" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SUP20" gate="G$1" pin="VCC"/>
<pinref part="J32" gate="G$1" pin="1"/>
<wire x1="71.12" y1="116.84" x2="71.12" y2="114.3" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J31" gate="G$1" pin="2"/>
<pinref part="SUP19" gate="G$1" pin="VCC"/>
<wire x1="55.88" y1="147.32" x2="45.72" y2="147.32" width="0.1524" layer="91"/>
<wire x1="45.72" y1="147.32" x2="45.72" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<pinref part="J24" gate="G$1" pin="2"/>
<wire x1="119.38" y1="231.14" x2="119.38" y2="220.98" width="0.1524" layer="91"/>
<pinref part="TR1" gate="G$1" pin="3"/>
<wire x1="119.38" y1="220.98" x2="134.62" y2="220.98" width="0.1524" layer="91"/>
<wire x1="134.62" y1="220.98" x2="134.62" y2="215.9" width="0.1524" layer="91"/>
<wire x1="134.62" y1="215.9" x2="132.08" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="J26" gate="G$1" pin="2"/>
<wire x1="142.24" y1="213.36" x2="137.16" y2="213.36" width="0.1524" layer="91"/>
<wire x1="137.16" y1="223.52" x2="137.16" y2="213.36" width="0.1524" layer="91"/>
<junction x="137.16" y="213.36"/>
<pinref part="TR1" gate="G$1" pin="1"/>
<wire x1="137.16" y1="213.36" x2="132.08" y2="213.36" width="0.1524" layer="91"/>
<pinref part="J24" gate="G$1" pin="1"/>
<wire x1="137.16" y1="223.52" x2="121.92" y2="223.52" width="0.1524" layer="91"/>
<wire x1="121.92" y1="223.52" x2="121.92" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+3V3" class="0">
<segment>
<pinref part="J28" gate="G$1" pin="1"/>
<pinref part="+3V38" gate="G$1" pin="+3V3"/>
<wire x1="144.78" y1="177.8" x2="142.24" y2="177.8" width="0.1524" layer="91"/>
<wire x1="142.24" y1="177.8" x2="142.24" y2="180.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="J31" gate="G$1" pin="3"/>
<pinref part="+3V39" gate="G$1" pin="+3V3"/>
<wire x1="55.88" y1="144.78" x2="38.1" y2="144.78" width="0.1524" layer="91"/>
<wire x1="38.1" y1="144.78" x2="38.1" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$59" class="0">
<segment>
<pinref part="J29" gate="G$1" pin="TX"/>
<pinref part="J30" gate="G$1" pin="1"/>
<wire x1="137.16" y1="160.02" x2="144.78" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$60" class="0">
<segment>
<pinref part="J30" gate="G$1" pin="2"/>
<pinref part="J29" gate="G$1" pin="RX"/>
<wire x1="144.78" y1="157.48" x2="137.16" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$61" class="0">
<segment>
<pinref part="J30" gate="G$1" pin="3"/>
<wire x1="144.78" y1="154.94" x2="139.7" y2="154.94" width="0.1524" layer="91"/>
<wire x1="139.7" y1="154.94" x2="139.7" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J29" gate="G$1" pin="GPIO0"/>
<wire x1="139.7" y1="152.4" x2="137.16" y2="152.4" width="0.1524" layer="91"/>
<pinref part="R30" gate="&gt;NAME" pin="1"/>
<wire x1="139.7" y1="154.94" x2="139.7" y2="162.56" width="0.1524" layer="91"/>
<junction x="139.7" y="154.94"/>
</segment>
</net>
<net name="N$62" class="0">
<segment>
<pinref part="J29" gate="G$1" pin="GPIO2"/>
<wire x1="137.16" y1="149.86" x2="142.24" y2="149.86" width="0.1524" layer="91"/>
<wire x1="142.24" y1="149.86" x2="142.24" y2="152.4" width="0.1524" layer="91"/>
<pinref part="J30" gate="G$1" pin="4"/>
<wire x1="142.24" y1="152.4" x2="144.78" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$63" class="0">
<segment>
<pinref part="J29" gate="G$1" pin="CH_PD"/>
<pinref part="R28" gate="&gt;NAME" pin="1"/>
<wire x1="106.68" y1="154.94" x2="91.44" y2="154.94" width="0.1524" layer="91"/>
<wire x1="91.44" y1="154.94" x2="91.44" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$64" class="0">
<segment>
<pinref part="J29" gate="G$1" pin="RST"/>
<pinref part="R29" gate="&gt;NAME" pin="1"/>
<wire x1="106.68" y1="157.48" x2="99.06" y2="157.48" width="0.1524" layer="91"/>
<wire x1="99.06" y1="157.48" x2="99.06" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$65" class="0">
<segment>
<pinref part="R28" gate="&gt;NAME" pin="2"/>
<pinref part="R29" gate="&gt;NAME" pin="2"/>
<wire x1="91.44" y1="177.8" x2="91.44" y2="172.72" width="0.1524" layer="91"/>
<wire x1="91.44" y1="177.8" x2="99.06" y2="177.8" width="0.1524" layer="91"/>
<wire x1="99.06" y1="177.8" x2="99.06" y2="172.72" width="0.1524" layer="91"/>
<pinref part="R30" gate="&gt;NAME" pin="2"/>
<wire x1="99.06" y1="177.8" x2="104.14" y2="177.8" width="0.1524" layer="91"/>
<wire x1="104.14" y1="177.8" x2="124.46" y2="177.8" width="0.1524" layer="91"/>
<wire x1="124.46" y1="177.8" x2="139.7" y2="177.8" width="0.1524" layer="91"/>
<wire x1="139.7" y1="177.8" x2="139.7" y2="175.26" width="0.1524" layer="91"/>
<junction x="99.06" y="177.8"/>
<pinref part="C28" gate="G$1" pin="1"/>
<wire x1="139.7" y1="175.26" x2="139.7" y2="172.72" width="0.1524" layer="91"/>
<wire x1="124.46" y1="175.26" x2="124.46" y2="177.8" width="0.1524" layer="91"/>
<junction x="124.46" y="177.8"/>
<pinref part="J29" gate="G$1" pin="VCC"/>
<wire x1="106.68" y1="160.02" x2="104.14" y2="160.02" width="0.1524" layer="91"/>
<wire x1="104.14" y1="160.02" x2="104.14" y2="177.8" width="0.1524" layer="91"/>
<junction x="104.14" y="177.8"/>
<pinref part="J28" gate="G$1" pin="2"/>
<wire x1="144.78" y1="172.72" x2="142.24" y2="172.72" width="0.1524" layer="91"/>
<wire x1="142.24" y1="172.72" x2="142.24" y2="175.26" width="0.1524" layer="91"/>
<wire x1="142.24" y1="175.26" x2="139.7" y2="175.26" width="0.1524" layer="91"/>
<junction x="139.7" y="175.26"/>
</segment>
</net>
<net name="ENC_B" class="0">
<segment>
<pinref part="R31" gate="&gt;NAME" pin="1"/>
<wire x1="109.22" y1="101.6" x2="109.22" y2="109.22" width="0.1524" layer="91"/>
<pinref part="ENC1" gate="G$1" pin="B"/>
<wire x1="129.54" y1="101.6" x2="109.22" y2="101.6" width="0.1524" layer="91"/>
<pinref part="J33" gate="G$1" pin="2"/>
<wire x1="109.22" y1="101.6" x2="101.6" y2="101.6" width="0.1524" layer="91"/>
<junction x="109.22" y="101.6"/>
</segment>
</net>
<net name="ENC_A" class="0">
<segment>
<pinref part="ENC1" gate="G$1" pin="A"/>
<pinref part="R32" gate="&gt;NAME" pin="1"/>
<wire x1="129.54" y1="106.68" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<wire x1="121.92" y1="106.68" x2="121.92" y2="109.22" width="0.1524" layer="91"/>
<pinref part="J33" gate="G$1" pin="1"/>
<wire x1="101.6" y1="106.68" x2="121.92" y2="106.68" width="0.1524" layer="91"/>
<junction x="121.92" y="106.68"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<pinref part="R31" gate="&gt;NAME" pin="2"/>
<wire x1="109.22" y1="119.38" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<pinref part="R32" gate="&gt;NAME" pin="2"/>
<wire x1="109.22" y1="121.92" x2="121.92" y2="121.92" width="0.1524" layer="91"/>
<wire x1="121.92" y1="121.92" x2="121.92" y2="119.38" width="0.1524" layer="91"/>
<pinref part="J32" gate="G$1" pin="2"/>
<wire x1="76.2" y1="114.3" x2="76.2" y2="121.92" width="0.1524" layer="91"/>
<wire x1="76.2" y1="121.92" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<junction x="109.22" y="121.92"/>
</segment>
</net>
<net name="+5V" class="0">
<segment>
<pinref part="J31" gate="G$1" pin="1"/>
<pinref part="P+18" gate="1" pin="+5V"/>
<wire x1="55.88" y1="149.86" x2="50.8" y2="149.86" width="0.1524" layer="91"/>
<wire x1="50.8" y1="149.86" x2="50.8" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
<errors>
<approved hash="102,1,109.22,228.6,VIN,+5V,,,,"/>
<approved hash="102,1,139.7,228.6,VOUT,+3V3,,,,"/>
</errors>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
